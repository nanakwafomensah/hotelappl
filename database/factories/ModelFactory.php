<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(hotelapp\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(hotelapp\Event::class, function (Faker\Generator $faker) {


    return [
        'eventname' => $faker->name,
        'purpose' => $faker->sentence(20),
        'eventtype' => $faker->sentence(10),
        'startdate' => $faker->dateTime,
        'enddate'=>$faker->dateTime,
        'coordinator1'=>$faker->name,
        'coordinator2'=>$faker->name,
        'coorphone1'=>$faker->phoneNumber,
        'coorphone2'=>$faker->phoneNumber,
        'email'=>$faker->unique()->safeEmail
    ];
});


$factory->define(hotelapp\Customer::class, function (Faker\Generator $faker) {


    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'wholename' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'telephone'=>$faker->phoneNumber,
        'location'=>$faker->name
    ];
});


$factory->define(hotelapp\RoomType::class, function (Faker\Generator $faker) {


    return [
        'name' => $faker->name,
        'short_name' => $faker->name,
        'base_price' => $faker->randomDigit,
        'description' => $faker->sentence(30),
        'max_occupancy'=>$faker->randomDigit

    ];
});
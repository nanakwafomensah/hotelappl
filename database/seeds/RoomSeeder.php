<?php

use Illuminate\Database\Seeder;
use App\RoomType;
class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(RoomType::class,500)->create()->each(function($roomtype){
            $roomtype->save();
        });
    }
}

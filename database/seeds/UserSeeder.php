<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        
        DB::table('users')->delete();
        $credentials=[
            'first_name'=>'Admin',
            'last_name'=>'Mensah',
            'email'=>'admin@example.com',
            'password'=>'admin',
            'location'=>"Dansoman",
            'passwordagain'=>'admin',

        ];
        $frontdeskcredentials=[
            'first_name'=>'nana',
            'last_name'=>'Mensah',
            'email'=>'frontdesk@example.com',
            'password'=>'frontdesk',
            'location'=>"Dansoman",
            'passwordagain'=>'frontdesk',

        ];
        $user = Sentinel::registerAndActivate($credentials);
        $role=Sentinel::findRoleBySlug('Admin');
        $role->users()->attach($user);

        $userf = Sentinel::registerAndActivate($frontdeskcredentials);
        $rolef=Sentinel::findRoleBySlug('frontdesk');
        $rolef->users()->attach($userf);
    }
}

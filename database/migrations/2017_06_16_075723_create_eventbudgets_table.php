<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventbudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventbudgets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('eventid');
            $table->string('budget_type')->nullable()->default(null);
            $table->text('categories')->nullable()->default(null);
            $table->string('projected_subtotal')->nullable()->default(null);
            $table->string('actual_subtotal')->nullable()->default(null);
            $table->text('comment')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventbudgets');
    }
}

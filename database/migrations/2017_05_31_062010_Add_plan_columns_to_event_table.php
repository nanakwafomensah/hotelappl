<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlanColumnsToEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      // Schema::table('clients', function (Blueprint $table) {
        Schema::table('events', function (Blueprint $table) {
            $table->text('size');
            $table->text('gender_mix');
            $table->text('age_of_attendees');
            $table->text('special_needs');
            $table->text('food_requirements');
            $table->text('schedule_speakers');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('events', function (Blueprint $table) {
            //
            $table->dropColumn('size');
            $table->dropColumn('gender_mix');
            $table->dropColumn('age_of_attendees');
            $table->dropColumn('special_needs');
            $table->dropColumn('food_requirements');
            $table->dropColumn('schedule_speakers');
         
        });
    }
}

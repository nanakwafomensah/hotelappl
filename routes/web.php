<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('authentication.local.login');
});

////////////////////////////////////
/////////////FRONT DESK///////////////////////
////////////////////////////////////
////////////////////////////////////
Route::get('frontdashboard',['as'=>'frontdashboard','uses'=>'FrontDashboardController@index']);

//////////////////////////////
//////////////ADMIN///////////////
///////////////////////////////////
///////////////////////////////////
///////////////////////////////////
Route::get('login_local',['as'=>'login_local','uses'=>'LocalLoginController@login']);
Route::post('postlogin_local',['as'=>'postlogin_local','uses'=>'LocalLoginController@postlogin']);

Route::group(['middleware' => ['admin']], function () {
    //local

    Route::post('postlogout_local',['as'=>'postlogout_local','uses'=>'LocalLoginController@postlogout']);
    Route::get('new_user',['as'=>'new_user','uses'=>'LocalRegistrationController@register']);
    Route::post('addregister_local',['as'=>'addregister_local','uses'=>'LocalRegistrationController@addregistration']);
    Route::post('updateregisteredUser',['as'=>'updateregisteredUser','uses'=>'LocalRegistrationController@updateregisteredUser']);
    Route::post('userdelete',['as'=>'userdelete','uses'=>'LocalRegistrationController@userdelete']);

    Route::post('forgotpasswordlocal',['as'=>'forgotpasswordlocal','uses'=>'LocalRegistrationController@forgotpassword']);
    Route::get('reset/{email}/{resetCode}',['as'=>'reset','uses'=>'LocalRegistrationController@resetpassword']);
    Route::post('reset/{email}/{resetCode}',['as'=>'reset','uses'=>'LocalRegistrationController@postresetpassword']);


    Route::post('updateroomtype',['as'=>'updateroomtype','uses'=>'RoomTypeController@updateroomtype']);
    Route::post('deleteroomtype',['as'=>'deleteroomtype','uses'=>'RoomTypeController@deleteroomtype']);
    Route::post('setroomprice',['as'=>'setroomprice','uses'=>'RoomCalendarController@setPriceInRangeForRoomType']);


//Guest
    Route::post('storecustomer',['as'=>'storecustomer','uses'=>'CustomerController@postnewcustomer']);
    Route::post('editcustomer',['as'=>'editcustomer','uses'=>'CustomerController@editcustomer']);
    Route::post('deletecustomer',['as'=>'deletecustomer','uses'=>'CustomerController@deletecustomer']);
    Route::get('guestlist',['as'=>'guestlist','uses'=>'CustomerController@guestlist']);
    Route::get('archivedguest',['as'=>'archivedguest','uses'=>'CustomerController@archivedguest']);
    Route::get('datatable/guestlistdetails',array('as'=>'datatable.guestlistdetails','uses'=>'CustomerController@guestlistdetails'));
    Route::get('datatable/archivedguestdetails',array('as'=>'datatable.archivedguestdetails','uses'=>'CustomerController@archivedguestdetails'));
    Route::post('checkoutguest',['as'=>'checkoutguest','uses'=>'CustomerController@checkoutguest']);
//Route::get('printguest/{id}',['as'=>'printguest/{id}','uses'=>'printController@printpreview']);
    Route::post('generate_invoice',['as'=>'generate_invoice','uses'=>'InvoiceController@generate_invoice']);
    Route::post('update_invoice',['as'=>'update_invoice','uses'=>'InvoiceController@update_invoice']);
    Route::post('delete_for_update',['as'=>'delete_for_update','uses'=>'InvoiceController@delete_for_update']);

    Route::get('profile',['as'=>'profile','uses'=>'ProfileController@saveprofile']);
    Route::post('updateprofile',['as'=>'updateprofile','uses'=>'ProfileController@updateprofile']);
    Route::post('reserveroom',['as'=>'reserveroom','uses'=>'BookingController@createReservation']);
//finance
    Route::get('payment',['as'=>'payment','uses'=>'PaymentController@index']);
    Route::post('makepayment',['as'=>'makepayment','uses'=>'PaymentController@storepayment']);
    Route::get('billing',['as'=>'billing','uses'=>'BillingController@index']);


    Route::post('searchroom',['as'=>'searchroom','uses'=>'RoomCalendarController@searchAvailability']);
    Route::get('newcustomer',['as'=>'newcustomer','uses'=>'CustomerController@index']);
    Route::get('newbooking',['as'=>'newbooking','uses'=>'BookingController@index']);
    Route::get('room_type',['as'=>'room_type','uses'=>'RoomTypeController@index']);
    Route::post('addroomtype',['as'=>'addroomtype','uses'=>'RoomTypeController@storeroomtype']);
    Route::get('broadcast_message',['as'=>'broadcast_message','uses'=>'broadcastController@broadcast_message']);
    Route::get('dashboard',['as'=>'dashboard','uses'=>'dashboardController@index']);
//search
    Route::get('getavailableroomslocal',['as'=>'getavailableroomslocal','uses'=>'BookingController@getavailableroomslocal']);
    Route::get('validatecustomer',['as'=>'validatecustomer','uses'=>'BookingController@validatecustomer']);
    Route::get('validatepayment',['as'=>'validatepayment','uses'=>'BookingController@validatepayment']);
    Route::get('specialbook',['as'=>'specialbook','uses'=>'BookingController@specialbook']);
    Route::get('debtors',['as'=>'debtors','uses'=>'DebtorsController@index']);
    Route::get('excess',['as'=>'excess','uses'=>'ExcessController@index']);
//datatables
    Route::get('datatable/userdetails',array('as'=>'datatable.userdetails','uses'=>'LocalRegistrationController@userdetails'));
    Route::get('datatable/roomtypedetails',array('as'=>'datatable.roomtypedetails','uses'=>'RoomTypeController@roomtypedetails'));
    Route::get('datatable/customersdetails',array('as'=>'datatable.customersdetails','uses'=>'CustomerController@customersdetails'));
    Route::get('datatable/eventsdetails',array('as'=>'datatable.eventsdetails','uses'=>'EventController@eventsdetails'));
    Route::get('datatable/availableroomdetails',array('as'=>'datatable.availableroomdetails','uses'=>'RoomCalenderController@searchAvailability'));
    Route::get('datatable/paymentdetails',array('as'=>'datatable.paymentdetails','uses'=>'PaymentController@paymentdetails'));
    Route::get('datatable/specialbook',['as'=>'datatable.specialbook','uses'=>'BookingController@specialbookdetails']);
    Route::get('datatable/usercontact',['as'=>'datatable.usercontact','uses'=>'SmsController@usercontact']);
    Route::get('datatable/bills',['as'=>'datatable.bills','uses'=>'BillingController@getbills']);
    Route::get('datatable/excess',['as'=>'datatable.excess','uses'=>'ExcessController@getbills']);
    Route::get('datatable/debtors',['as'=>'datatable.debtors','uses'=>'DebtorsController@getbills']);
    Route::get('datatable/closeevent',['as'=>'datatable.closeevent','uses'=>'EventController@get_all_planed_event']);

    Route::get('datatable/tasklist',['as'=>'datatable.tasklist','uses'=>'TaskController@gettasklist']);
    Route::get('datatable/completedtask',['as'=>'datatable.completedtask','uses'=>'TaskController@getcompletedtask']);
    Route::get('datatable/pendingtask',['as'=>'datatable.pendingtask','uses'=>'TaskController@getbills']);


///////////
//special PDF
    Route::get('pdfinvoice/{customerid}/{invoicenumber}',['as'=>'pdfinvoice','uses'=> 'PdfController@pdfinvoice']);
    Route::get('pdfreceiptpayment/{customerid}/{invoicenumber}/{receipt}',['as'=>'pdfreceiptpayment','uses'=> 'PdfController@pdfreceiptpayment']);
    Route::get('editinvoice/{customeridinvoice}/pdfinvoice/{customerid}/{invoicenumber}',['as'=>'editinvoice','uses'=> 'PdfController@pdfinvoice']);

    Route::get('viewinvoice/{customerid}/{invoicenumber}',['as'=>'viewinvoice','uses'=> 'InvoiceController@viewinvoice']);
    Route::get('editinvoice/{customerid}/{invoicenumber}',['as'=>'editinvoice','uses'=> 'InvoiceController@editinvoice']);

    Route::group(['prefix'=>'sms'],function(){
        Route::post('single',['as'=>'single','uses'=>'SmsController@singlesms']);
        Route::post('bulk',['as'=>'bulk','uses'=>'SmsController@loadbulkcontact']);
        Route::post('sendbulk',['as'=>'sendbulk','uses'=>'SmsController@sendbulksms']);
    });
    Route::get('newtask',['as'=>'newtask','uses'=>'TaskController@index']);
    Route::get('completedtask',['as'=>'completedtask','uses'=>'TaskController@completedtask']);
    Route::get('pendingtask',['as'=>'pendingtask','uses'=>'TaskController@pendingtask']);
    Route::post('createtask',['as'=>'createtask','uses'=>'TaskController@createtask']);
    Route::post('deletetask',['as'=>'deletetask','uses'=>'TaskController@deletetask']);
    Route::post('updatetask',['as'=>'updatetask','uses'=>'TaskController@updatetask']);
    Route::post('complete_a_task',['as'=>'complete_a_task','uses'=>'TaskController@complete_a_task']);
    
    
    Route::group(['prefix'=>'event'],function(){
        Route::post('storeevent',['as'=>'storeevent','uses'=>'EventController@store']);
        Route::post('updateevent',['as'=>'updateevent','uses'=>'EventController@update']);
        Route::get('planevent/{eventid?}',['as'=>'planevent','uses'=>'EventController@plan']);
        Route::get('getevent',['as'=>'getevent','uses'=>'EventController@getevent']);
        Route::get('budgetevent/{eventid?}',['as'=>'budgetevent','uses'=>'EventController@budgetindex']);
        Route::get('reserveevent',['as'=>'reserveevent','uses'=>'EventController@reserve']);
        Route::get('eventplanpdf/{planid}',['as'=>'eventplanpdf','uses'=>'PdfController@eventpdf']);
        Route::get('eventbudgetpdf/{event_id}',['as'=>'eventbudgetpdf','uses'=>'PdfController@eventbudgetpdf']);
        Route::get('update_event_budget',['as'=>'update_event_budget','uses'=>'EventController@budgetcategoryexit']);
    });
     Route::post('postplanevent',['as'=>'postplanevent','uses'=>'EventController@postplanevent']);
     Route::post('event_budget',['as'=>'event_budget','uses'=>'EventController@event_budget']);
     Route::get('newevent',['as'=>'newevent','uses'=>'EventController@index']);
     Route::get('closeevent',['as'=>'closeevent','uses'=>'EventController@closeevent']);
   




});
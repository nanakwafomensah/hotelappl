<?php

namespace hotelapp;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $fillable = ['first_name', 'last_name','email','telephone'];

}

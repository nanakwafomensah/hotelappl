<?php

namespace hotelapp;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    //
    protected $fillable = ['total_price', 'occupancy','checkin','checkout','name'];

    public function nights()
    {
        return $this->hasMany('hotelapp\ReservationNight');
    }

    public function Customer(){
        return $this->belongsTo('hotelapp\Customer');
        }
}

<?php

namespace hotelapp;

use Illuminate\Database\Eloquent\Model;

class ReservationNight extends Model
{
    //
    protected $fillable = ['rate', 'date', 'room_type_id'];

    function Reservation()
    {
        return $this->hasOne('hotelapp\Reservation');
    }

    function RoomType()
    {
        return $this->hasOne('hotelapp\RoomType');
    }
}

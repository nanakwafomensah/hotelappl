<?php

namespace hotelapp\Http\Middleware;

use Closure;
use Sentinel;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //1.user should be authenticated.
        //2.authenticated user must have a role of an admin
        if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug=='Admin'){

            return $next($request);
        }else{
            return redirect('/');
        }

    }
}

<?php

namespace hotelapp\Http\Middleware;

use Closure;
use Sentinel;
class DashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::check()&& (Sentinel::getUser()->roles()->first()->slug=='admin'||'manager')){

            return $next($request);
        }else{
            return redirect('/');
        }

    }
    
}

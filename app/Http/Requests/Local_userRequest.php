<?php

namespace hotelapp\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Sentinel;
class Local_userRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug=='admin'){
//            return true;
//        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required|unique:users',
            'password'=>'required',
            'location'=>'required',
            'passwordagain'=>'required'
        ];
    }
}

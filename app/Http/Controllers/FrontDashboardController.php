<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;

class FrontDashboardController extends Controller
{
    //
    public function index(){
        return view('frontdesk.frontdashboard');
    }
}

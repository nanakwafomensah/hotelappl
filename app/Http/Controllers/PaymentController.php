<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use hotelapp\Payment;
use DB;
use Datatables;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class PaymentController extends Controller
{
    //
    public function index(){
        return view('sitepages.payment');
    }
    public function storepayment(Request $request){
        $payment = new Payment();

        $payment->customerid=$request->customerid;
        $payment->category=$request->category;
        $payment->receiptnumber=$request->receiptnumber;
        $payment->invoicenumber=$request->invoicenumber;
        $payment->paymentdate=$request->paymentdate;
        $payment->amount=$request->amount;
        $payment->remark=$request->remark;
        $payment->status='open';
        $payment->staff=Sentinel::getUser()->first_name;
        $payment->save();
        $notification=array(
            'message'=>"New Payment has been Succesfully Added",
            'alert-type'=>'success'
        );
        return redirect('payment')->with($notification);
    }
    public function paymentdetails(){
        $paymentdetails = DB::table('payments')->select(['payments.id','payments.customerid', 'payments.category','payments.receiptnumber','payments.paymentdate','payments.amount','payments.remark','payments.status','payments.invoicenumber']);

        return Datatables::of($paymentdetails)

            ->addColumn('action', function ($paymentdetails) {
                $pdfurl="pdfreceiptpayment/".$paymentdetails->customerid . "/" . "$paymentdetails->invoicenumber" . "/" . "$paymentdetails->receiptnumber";
                // return '<a href="#edit-'.$questions->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                return'<a href="" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal1" data-customerid="'.$paymentdetails->id.'"  data-firstname="'.$paymentdetails->customerid.'"   data-lastname="'.$paymentdetails->category.'"  data-email="'.$paymentdetails->receiptnumber.'" data-telephone="'.$paymentdetails->paymentdate.'"  >Edit</a>&nbsp
                                           <a href="" data-toggle="modal" data-target="#delModal"  data-customerid="'.$paymentdetails->id.'"  data-firstname="'.$paymentdetails->customerid.'" class="deletebtn btn btn-danger" > Delete</a>&nbsp
                             <a href="'.$pdfurl.'"  data-customerid="'.$paymentdetails->id.'"  data-firstname="'.$paymentdetails->customerid.'" class="downloadbtn btn btn-success" > Download</a>   ';
            })

            ->make(true);
    }
}

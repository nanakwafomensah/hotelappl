<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use hotelapp\RoomType;
use Mockery\Exception;
use Datatables;
use DB;

class RoomTypeController extends Controller
{
    //
    public function index()
    {

        return view('sitepages.roomtype');
    }
    public function roomtypedetails(){
        
        $roomtypedetails = DB::table('room_types')->select(['room_types.id','room_types.name', 'room_types.short_name','room_types.base_price','room_types.description','room_types.max_occupancy']);

        return Datatables::of($roomtypedetails)
            ->addColumn('action', function ($roomtypedetails) {
                // return '<a href="#edit-'.$questions->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                return'<a href="" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal1" data-id="'.$roomtypedetails->id.'"  data-name="'.$roomtypedetails->name.'"   data-shortname="'.$roomtypedetails->short_name.'"  data-baseprice="'.$roomtypedetails->base_price.'" data-description="'.$roomtypedetails->description.'" data-maxoccupancy="'.$roomtypedetails->max_occupancy.'" >Edit</a>&nbsp
                                           <a href="" data-toggle="modal" data-target="#delModal"  data-id="'.$roomtypedetails->id.'"  data-name="'.$roomtypedetails->name.'" class="deletebtn btn btn-danger" > Delete</a>
                                ';
            })

            ->make(true);

         
    }
    public function storeroomtype(Request $request)
    {
        try{
            $room_type = new RoomType();

            $room_type->name=$request->roomname;
            $room_type->short_name=$request->shortname;
            $room_type->base_price=$request->baseprice;
            $room_type->description=$request->description;
            $room_type->max_occupancy=$request->maxoccupancy;

            $room_type->save();
            $notification=array(
                'message'=>"New RoomType has been Succesfully Added",
                'alert-type'=>'success'
            );
            return redirect('room_type')->with($notification);
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('room_type')->with($notification);
        }



}
    public function updateroomtype(Request $request)
    {

        try{
            $rootype = RoomType::findorfail($request->roomtypeid);
            $rootype->name=$request->roomname;
            $rootype->short_name=$request->shortname;
            $rootype->base_price=$request->baseprice;
            $rootype->description=$request->description;
            $rootype->max_occupancy=$request->maxoccupancy;

            if($rootype->update()){
                $notification=array(
                    'message'=>"RoomType has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('room_type')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"RoomType  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('room_type')->with($notification);
        }


    }
    public function deleteroomtype(Request $request)
    {
        try{
            $rootype = RoomType::findorfail($request->roomtypeid);
           if($rootype->delete()){
           $notification=array(
               'message'=>"RoomType has been deleted",
               'alert-type'=>'success'
           );
            return redirect('room_type')->with($notification);
           }


        }catch(Exception $e){

        }
    }
}

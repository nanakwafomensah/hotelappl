<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use hotelapp\Invoice;

use DB;
use Datatables;
class InvoiceController extends Controller
{
    //
    public function  generate_invoice(Request $request){

            $newin=new Invoice();
            $newin->customerid= $request['customerid'];
            $newin->invoicedate=$request['invoicedate'];
            $newin->invoicenumber=$request['invoicenumber'];
            $newin->description=$request['description'];
            $newin->quantity=$request['quantity'];
            $newin->amount=$request['amount'];
            
            if($newin->save()) {

                $customerid = $request['customerid'];
                $invoicenumber = $request['invoicenumber'];
                $url="pdfinvoice/".$customerid . "/" . "$invoicenumber";
                $output = '<a href="' . $url . '" class="btn btn-success" id="downloadbtn"><i class="fa fa-download" aria-hidden="true" ></i> Download</a>';
                return Response($output);
            }else{
                $output='<h3 class="alert alert-danger">error<h3>';
                return Response($output);
            }




    }
    public function update_invoice(Request $request){
          $newin=new Invoice();
          $newin->customerid= $request['customerid'];
          $newin->invoicedate=$request['invoicedate'];
          $newin->invoicenumber=$request['invoicenumber'];
          $newin->description=$request['description'];
          $newin->quantity=$request['quantity'];
          $newin->amount=$request['amount'];
        if($newin->save()) {

            $customerid = $request['customerid'];
            $invoicenumber = $request['invoicenumber'];
            $url = "pdfinvoice/" . $customerid . "/" . "$invoicenumber";
            $output = '<a href="' . $url . '" class="btn btn-success" id="downloadbtn"><i class="fa fa-download" aria-hidden="true" ></i> Download</a>';
            return Response($output);
        }else{
            $output='<h3 class="alert alert-danger">error<h3>';
            return Response($output);
        }
      }
    public function delete_for_update(Request $request){
        $state=null;
        $deleteinvoice =  DB::table('invoices')->where([
            ['invoicenumber', '=',$request->invoicenumber],
            ['customerid', '=', $request->customerid],
            ['invoicedate', '=', $request->invoicedate],
        ])->delete();
        if($deleteinvoice){
             $state="good";
        }
        else{
            $state="bad";
        }
        return Response($state);
    }
    
    public function viewinvoice($customerid,$invoicenumber){

        $customername = DB::table('customers')->where('id',$customerid )->first();

        $customername= $customername->first_name ." ".$customername->last_name;
        ////////////////////INVOICE ITEMS/////////////////////////////////////////
        $items_oninvoice = DB::table('invoices')
            ->where([
                ['invoicenumber', '=',$invoicenumber],
                ['customerid', '=', $customerid],
            ]) ->get();
        ///////////////////////////////////////////////
        $invoicedate=DB::table('invoices')
            ->where([
            ['invoicenumber', '=',$invoicenumber],
            ['customerid', '=', $customerid],
        ])->first();
        $invoicedate=$invoicedate->invoicedate;
        //////////////////OWING//////////////////////////////
        $value=array();
        $bills = DB::table('invoices')->select([DB::raw('SUM(invoices.amount) as a_mount')])->where([['invoicenumber', 'like', '%'.$invoicenumber.'%'], ['customerid', '=', $customerid]])->get();
        foreach($bills as $l){foreach($l as $key => $val){array_push($value, $val);}}


        $allpay = DB::table('payments')->where([['invoicenumber', 'like', '%'.$invoicenumber.'%'], ['customerid', '=', $customerid],])->sum('amount');
        $owing='';
        if($value[0] > $allpay){$owing= $value[0]-$allpay;}else{$owing='0';}

        ////////////////////EXCESS///////////////
        $excess='';
        if($allpay >$value[0]){
            $excess=$allpay-$value[0];
        }else{
            $excess='0';
        }
        
        //////////////////PAYMENTS////////////////
        $paymentdetails=DB::table('payments')->where([['invoicenumber', 'like', '%'.$invoicenumber.'%'], ['customerid', '=', $customerid],])->get();
        
        ///////////////////////////////////////
        
        return view('sitepages.viewinvoice')->with(['customerid'=>$customerid,
            'invoicenumber'=>$invoicenumber,'items_oninvoice'=>$items_oninvoice,'customername'=>$customername,'invoicedate'=>$invoicedate,
            'owing'=>$owing,'excess'=>$excess,'payment'=>$allpay,'paymentdetails'=>$paymentdetails]);
    }

    public function editinvoice($customerid,$invoicenumber){
        /////////////GET INVOICE DATE///////////////////

        $invoicedate=DB::table('invoices')
            ->where([
                ['invoicenumber', '=',$invoicenumber],
                ['customerid', '=', $customerid],
            ])->first();
        $invoicedate=$invoicedate->invoicedate;
        //////////////GET INVOICE ITEMS///////////////////
        $items_oninvoice = DB::table('invoices')
            ->where([
                ['invoicenumber', '=',$invoicenumber],
                ['customerid', '=', $customerid],
            ]) ->get();

        //////////////////////////////////////////////////
        return view('sitepages.editinvoice')->with(['customerid'=>$customerid,
            'invoicenumber'=>$invoicenumber,'invoicedate'=>$invoicedate,'items_oninvoice'=>$items_oninvoice]);
    }
}

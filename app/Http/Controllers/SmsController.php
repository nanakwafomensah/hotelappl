<?php

namespace hotelapp\Http\Controllers;

use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use DB;
use Datatables;
use Illuminate\Database\Eloquent\Collection;


class SmsController extends Controller
{
    //
    public function usercontact(){
          $customercontacts = DB::table('customers')
                            ->select(DB::raw('CONCAT_WS(" ",customers.first_name,customers.last_name) as wholename'),'telephone','id')->get();
           return Datatables::of($customercontacts)
            ->addColumn('customname', function ($customercontacts) {
                return'<a href=""id="customerstatus" data-customerid="'.$customercontacts->id.'" data-telephone="'.$customercontacts->telephone.'"  data-name="'.$customercontacts->wholename.'" >'.$customercontacts->wholename.'</a>
                                ';
            })
            ->addColumn('status', function ($customercontacts) {
                $customer_id_in_collection=array();
                $customer_id_in= DB::table('reservations')
                    ->select('id')->get();
                foreach($customer_id_in as $l){
                    foreach($l as $key => $val){
                        array_push($customer_id_in_collection, $val);
                    }
                }

                if(in_array($customercontacts->id, $customer_id_in_collection)){
                    return'<button type="button" class="btn btn-success" aria-label="Left Align">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            </button>';
                }else{
                    return'<button type="button" class="btn btn-danger" aria-label="Left Align">
              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </button>';
                }

            })
            ->make(true);
    }

    public function singlesms(Request $request){
          $msisdn=$request->number;
          $sender=$request->sender;
          $message=$request->messagecontent;
           $receipient=$request->name_of_receipient;
          $msg = urlencode($message);
           $api= 'http://api.nalosolutions.com/bulksms/?username=nalo_kwafo&password=p@$$w0rd&type=0&dlr=1&destination='.$msisdn.'&source='.$sender.'&message=' . $msg;
        if(file_get_contents($api)){
            return Response('
<div class="alert alert-success alert-dismissable">  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a
>Message was successfully sent to '.$receipient.'</div>');

        }else{
            return Response('<div class="alert alert-danger alert-dismissable">  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Message could not be sent to '.$receipient.'</div>');
        }

    }

    public function loadbulkcontact(Request $request){
         $datefrom=$request->date_from;
        $dateto=$request->date_to;
        $value=$request->value;
        $output="";
        if($value=='checkin'){
            $checkedincutomers= DB::table('customers')
                ->join('reservations', 'customers.id', '=', 'reservations.customer_id')
                ->select(DB::raw('CONCAT_WS(" ",customers.first_name,customers.last_name) as wholename'),
                    'customers.telephone', 'reservations.room_type_id','customers.id')
                ->where([
                    ['reservations.status', '=', '1'],
                    ['reservations.checkin', '>=', $datefrom],
                    ['reservations.checkin', '>=', $dateto]
                ])
                ->get();//end of query
//            return Response($checkedincutomers);


            $checkedincutomer = new Collection($checkedincutomers);
            if($checkedincutomer){
                foreach($checkedincutomer as $key=>$checkin) {
                    $output .= '<tr>' .
                        '<td class="td_4"><input id="check-box"  type="checkbox"  data-name="'.$checkin->wholename .'" data-telephone="'.$checkin->telephone.'"/></td>' .
                        '<td class="guestname">' . $checkin->wholename . '</td>' .
                        '<td class="number">' . $checkin->telephone . '</td>' .
                        '</tr>';
                }
                return Response($output);
            }







        }
    }
    
    
    public function sendbulksms(Request $request){
        $name=$request->name;
        $msisdn=$request->telephone;
        $sender="hotel";
        $message="hello";
        $msg = urlencode($message);
        $api= 'http://api.nalosolutions.com/bulksms/?username=nalo_kwafo&password=p@$$w0rd&type=0&dlr=1&destination='.$msisdn.'&source='.$sender.'&message=' . $msg;
        if(file_get_contents($api)){
            echo "sent";
        }
        
    }

}




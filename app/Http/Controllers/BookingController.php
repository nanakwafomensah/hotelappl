<?php

namespace hotelapp\Http\Controllers;
use hotelapp\Customer;
use hotelapp\Payment;
use hotelapp\RoomCalendar;
use hotelapp\RoomType;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use DB;
use Datatables;
use Carbon\Carbon;
use hotelapp\Reservation;
use hotelapp\ReservationNight;
use Illuminate\Http\Response;


class BookingController extends Controller
{
    //
    public function index(){
        return view('sitepages.newbooking');
    }
    public function onlineindex(){
        return view('sitepages.onlinebook');
    }
    public function getavailableroomslocal(Request $request){
      if($request->ajax()){
          $start_dt =  $request['start_dt'];
          $end_dt =  $request['end_dt'];
          $min_occupancy = $request['max_occupancy'];


          $room_types = RoomType::where('max_occupancy','>=',$min_occupancy)->get();

          $available_room_types=array();
          $output="";
          foreach( $room_types as $room_type){

              $count = RoomCalendar::where('day','>=',$start_dt)
                  ->where('day','<',$end_dt)
                  ->where('room_type_id','=',$room_type->id)
                  ->where('availability','<=',0)->count();

              if($count==0){
                  $total_price = RoomCalendar::where('day','>=',$start_dt)
                      ->where('day','<',$end_dt)
                      ->where('room_type_id','=',$room_type->id)
                      ->sum('rate');

                  $room_type->total_price = $total_price;
                  array_push($available_room_types,$room_type);
              }

          }
          $available_rooms = new Collection($available_room_types);
                  if($available_rooms){
                     foreach($available_rooms as $key=>$available_room) {
                         $output .= '<tr>' .

                             '<td class="td_2">' . $available_room->name . '</td>' .
                             '<td class="td_3">' . $available_room->short_name . '</td>' .
                             '<td class="td_4">' . $available_room->total_price . '</td>' .
                             '<td class="td_4">' . $available_room->description . '</td>' .
                             '<td class="td_4">' . $available_room->max_occupancy . '</td>' .
                             '<td>' .
                             '<a href="" id="editme" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal1" data-id="' . $available_room->id . '"  data-name="' . $available_room->name . '" data-short_name="' . $available_room->short_name . '" data-total_price="' . $available_room->total_price . '" data-max_occupancy="'.$available_room->max_occupancy.'" >Book</a>&nbsp
                             <a href="" data-toggle="modal" data-target="#delModal"  data-id="' . $available_room->id . '" data-name="' . $available_room->name . '" class="deletebtn btn btn-danger" >invoice</a></td>-->
                          </tr>';
                     }
                      return Response($output);
                  }
      }



    }

    public function specialbookdetails(){
        $bookedrooms=array();
        $allrerooms= DB::table('reservations')
            //->join('reservation_nights', 'reservations.id', '=', 'reservation_nights.reservation_id')
            ->select('reservations.room_type_id')
            ->Where('reservations.status',1)
            ->groupBy('reservations.room_type_id')
            ->get();

        foreach($allrerooms as $l){
            foreach($l as $key => $val){
                array_push($bookedrooms, $val);
            }
        }




        $roomtypedetails = DB::table('room_types')

            ->select(['room_types.id','room_types.name', 'room_types.short_name','room_types.base_price','room_types.description','room_types.max_occupancy'])

             ->whereNotIn('room_types.id',$bookedrooms);



        return Datatables::of($roomtypedetails)
            ->addColumn('action', function ($roomtypedetails) {
                // return '<a href="#edit-'.$questions->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                return'<a href="" id="editme" class="edit-modal btn btn-sm btn-warning"  data-toggle="modal" data-target="#Modal1" data-id="'.$roomtypedetails->id.'"  data-name="'.$roomtypedetails->name.'"   data-shortname="'.$roomtypedetails->short_name.'"  data-total_price="'.$roomtypedetails->base_price.'" data-baseavailability="'.$roomtypedetails->description.'" data-maxoccupancy="'.$roomtypedetails->max_occupancy.'" data-toggle="tooltip" title="Booking"><i class="fa fa-bed" aria-hidden="true">&nbsp;</i></a>&nbsp
                       <a href="" data-toggle="modal" data-target="#delModal" data-toggle="tooltip" title="Invoice" id="invoice_auto_load" data-id="'.$roomtypedetails->id.'" data-total_price="'.$roomtypedetails->base_price.'" data-name="'.$roomtypedetails->name.'" class="deletebtn btn btn-success" ><i class="fa fa-dashcube" aria-hidden="true">&nbsp;</i></a>
                                ';
            })

            ->make(true);
    }

    private function sendEmail($customerdetails){
        Mail::send('sitepages.emails.booking',
            [
                'customerdetails'=>$customerdetails,

            ],function($message)use($customerdetails){
                $message ->to($customerdetails->email);
                $message ->subject("Hello $customerdetails->first_name,
                activate your account.");
            });
    }
    public function validatecustomer(Request $request){
        if($request->ajax()){
            $customer_id=$request->customerid;
            $customername = DB::table('customers')->select('first_name')
                ->where('id', '=', $customer_id)
                ->first();
            $name = new Collection($customername);
            if($name){
                Return Response($name);
            }

        }

    }

    public function createReservation(Request $request){
        try{
            //dd($request->all());
            //$room_info  =$request['roomidreserve'];
            $receiptnumber= $request['receiptnumber'];
            $start_dt = $request['startdate'];
            $end_dt=$request['enddate'];

            $customerid = $request['customerid'];

            $reservation  =new Reservation();
            $reservation->total_price=$request['total_price'];
            $reservation->occupancy=$request['occupancy'];
            $reservation->customer_id=$request['customerid'];
            $reservation->checkin=$start_dt;
            $reservation->checkout=$end_dt;
            $reservation->status=1;
           $reservation->room_type_id=$request['roomidreserve'];
            $reservation->save();

//            $date=$start_dt;
//
//            while (strtotime($date) < strtotime($end_dt)) {
//
//                $room_calendar = RoomCalendar::where('day','=',$date)
//                    ->where('room_type_id','=',$request['roomidreserve'])->first();
//
//                $night = new ReservationNight();
//                $night->day=$date;
//
//                $night->rate=$room_calendar->rate;
//                $night->room_type_id=$request['roomidreserve'];
//                $night->reservation_id=$reservation->id;
//
//                $room_calendar->availability--;
//                $room_calendar->reservations++;
//
//                $room_calendar->save();
//                $night->save();
//
//                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
//
//            }

            DB::table('payments')
                ->where('customerid', $customerid)
                ->where('receiptnumber', $receiptnumber)
                ->where('status', 'open')
                ->where('category',$request['category'])
                ->where('paymentdate',$request['paymentdate'])
                ->update(['status' => "cleared"]);


            $customerdetails = Customer::find($customerid);
            $this->sendEmail($customerdetails);

            // return $reservation;
            $notification=array(
                'message'=>"A new customer has been Succesfully reserverd a room",
                'alert-type'=>'success'
            );
            return redirect('newbooking')->with($notification);
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('newbooking')->with($notification);
        }


    }
    public function validatepayment(Request $request){
        if($request->ajax()){
            $receiptnumber=$request->receiptnumber;
            $customerid=$request->customerid;
            $category=$request->category;
            $paymentdate=$request->paymentdate;

            $paymentvalidate = DB::table('payments')
                ->where('receiptnumber', '=', $receiptnumber)
                ->where('customerid', '=', $customerid)
                ->where('status', '=', 'open')
                ->where('category', '=', $category)
                ->where('paymentdate', '=', $paymentdate)
                ->first();
            $details = new Collection($paymentvalidate);
            if($details){
                Return Response($details);

            }


        }
    }
    public function specialbook(){
        return view('sitepages.specialbooking');
    }
}

<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use hotelapp\Profile;
use DB;
use Image;
class ProfileController extends Controller
{
    //
    public function saveprofile()
    {
        $hoteldetails =  DB::table('profiles')->first();
      //  dd($hoteldetails);
        return view('sitepages.profile')->with(['hoteldetails'=>$hoteldetails]);
    }

    public function updateprofile(Request $request){

        if($request->hasFile('avatar')){
            $avatar=$request->file('avatar');
            $filename=time().'.'.$avatar->getClientOriginalExtension();


            $profile = Profile::findorfail($request->profileid);
            $profile->hotelname=$request->hotelname;
            $profile->website=$request->website;
            $profile->address=$request->address;
            $profile->telephone=$request->telephone;
            $profile->phone=$request->phone;
            $profile->avatar=$filename;
            $profile->country=$request->country;
            $profile->city=$request->city;
            $profile->email=$request->email;
            if($profile->update()){
                Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/'.$filename));
                $notification=array(
                    'message'=>"Hotel details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('profile')->with($notification);
            }


        }else{
            $profile = Profile::findorfail($request->profileid);
            $profile->hotelname=$request->hotelname;
           $profile->website=$request->website;
            $profile->address=$request->address;
            $profile->telephone=$request->telephone;
            $profile->phone=$request->phone;
            
            $profile->country=$request->country;
            $profile->city=$request->city;
            $profile->email=$request->email;
            if($profile->update()){

                $notification=array(
                    'message'=>"Hotel details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('profile')->with($notification);
            }

        }

    }
}

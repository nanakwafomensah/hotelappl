<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use hotelapp\RoomType;
class DashboardController extends Controller
{
    //
    public  function index(){

        $allroomslist=array();
        $allrooms= DB::table('room_types')
            ->select('id')
            ->get();

        foreach($allrooms as $l){
            foreach($l as $key => $val){
                array_push($allroomslist, $val);
            }
        }
        ///////////////////////////////////////////////////
        $totalrooms=DB::table('room_types')->count();
        //$availablerooms


        $bookedrooms=array();
        $allrerooms= DB::table('reservations')
            //->join('reservation_nights', 'reservations.id', '=', 'reservation_nights.reservation_id')
            ->select('reservations.room_type_id')
            ->Where('reservations.status',1)
          //  ->groupBy('reservation_nights.room_type_id')
            ->get();

        foreach($allrerooms as $l){
            foreach($l as $key => $val){
                array_push($bookedrooms, $val);
            }
        }




        $availablerooms = DB::table('room_types')

            ->select(['room_types.id','room_types.name', 'room_types.short_name','room_types.base_price','room_types.description','room_types.max_occupancy'])

            ->whereNotIn('room_types.id',$bookedrooms)->get()->count();

$occupiedrooms=DB::table('reservations')
           // ->join('reservations', 'reservations.id', '=', 'reservation_nights.reservation_id')
            ->select('room_type_id')
            ->where('reservations.status',1)
            ->distinct()
            ->whereIn('room_type_id',$allroomslist)
            ->get()->count();



       // number of guest
        $number_of_guest=DB::table('reservations')
            ->where('status',1)
            ->get()
            ->count();
      $checkouttoday=DB::table('reservations')
          ->where('checkout', date('Y-m-d'))
          ->get()
          ->count();
      $unspecifiedcheckoutdate=DB::table('reservations')
          ->where('checkout','')
          ->get()
          ->count();

      $upcomingcheckout= DB::table('reservations')
          ->join('customers', 'customers.id', '=', 'reservations.customer_id')
          //->join('reservation_nights', 'reservation_nights.reservation_id', '=', 'reservations.id')
          ->join('room_types', 'reservations.room_type_id', '=', 'room_types.id')
          ->select(DB::raw('CONCAT_WS(" ",customers.first_name,customers.last_name) as wholename'),
              'reservations.customer_id', 'reservations.status','reservations.updated_at','reservations.checkin','reservations.checkout',
              'customers.first_name',
              'customers.last_name',
              'reservations.room_type_id','customers.email','customers.telephone',
              'room_types.name')
          ->groupBy('reservations.room_type_id')
          ->orderBy('reservations.checkout', 'desc')
          ->where('reservations.status',1)
          ->take(5)
          ->get();
//dd($upcomingcheckout);
        $unspecifiedcheckout=DB::table('reservations')
            ->join('customers', 'customers.id', '=', 'reservations.customer_id')
            //->join('reservation_nights', 'reservation_nights.reservation_id', '=', 'reservations.id')
            ->join('room_types', 'reservations.room_type_id', '=', 'room_types.id')
            ->select(DB::raw('CONCAT_WS(" ",customers.first_name,customers.last_name) as wholename'),
                'reservations.customer_id', 'reservations.status','reservations.updated_at','reservations.checkin','reservations.checkout',
                'customers.first_name',
                'customers.last_name',
                'reservations.room_type_id','customers.email','customers.telephone',
                'room_types.name')
            ->groupBy('reservations.room_type_id')
            ->orderBy('reservations.checkin', 'desc')
            ->where('reservations.status',1)
            ->where('reservations.checkout','')
            ->take(5)
            ->get();
        return view('sitepages.dashboard')->with(
            ['totalrooms'=>$totalrooms,
             'occupiedrooms'=>$occupiedrooms,
             'availablerooms'=>$availablerooms,
             'number_of_guest'=>$number_of_guest,
              'checkouttoday'=>$checkouttoday,
                'unspecifiedcheckoutdate'=>$unspecifiedcheckoutdate,
                'upcomingcheckout'=>$upcomingcheckout,
                'unspecifiedcheckout'=>$unspecifiedcheckout
            ]
        );
        //return view('sitepages.dashboard');
    }
}

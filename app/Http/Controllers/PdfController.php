<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
Use PDF;

use DB;
use Datatables;
use Illuminate\Support\Collection;

class PdfController extends Controller
{
    //
    public function pdfinvoice($customerid,$invoicenumber){
////////////////HOTEL DETAILS//////////////
        $hoteldetails =  DB::table('profiles')->first();
 /////////////////CUSTOMER DETAILS/////////////////////////////
        $customerdetails=DB::table('customers') ->where([
                       ['id', '=', $customerid],
        ])->first();
   /////////////////INVOICE DATE////////////////////////////////////////////     
        $invoicedate=DB::table('invoices')
            ->where([
                ['invoicenumber', '=',$invoicenumber],
                ['customerid', '=', $customerid],
            ])->first();
     //////////////////invoice  items/////////////////////////////   
        $items_oninvoice = DB::table('invoices')
            ->where([
                ['invoicenumber', '=',$invoicenumber],
                ['customerid', '=', $customerid],
            ]) ->get();
        /////////////////////////////////////////////////////
        $html=view('pdfs.invoicepdf')->with([
            'customerdetails'=>$customerdetails,
            'customerid'=>$customerid,
            'invoicenumber'=>$invoicenumber,
            'hoteldetails'=>$hoteldetails,
            'invoicedate'=>$invoicedate, 
            'items_oninvoice'=>$items_oninvoice
        ])->render();
        return PDF::load($html)->download();

    }







    public function pdfreceiptpayment($customerid,$invoicenumber,$receipt){
////////////////HOTEL DETAILS//////////////
        $hoteldetails =  DB::table('profiles')->first();
////////////////////////PAYMENT//////////////////////
        $paymentdetails=DB::table('payments')->where([['invoicenumber', 'like', '%'.$invoicenumber.'%'],
            ['customerid', '=', $customerid],
            ['receiptnumber', '=', $receipt],
        ])->get();
        /////////////////CUSTOMER DETAILS/////////////////////////////
        $customerdetails=DB::table('customers') ->where([
            ['id', '=', $customerid],
        ])->first();
        /////////////////INVOICE DATE////////////////////////////////////////////
        $invoicedate=DB::table('invoices')
            ->where([
                ['invoicenumber', '=',$invoicenumber],
                ['customerid', '=', $customerid],
            ])->first();
///////////////////////////////////////////////////
        $html = view('pdfs.paymentreceiptpdf')->with([

            'customerid'=>$customerid,
            'invoicenumber'=>$invoicenumber,
            'receipt'=>$receipt,
            'paymentdetails'=>$paymentdetails,
            'hoteldetails'=>$hoteldetails,
            'customerdetails'=>$customerdetails,
            'invoicedate'=>$invoicedate,
        ])->render();

        return PDF::load($html)
            ->download();
    }
    public function eventpdf($event_id){
        $id=$event_id;
        $event = DB::table('events')->where('id',$id)->get();
     
        $html = view('pdfs.eventplanpdf')->with('event',$event)->render();

        return PDF::load($html)
            ->download();
    }
    public function eventbudgetpdf($event_id){
        $id=$event_id;
        $eventbudget = DB::table('eventbudgets')->where('eventid',$id)->get();
      if (!$eventbudget->isEmpty()) {
            $html = view('pdfs.eventbudgetpdf')->with('eventbudget',$eventbudget)->render();
             return PDF::load($html)
                ->download();
        }else{
            $notification=array(
                'message'=>"Budget not prepared ",
                'alert-type'=>'info'
            );
            return redirect('event/budgetevent')->with($notification);
        }

    }
}

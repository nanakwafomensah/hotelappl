<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use hotelapp\Invoice;
use DB;
use Datatables;
use Illuminate\Support\Collection;


class BillingController extends Controller
{
    //
    public function index(){
        return view('sitepages.billing');
    }
    public function getbills()
    {
        $bills = DB::table('invoices')
            ->join('customers', 'customers.id', '=', 'invoices.customerid')

            ->select([DB::raw('SUM(invoices.amount) as a_mount'),DB::raw('CONCAT_WS(" ",customers.first_name,customers.last_name) as wholename'),'invoices.invoicenumber', 'invoices.customerid', 'invoices.invoicedate', 'invoices.created_at', 'invoices.updated_at'])
            ->groupBy(['invoices.invoicenumber','invoices.customerid']);


        return Datatables::of($bills)
            ->addColumn('action', function ($bills) {
                $viewurl = "viewinvoice/" . $bills->customerid . "/" . "$bills->invoicenumber";
                $editurl = "editinvoice/" . $bills->customerid . "/" . "$bills->invoicenumber";
                $pdfurl="pdfinvoice/".$bills->customerid . "/" . "$bills->invoicenumber";
                return'<a href="'.$viewurl.'" data-invoicenumber="'.$bills->invoicenumber.'" data-customerid="'.$bills->customerid.'" data-toggle="tooltip" title="Quick view"><span class="glyphicon glyphicon-search"></span></a>&nbsp
                       <a href="'.$editurl.'" data-invoicenumber="'.$bills->invoicenumber.'" data-customerid="'.$bills->customerid.'" id="invoice_auto_load" data-toggle="tooltip" title="Edit"> <span class="glyphicon glyphicon-pencil"></span></a> &nbsp
                       <a href="'.$pdfurl.'" data-invoicenumber="'.$bills->invoicenumber.'" data-customerid="'.$bills->customerid.'" id="invoice_auto_load" data-toggle="tooltip" title="Print"> <span class="glyphicon glyphicon-print"></span></a> &nbsp
                       <a href="payment" data-invoicenumber="'.$bills->invoicenumber.'" data-customerid="'.$bills->customerid.'" id="invoice_auto_load" data-toggle="tooltip" title="Make Payment"> <span class="glyphicon glyphicon-usd"></span></a> &nbsp
                       <a href="" data-invoicenumber="'.$bills->invoicenumber.'" data-customerid="'.$bills->customerid.'" id="invoice_auto_load" data-toggle="tooltip" title="Send email"> <span class="glyphicon glyphicon-envelope"></span></a>
                        ';
            })
            ->addColumn('owing', function ($bills) {
                $allpay = DB::table('payments')
                    ->where([
                        ['invoicenumber', 'like', '%'.$bills->invoicenumber.'%'],
                        ['customerid', '=', $bills->customerid],
                    ])
                    ->sum('amount');
                $owing='';
                 if($bills->a_mount > $allpay){
                     $owing= $bills->a_mount-$allpay;
                 }else{
                     $owing='0';
                 }

                return number_format($owing);
            })

            ->addColumn('excess', function ($bills) {
                $allpay = DB::table('payments')
                    ->where([
                        ['invoicenumber', 'like', '%'.$bills->invoicenumber.'%'],
                        ['customerid', '=', $bills->customerid],
                    ])
                    ->sum('amount');
                $excess='';
                if($allpay >$bills->a_mount ){
                  $excess=$allpay-$bills->a_mount;
                }else{
                    $excess='0';
                }
                return number_format($excess);

            })
            ->make(true);


    }
    public function getowing($customerid,$invoicenumber){
        $owing = DB::table('payments')


            ->select(DB::raw('SUM(payments.amount) as amount'))
            ->where(['payments.invoicenumber'==$invoicenumber,'payments.customerid'==$payments.customerid])
            ->groupBy(['payments.invoicenumber','payments.customerid'])->get();

        return $owing;
    }
}

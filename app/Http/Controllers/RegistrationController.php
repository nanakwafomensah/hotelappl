<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use hotelapp\User;
use Mail;
use hotelapp\Http\Requests\Online_userRequest;
class RegistrationController extends Controller
{
    //
    public function register(){

        return view('authentication.online.signup'); 
    }
    public function addregistration(Online_userRequest $request){
       // is validated in the request class
        
        $credentials=[
           'first_name'=>$request->firstname,
            'last_name'=>$request->lastname,
            'email'=>$request->email,
            'password'=>$request->password,
            'location'=>$request->location,
        ];
        $user = Sentinel::register($credentials);
        $activation =Activation::create($user);
        $role=Sentinel::findRoleBySlug('client');
        $role->users()->attach($user);
        $this->sendEmail($user,$activation->code);
        return redirect('register');
    }
    private function sendEmail($user,$code){
        Mail::send('authentication.online.emails.activation',
            [
                'user'=>$user,
                'code'=>$code
            ],function($message)use($user){
                $message ->to($user->email);
                $message ->subject("Hello $user->first_name,
                activate your account.");
            });
    }
}

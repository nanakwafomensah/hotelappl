<?php

namespace hotelapp\Http\Controllers;

use hotelapp\Http\Requests\Online_loginRequest;
use Illuminate\Http\Request;
use Sentinel;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
class LoginController extends Controller
{
    //
    public function login(){
        return view('authentication.online.login');
    }
    public function postlogin(Online_loginRequest $request){
        //has the validations in the request
        try{
            if(Sentinel::authenticate($request->all())){
                if(Sentinel::check()){
                    return redirect('onlinebook');
                }
            }
            else{
                return redirect()->back()->with(['error'=>'wrong credentials']);
            }

        }catch(ThrottlingException $e){
            $delay=$e->getDelay();
            return redirect()->back()->with(['error'=>"You are banned for $delay seconds."]);
        }catch (NotActivatedException $e){
            return redirect()->back()->with(['error'=>"Your account has not been activated"]);
        }

      
    }
    public function postlogout(){
         Sentinel::logout();
        return redirect('login');
    }
}

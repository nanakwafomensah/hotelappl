<?php

namespace hotelapp\Http\Controllers;


use Illuminate\Http\Request;
use hotelapp\User;
use Activation;
use Sentinel;

class ActivationController extends Controller
{

    public function activate($email,$activationCode){
           $user=User::whereEmail($email)->first();
          $sentineluser=Sentinel::findById($user->id);
            if(Activation::complete($sentineluser,$activationCode)){
                  return redirect('login');
            }else{

            }
    }
    


}

<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Datatables;
use Sentinel;
use hotelapp\Task;
use Carbon\Carbon;

class TaskController extends Controller
{
    //
    public function index(){
        
        return view('sitepages.newtask');
    }
    public function completedtask(){

        return view('sitepages.completedtask');
    }

    
    public function gettasklist(){
        $taskdetails = DB::table('tasks')
            ->select('id','task','created_at','updated_at')
             ->where('status','=','incomplete');
        

        return Datatables::of($taskdetails)
            ->addColumn('action', function ($taskdetails) {

                return'<a href="" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal1" data-id="'.$taskdetails->id.'"  data-task="'.$taskdetails->task.'" ><i class="fa fa-edit" aria-hidden="true">&nbsp;</i></a>&nbsp
                       <a href="" data-toggle="modal" data-target="#delModal"  data-id="'.$taskdetails->id.'"  data-task="'.$taskdetails->task.'" class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                       <a href="" data-toggle="modal" data-target="#Modal2"  data-id="'.$taskdetails->id.'"  data-task="'.$taskdetails->task.'" class="completebtn btn btn-success" > <i class="fa fa-check-square" aria-hidden="true">&nbsp;</i></a>
                    ';
            })
            
            ->make(true);
    }
public function getcompletedtask(){
    $taskdetails = DB::table('tasks')
        ->select('id','task','created_at','updated_at','completiontime')
        ->where('status','=','completed');


    return Datatables::of($taskdetails)
        ->addColumn('action', function ($taskdetails) {

            return'<a href="" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal1" data-id="'.$taskdetails->id.'"  data-task="'.$taskdetails->task.'" ><i class="fa fa-edit" aria-hidden="true">&nbsp;</i></a>&nbsp
                       <a href="" data-toggle="modal" data-target="#delModal"  data-id="'.$taskdetails->id.'"  data-task="'.$taskdetails->task.'" class="deletebtn btn btn-danger" ><i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                       <a href="" data-toggle="modal" data-target="#Modal2"  data-id="'.$taskdetails->id.'"  data-task="'.$taskdetails->task.'" class="completebtn btn btn-success" > <i class="fa fa-check-square" aria-hidden="true">&nbsp;</i></a>
                    ';
        })

        ->make(true);
}

    public function createtask(Request $request){
        $newtask = new Task();
        $newtask->task=$request->task;
        $newtask->status='incomplete';//
        $newtask->user=Sentinel::getUser()->first_name ." ".Sentinel::getUser()->last_name;

        if($newtask->save()){
            return Response("success");
        }
        else{
            return Response("fail");
        }

        
    }
    public function updatetask(Request $request){
        $task = Task::findorfail($request->taskid);
        $task->task=$request->task;
        $notification=array(
            'message'=>"Task has being succesfully being updated",
            'alert-type'=>'success'
        );
        if($task->update()){
            return redirect('newtask')->with($notification);
        }

    }
    public function deletetask(Request $request){
        $task = Task::findorfail($request->taskiddd);
        $notification=array(
            'message'=>"Task has being succesfully being removed",
            'alert-type'=>'success'
        );
        if($task->delete()){
            return redirect('newtask')->with($notification);
        }
    }
    public function complete_a_task(Request $request){
        $mytime = Carbon::now();
       
        $task = Task::findorfail($request->taskidww);

        $task->status='completed';
        $task->completiontime= $mytime->toDateTimeString();
        $notification=array(
            'message'=>"Task succesfully Completed",
            'alert-type'=>'success'
        );
        if($task->update()){
            return redirect('newtask')->with($notification);
        }
    }
}

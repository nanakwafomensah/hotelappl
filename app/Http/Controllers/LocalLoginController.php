<?php

namespace hotelapp\Http\Controllers;

use hotelapp\Http\Requests\Local_loginRequest;
use Illuminate\Http\Request;
use Sentinel;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use DB;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use hotelapp\Profile;

class LocalLoginController extends Controller
{
    //
    public function login(){

        return view('authentication.local.login');
    }
    public function postlogin(Local_loginRequest $request){
        //has the validations in the request
      try{
          $hoteldetails =  DB::table('profiles')->first();
          $hotelimage=null;
            if(!empty($hoteldetails)){
                $hotelimage=$hoteldetails->avatar;
            }

          if(Sentinel::authenticate($request->all())) {
              if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug=='frontdesk'){
                  $role='frontdesk';
                  return redirect('frontdashboard')->with(['role'=>$role,'hotelimage'=>$hotelimage]);
              }
              else if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug=='Admin'){
                  $role='Admin';

                  return redirect('dashboard')->with(['role'=>$role,'hotelimage'=>$hotelimage]);
              }
              else{
                  return redirect('/');
              }
          }else{
              return redirect()->back()->with(['error'=>'wrong credentials']);
          }
      }catch(ThrottlingException $e){
          $delay=$e->getDelay();
          return redirect()->back()->with(['error'=>"You are banned for $delay seconds."]);

      }catch (NotActivatedException $e){
          return redirect()->back()->with(['error'=>"Your account has not been activated"]);
      }



    }
    public function postlogout(){
        Sentinel::logout();
        return redirect('/');
    }
}

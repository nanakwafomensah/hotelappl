<?php

namespace hotelapp\Http\Controllers;

use Illuminate\Http\Request;
use hotelapp\Event;
use hotelapp\Eventbudget;
use DB;
use Datatables;


class EventController extends Controller
{
    //
    public function closeevent(){
        return view('sitepages.closeevent');  
    }
    public function budgetindex($eventid=null)
    {
        $listofevents=DB::table('events')->select(['id','eventname'])->get();
//        if($eventid != null){
//
//            return view('sitepages.budgetevent')->with(['eventid'=>$eventid,'listofevents'=>$listofevents]);
//        }else{
//            return view('sitepages.budgetevent')->with(['eventid'=>0,'listofevents'=>$listofevents]);
//        }
        return view('sitepages.budgetevent')->with(['listofevents'=>$listofevents]);
    }
    public  function index(){
        return view('sitepages.newevent');
    }
    public function getevent(Request $request){
        $events=DB::table('events')->where('id',$request->eventid)->first();
          return response()->json($events);
    }
    public function plan($eventid= null){
        $listofevents=DB::table('events')->select(['id','eventname'])->get();
        if($eventid != null){

            return view('sitepages.planevent')->with(['eventid'=>$eventid,'listofevents'=>$listofevents]);
        }else{
            return view('sitepages.planevent')->with(['eventid'=>0,'listofevents'=>$listofevents]);
        }

    }

    public function postplanevent(Request $request)
    {
            $data=Event::findorfail($request->eventid);
            $data->size=$request->size;
            $data->gender_mix=$request->gender_mix;
            $data->age_of_attendees=$request->age_of_attendees;
            $data->special_needs=$request->special_needs;
            $data->food_requirements=$request->food_requirements;
            $data->schedule_speakers=$request->schedule_speakers;
            $data->update();
            $notification=array(
                'message'=>"Plan details successfully added",
                'alert-type'=>'success'
            );
        return redirect()->back()->with($notification);
    }
    public function store(Request $request){
    
        $event=new Event();
        $event->eventname=$request->eventname;
        $event->purpose=$request->purpose;
        $event->eventtype=$request->eventtype;
        $event->startdate=$request->startdate;
        $event->enddate=$request->enddate;
        $event->coordinator1=$request->coordinator1;
        $event->coordinator2=$request->coordinator2;
        $event->coorphone1=$request->coorphone1;
        $event->coorphone2=$request->coorphone2;
        $event->email=$request->email;


        //TODO: Send sms on to event coordinator
        $sms_message="hello ";
        $msg = urlencode($sms_message);
        $senderid = 'hotelname';
        $sms_pass ='p@$$w0rd';
        $sms_username = 'nalo_kwafo';

        $api = 'http://api.nalosolutions.com/bulksms/?username=' . $sms_username . '&password=' . $sms_pass . '&type=0&dlr=1&destination=' . $request->coorphone1.','.$request->coorphone2 . '&source=' . $senderid . '&message=' . $msg;
        @file_get_contents($api);

        $event->save();
        $notification=array(
            'message'=>"A new Event has been Succesfully added",
            'alert-type'=>'success'
        );
        return redirect()->back()->with($notification);
    }

    public function update(Request $request){
        //dd($request->all());
        $event = Event::findorfail($request->updateeventid);
        $event->eventname=$request->updateeventname;
        $event->purpose=$request->updatepurpose;
        $event->eventtype=$request->updateeventtype;
        $event->startdate=$request->updatestartdate;
        $event->enddate=$request->updateenddate;
        $event->coordinator1=$request->updatecoordinator1;
        $event->coordinator2=$request->updatecoordinator2;
        $event->coorphone1=$request->updatecoorphone1;
        $event->coorphone2=$request->updatecoorphone2;
        $event->email=$request->updateemail;
        $event->update();
         $notification=array(
            'message'=>"A $request->updateeventname Event has been Updated",
            'alert-type'=>'success'
        );
        return redirect()->back()->with($notification);

    }
    public function  eventsdetails(){
        $eventdetails = DB::table('events')->select(['id','eventname','purpose', 'eventtype','coordinator1','coordinator2','coorphone1','coorphone2','email']);

        return Datatables::of($eventdetails)
            ->addColumn('action', function ($eventdetails) {

 $planurl="event/planevent/".$eventdetails->id;
                return'<a href="" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal1" data-id="'.$eventdetails->id.'"   data-email="'.$eventdetails->email.'"   ><i class="fa fa-edit" aria-hidden="true">&nbsp;</i></a>&nbsp
                      <a href="" data-toggle="modal" data-target="#delModal"  data-eventid="'.$eventdetails->id.'"  class="deletebtn btn btn-danger" > <i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                      <a href="'.$planurl.'"  data-eventid="'.$eventdetails->id.'"  class=" btn btn-info" > <i class="fa fa-forward" aria-hidden="true">&nbsp;</i></a>
                                ';
            })

            ->addColumn('coordinator', function ($eventdetails) {
                 return'<ul><li>'."<strong>".$eventdetails->coordinator1. "(" .$eventdetails->coorphone1.")"."</strong>".'</li>
                            <li>'."<strong>".$eventdetails->coordinator2. "(" .$eventdetails->coorphone2.")"."</strong>".'</li>
                 </ul>';

            })

            ->make(true);
    }
    public function event_budget(Request $request){
          if($request->type=="others"){

                

              $eventb=new Eventbudget();
              $eventb->categories=$request->category_others;
              $eventb->projected_subtotal=$request->projected_others;
              $eventb->actual_subtotal=$request->actual_others;
              $eventb->comment=$request->comment_others;
              $eventb->eventid=$request->eventid;
              $eventb->budget_type=$request->type;
              $eventb->save();
              return "success";


          }elseif($request->type=="refreshment"){
              $eventb=new Eventbudget();
              $eventb->categories=$request->category_refreshment;
              $eventb->projected_subtotal=$request->projected_refreshment;
              $eventb->actual_subtotal=$request->actual_refreshment;
              $eventb->comment=$request->comment_refreshment;
              $eventb->eventid=$request->eventid;
              $eventb->budget_type=$request->type;
              $eventb->save();
              return "success";
          }elseif ($request->type=="socialmedia"){
              $eventb=new Eventbudget();
              $eventb->categories=$request->category_sm;
              $eventb->projected_subtotal=$request->projected_sm;
              $eventb->actual_subtotal=$request->actual_sm;
              $eventb->comment=$request->comment_sm;
              $eventb->eventid=$request->eventid;
              $eventb->budget_type=$request->type;
              $eventb->save();
              return "success";
          }elseif ($request->type=="eventp"){
              $eventb=new Eventbudget();
              $eventb->categories=$request->category_eventp;
              $eventb->projected_subtotal=$request->projected_eventp;
              $eventb->actual_subtotal=$request->actual_eventp;
              $eventb->comment=$request->comment_eventp;
              $eventb->eventid=$request->eventid;
              $eventb->budget_type=$request->type;
              $eventb->save();
              return "success";
          }elseif ($request->type=="decor"){
              $eventb=new Eventbudget();
              $eventb->categories=$request->category_decor;
              $eventb->projected_subtotal=$request->projected_decor;
              $eventb->actual_subtotal=$request->actual_decor;
              $eventb->comment=$request->comment_decor;
              $eventb->eventid=$request->eventid;
              $eventb->budget_type=$request->type;
              $eventb->save();
              return "success";
          }elseif ($request->type=="publicrelations"){
              $eventb=new Eventbudget();
              $eventb->categories=$request->category_publicrelations;
              $eventb->projected_subtotal=$request->projected_publicrelations;
              $eventb->actual_subtotal=$request->actual_publicrelations;
              $eventb->comment=$request->comment_publicrelations;
              $eventb->eventid=$request->eventid;
              $eventb->budget_type=$request->type;
              $eventb->save();
              return "success";
          }elseif ($request->type=="travel"){
              $eventb=new Eventbudget();
              $eventb->categories=$request->category_travel;
              $eventb->projected_subtotal=$request->projected_travel;
              $eventb->actual_subtotal=$request->actual_travel;
              $eventb->comment=$request->comment_travel;
              $eventb->eventid=$request->eventid;
              $eventb->budget_type=$request->type;
              $eventb->save();
              return "success";
          }elseif ($request->type=="venue"){
              $eventb=new Eventbudget();
              $eventb->categories=$request->category_venue;
              $eventb->projected_subtotal=$request->projected_venue;
              $eventb->actual_subtotal=$request->actual_venue;
              $eventb->comment=$request->comment_venue;
              $eventb->eventid=$request->eventid;
              $eventb->budget_type=$request->type;
              $eventb->save();
              return "success";
          }

      }


  public function budgetcategoryexit(Request $request)
  {
      $budgetexit = DB::table('eventbudgets')->where('eventid', $request->eventid)
                                             ->where('budget_type',$request->budget_type)->get();
      if (!$budgetexit->isEmpty())
      {
          DB::table('eventbudgets')->where('eventid', $request->eventid)
              ->where('budget_type', $request->budget_type)->delete();
              return 'removed';
      }
      else
      {
          return 'empty';
      }

  }



    public function get_all_planed_event(Request $request){
        $eventdetails = DB::table('events')
                       ->select(['events.id','events.eventname','events.purpose', 'events.eventtype','events.coordinator1','events.coordinator2','events.coorphone1','events.coorphone2','events.email',DB::raw('SUM(eventbudgets.projected_subtotal) as totalprojected'),DB::raw('SUM(eventbudgets.actual_subtotal) as totalactual')])
                       ->join('eventbudgets', 'events.id', '=', 'eventbudgets.eventid')
                        ->groupBy('eventbudgets.eventid')->get();

        return Datatables::of($eventdetails)
            ->addColumn('action', function ($eventdetails) {

                $planurl="event/planevent/".$eventdetails->id;
                return'<a href="" class="edit-modal btn btn-sm btn-warning" data-target="#edit-modal" data-toggle="modal" data-toggle="tooltip" title="approve"  data-id="'.$eventdetails->id.'"   data-email="'.$eventdetails->email.'"   ><i class="fa fa-check" aria-hidden="true">&nbsp;</i></a>&nbsp
                      <a href="'.$planurl.'" data-toggle="tooltip" title="Redo"    data-eventid="'.$eventdetails->id.'"  class="deletebtn btn btn-danger" > <i class="fa fa-repeat" aria-hidden="true">&nbsp;</i></a>
                      <a href=""  data-eventid="'.$eventdetails->id.'"  class=" btn btn-info"  data-toggle="tooltip" title="Preview" > <i class="fa fa-download" aria-hidden="true">&nbsp;</i></a>
                                ';
            })
//
            ->addColumn('coordinator', function ($eventdetails) {
                return'<ul><li>'."<strong>".$eventdetails->coordinator1. "(" .$eventdetails->coorphone1.")"."</strong>".'</li>
                            <li>'."<strong>".$eventdetails->coordinator2. "(" .$eventdetails->coorphone2.")"."</strong>".'</li>
                 </ul>';

            })
            ->addColumn('projectedtotal', function ($eventdetails) {
                return 'GHC '.$eventdetails->totalprojected;

            })
            ->addColumn('actualtotal', function ($eventdetails) {
                return 'GHC '.$eventdetails->totalactual;

            })

            ->make(true);
    }
}

<?php

namespace hotelapp\Http\Controllers;

use hotelapp\Http\Requests\Local_userRequestedit;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Sentinel;
use hotelapp\Http\Requests\Local_userRequest;
use Illuminate\Support\Facades\Response;
use Datatables;
use DB;
use Cartalyst\Sentinel\Users\EloquentUser;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Mail;
use hotelapp\User;
use Reminder;
use hotelapp\Profile;


class LocalRegistrationController extends Controller
{
    //


    public function register(){

        return view('authentication.local.signup');
    }
    public function addregistration(Local_userRequest $request){
         try{
             $credentials=[
                 'first_name'=>$request->firstname,
                 'last_name'=>$request->lastname,
                 'email'=>$request->email,
                 'password'=>$request->password,
                 'location'=>$request->location,
                 'passwordagain'=>$request->passwordagain,
             ];
             $user = Sentinel::registerAndActivate($credentials);
             $role=Sentinel::findRoleBySlug($request->userrole);
             $role->users()->attach($user);
             $notification=array(
                 'message'=>"$request->firstname has been Succesfully added",
                 'alert-type'=>'success'
             );
             return redirect('new_user')->with($notification);
         } catch(Exception $e){
             $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('new_user')->with($notification);
         }


    }
    public function userdetails()
    {
        $userdetails = DB::table('users')
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'roles.id', '=', 'role_users.role_id')
            ->select('users.id','users.first_name','users.last_name','users.location','users.email','roles.name as priviledge','users.passwordagain','users.last_login')
            ->get();

      return Datatables::of($userdetails)
          ->addColumn('action', function ($userdetails) {
              $details='';
             $details='<a href="" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal1" data-id="'.$userdetails->id.'"  data-firstname="'.$userdetails->first_name.'" data-lastname="'.$userdetails->last_name.'" data-location="'.$userdetails->location.'" data-priviledge="'.$userdetails->priviledge.'"  data-email="'.$userdetails->email.'" >Edit</a>&nbsp
                    <a href="" data-toggle="modal" data-target="#delModal"  data-id="'.$userdetails->id.'"data-firstname="'.$userdetails->first_name.'" data-lastname="'.$userdetails->last_name.'" class="deletebtn btn btn-danger" > Delete user</a>&nbsp';
                   if($userdetails->priviledge=='admin'){
                     $details.='<a href="" data-toggle="modal" data-target="#forgotpasswordModal"  data-id="'.$userdetails->id.'"data-firstname="'.$userdetails->first_name.'" data-lastname="'.$userdetails->last_name.'" class="forgotpasswordbtn btn btn-success" data-email="'.$userdetails->email.'" >Change password</a>&nbsp';
                   }
//            
//
              return $details;
          })
          ->make(true);
    }
    
    public function updateregisteredUser(Local_userRequestedit $request){
       try{
           $user = Sentinel::findById($request->userid);
           $credentials = [
               'first_name' => $request->firstname,
               'last_name'=>$request->lastname,
               'email'=>$request->email,
               
               'location'=>$request->location,
           ];
           $user = Sentinel::update($user, $credentials);
           $currentrole= Sentinel::findById($request->userid)->roles()->get();
           $role=Sentinel::findRoleBySlug($currentrole[0]['slug']);
           if( $role->users()->detach($user)){
               $role=Sentinel::findRoleBySlug($request->role);
               $role->users()->attach($user);
           }
           $notification=array(
               'message'=>"$request->firstname  details has been Succesfully updated",
               'alert-type'=>'success'
           );
           return redirect('new_user')->with($notification);

       }catch(Exception $e){
           $notification=array(
               'message'=>'A Error Occured',
               'alert-type'=>'error'
           );
           return redirect('new_user')->with($notification);
       }

    }
    public function userdelete(Request $request){
      try{
     $user = Sentinel::findById($request->userid);
     $firstname=$request->firstname;
     $user->delete();
     $notification=array(
         'message'=>"User has been Succesfully removed",
         'alert-type'=>'success'
     );
     return redirect('new_user')->with($notification);
       }catch(Exception $e){}
        $notification=array(
            'message'=>'A Error Occured',
            'alert-type'=>'error'
        );
        return redirect('new_user')->with($notification);
    }
    public function postresetpassword(Request $request,$email,$resetCode){
        $this->validate($request,[
            'password'=>'required',
            'password_confirmation'=>'required'
            ]);
        $user=User::byEmail($email);

        if(count($user)==0)
            abort(404);
        $sentinelUser=Sentinel::findById($user->id);
        if($reminder=Reminder::exists($sentinelUser)){
            if($resetCode==$reminder->code){
                      Reminder::complete($sentinelUser,$resetCode,$request->password);
                       return redirect('login_local')->with('success','Please login with your new password.');
            }
            else
                return redirect('login_local');
        }else{
            return redirect('login_local');
        }
        return $user;
    }
    public function resetpassword($email,$resetCode){
       $user=User::byEmail($email);

        if(count($user)==0)
            abort(404);
        $sentinelUser=Sentinel::findById($user->id);
        if($reminder=Reminder::exists($sentinelUser)){
            if($resetCode==$reminder->code)
                 return view('authentication.local.reset_password');
             else
                 return redirect('login_local');
        }else{
            return redirect('login_local');
        }
         return $user;
    }

    public function forgotpassword(Request $request){
        $user= User::whereEmail($request->email)->first();
        $sentinelUser=Sentinel::findById($user->id);
        if(count($user)==0)
            return redirect()->back()->with([
                'success'=>'Reset code was sent to your email'
            ]);
        $reminder=Reminder::exists($sentinelUser)?:Reminder::create($sentinelUser);
        $profile = Profile::where('id', 1)->first();
      if($this->sendEmail($user,$reminder->code,$profile)){
          return Response("success");
      }else{
          return Response("fail");
      }



    }

    private function sendEmail($user,$code,$profile){

        Mail::send('sitepages.emails.forgotpassword',[
            'user'=>$user,
            'code'=>$code
        ],function($message)use($user,$profile){
            $message->from($profile->email, $profile->name);
            $message->to($user->email);
            $message->subject("Reset your password");
        });
    }


}

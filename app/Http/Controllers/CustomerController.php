<?php

namespace hotelapp\Http\Controllers;

use hotelapp\Customer;
use Illuminate\Http\Request;
use DB;
use Datatables;

class CustomerController extends Controller
{
    //
    public function index(){
        return view('sitepages.newcustomer');
    }
    public function customersdetails(){
        $customerdetails = DB::table('customers')->select(['customers.id','customers.first_name', 'customers.last_name','customers.email','customers.telephone','customers.wholename','customers.location']);

        return Datatables::of($customerdetails)
            ->addColumn('action', function ($customerdetails) {
                $book= "specialbook";
                // return '<a href="#edit-'.$questions->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                return'<a href="" class="edit-modal btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal1" data-customerid="'.$customerdetails->id.'"  data-firstname="'.$customerdetails->first_name.'"   data-lastname="'.$customerdetails->last_name.'"  data-email="'.$customerdetails->email.'" data-telephone="'.$customerdetails->telephone.'"  ><i class="fa fa-edit" aria-hidden="true">&nbsp;</i></a>&nbsp
                                           <a href="" data-toggle="modal" data-target="#delModal"  data-customerid="'.$customerdetails->id.'"  data-firstname="'.$customerdetails->first_name.'" class="deletebtn btn btn-danger" > <i class="fa fa-trash" aria-hidden="true">&nbsp;</i></a>
                                          <a href="'.$book.'"  data-customerid="'.$customerdetails->id.'"  data-firstname="'.$customerdetails->first_name.'" class="deletebtn btn btn-success" ><i class="fa fa-bed" aria-hidden="true">&nbsp;</i></a>
                                ';
            })

            ->make(true);

    }
    public function postnewcustomer(Request $request){
         try{
             $customer = new Customer();

             $customer->first_name=$request->firstname;
             $customer->last_name=$request->lastname;
             $customer->wholename=$request->firstname ." ".$request->lastname;
             $customer->email=$request->email;
             $customer->telephone=$request->telephone;
             $customer->location=$request->location;
             if($customer->save()){
                 $notification=array(
                     'message'=>"New Customer has been Succesfully Added",
                     'alert-type'=>'success'
                 );
                 
                 return redirect('newcustomer')->with($notification);
             }
         }catch(Exception $e){
             $notification=array(
                 'message'=>'A Error Occured',
                 'alert-type'=>'error'
             );
             return redirect('newcustomer')->with($notification);
         }
        
    }
    public function editcustomer(Request $request){

        try{$customer = Customer::findorfail($request->customerid);
            $customer->first_name=$request->firstname;
            $customer->last_name=$request->lastname;
            $customer->wholename=$request->firstname ." ".$request->lastname;
            $customer->email=$request->email;
            $customer->telephone=$request->telephone;
            $customer->location=$request->location;


            if($customer->update()){

                $notification=array(
                    'message'=>"Customer details has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('newcustomer')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Customer details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('newcustomer')->with($notification);
        }

    }
    public function deletecustomer(Request $request){
           // dd($request->customerid);
        try{
            $customer = Customer::findorfail($request->customerid);
            if($customer->delete()){
                $notification=array(
                    'message'=>"Customer has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('newcustomer')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('newcustomer')->with($notification);
        }
    }
    public function archivedguest(){
        return view('sitepages.archivedguest');
    }
    public function guestlist(){
        return view('sitepages.guestlist');
    }
    public function guestlistdetails(){

        $guestlistdetails=DB::table('reservations')
            ->join('customers', 'customers.id', '=', 'reservations.customer_id')
            //->join('reservation_nights', 'reservation_nights.reservation_id', '=', 'reservations.id')
            ->join('room_types', 'reservations.room_type_id', '=', 'room_types.id')
            ->select(DB::raw('CONCAT_WS(" ",customers.first_name,customers.last_name) as wholename'),
                'reservations.customer_id', 'reservations.status','reservations.checkin','reservations.checkout',
                'customers.first_name',
                'customers.last_name','customers.email','customers.telephone',
                'reservations.room_type_id',
                'room_types.name')
            ->groupBy('reservations.room_type_id')
            ->having('status', '=',1)
            ->get();



        return Datatables::of($guestlistdetails)
            ->addColumn('action', function ($guestlistdetails) {
                return'<a href="" class="delModal btn btn-sm btn-success" data-toggle="modal" data-target="#delModal" data-customerid="'.$guestlistdetails->customer_id.'" data-wholename="'.$guestlistdetails->wholename.'" ><span class="glyphicon glyphicon-log-out"></span></a>
                                ';
            })
            ->addColumn('customname', function ($guestlistdetails) {
                return'<a href="" class="Modal1" data-toggle="modal" data-target="#Modal1" data-customerid="'.$guestlistdetails->customer_id.'" data-wholename="'.$guestlistdetails->wholename.'"  data-email="'.$guestlistdetails->email.'" data-telephone="'.$guestlistdetails->telephone.'"   data-checkin="'.$guestlistdetails->checkin.'" data-checkout="'.$guestlistdetails->checkout.'">'.$guestlistdetails->wholename.'</a>
                                ';
            })


            ->make(true);
    }
    public function archivedguestdetails(){
        $guestlistdetails=DB::table('reservations')
            ->join('customers', 'customers.id', '=', 'reservations.customer_id')
            //->join('reservation_nights', 'reservation_nights.reservation_id', '=', 'reservations.id')
            ->join('room_types', 'reservations.room_type_id', '=', 'room_types.id')
            ->select(DB::raw('CONCAT_WS(" ",customers.first_name,customers.last_name) as wholename'),
                'reservations.customer_id', 'reservations.status','reservations.updated_at','reservations.checkin','reservations.checkout',
                'customers.first_name',
                'customers.last_name',
                'reservations.room_type_id','customers.email','customers.telephone',
                'room_types.name')
            ->groupBy('reservations.room_type_id')
            ->having('status', '=',0)
            ->get();



        return Datatables::of($guestlistdetails)
            ->addColumn('action', function ($guestlistdetails) {
                return'<a href="" class="edit-modal btn btn-sm btn-success" data-toggle="modal" data-target="#Modal1" data-customerid="'.$guestlistdetails->customer_id.'" ><span class="glyphicon glyphicon-log-out"></span></a>
                                ';
            })
            ->addColumn('customname', function ($guestlistdetails) {
                return'<a href="" class="Modal1" data-toggle="modal" data-target="#Modal1" data-customerid="'.$guestlistdetails->customer_id.'" data-wholename="'.$guestlistdetails->wholename.'"  data-email="'.$guestlistdetails->email.'" data-telephone="'.$guestlistdetails->telephone.'"   data-checkin="'.$guestlistdetails->checkin.'" data-checkout="'.$guestlistdetails->checkout.'">'.$guestlistdetails->wholename.'</a>
                                ';
            })


            ->make(true);
    }
    public function checkoutguest(Request $request){
        $customerid=$request->customerid;
        DB::table('reservations')
            ->where('customer_id', $customerid)
            ->update(['status' => "0"]);
        return redirect('guestlist');
    }
}

<?php

namespace hotelapp;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    //
    protected $fillable = [
        'user_id', 'username', 'date', 'activity'
    ];

    protected $table = 'audits';
}

@extends('layout.local.front.localmaster')
<script src="admin/js/jquery.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
@section('content')
        <!-- Begin page -->
<div id="wrapper">

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Dashboard</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                    <a href="#">Hotel</a>
                                </li>
                                <li class="active">
                                    Dashboard
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row">

                    <div class="col-lg-2 col-md-4 col-sm-4">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-chart-areaspline widget-one-icon"></i>
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total Rooms</p>
                                <h2><small></small></h2>
                            </div>
                        </div>
                    </div><!-- end col -->
                    <div class="col-lg-2 col-md-4 col-sm-4">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-chart-areaspline widget-one-icon"></i>
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Occupied  Rooms</p>
                                <h2><small></small></h2>
                            </div>
                        </div>
                    </div><!-- end col -->
                    <div class="col-lg-2 col-md-4 col-sm-4">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-chart-areaspline widget-one-icon"></i>
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Available Rooms</p>
                                <h2><small></small></h2>
                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-2 col-md-4 col-sm-4">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-account-convert widget-one-icon"></i>
                            <div class="wigdet-one-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Number of Guest</p>
                                <h2><small></small></h2>
                                <!-- <p class="text-muted m-0"><b>Last:</b> 1250</p> -->
                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-2 col-md-4 col-sm-4">
                        <div class="card-box widget-box-one">
                            <i class="mdi mdi-class wigdet-one-content"></i>
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Checkout today</p>
                            <h2><small></small></h2>
                            <!-- <p class="text-muted m-0"><b>Last:</b> 40.33k</p> -->
                        </div>
                    </div>
                    </layers class="widget-one-icon"></i>
                    <div ></div><!-- end col -->

                        <div class="col-lg-2 col-md-4 col-sm-4">
                            <div class="card-box widget-box-one">
                                <i class="mdi mdi-av-timer widget-one-icon"></i>
                                <div class="wigdet-one-content">
                                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Request Per Minute">Unspecified chout date</p>
                                    <h2><small></small></h2>
                                    <!-- <p class="text-muted m-0"><b>Last:</b> 956</p> -->
                                </div>
                            </div>
                        </div><!-- end col -->



                    </div>
                    <!-- end row -->


                    {{--<div class="row">--}}
                    {{--<div class="col-lg-4">--}}
                    {{--<div class="card-box">--}}

                    {{--<h4 class="header-title m-t-0">Daily Sales</h4>--}}

                    {{--<div class="widget-chart text-center">--}}
                    {{--<div id="morris-donut-example"style="height: 245px;"></div>--}}
                    {{--<ul class="list-inline chart-detail-list m-b-0">--}}
                    {{--<li>--}}
                    {{--<h5 class="text-danger"><i class="fa fa-circle m-r-5"></i>Series A</h5>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<h5 class="text-success"><i class="fa fa-circle m-r-5"></i>Series B</h5>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div><!-- end col -->--}}

                    {{--<div class="col-lg-4">--}}
                    {{--<div class="card-box">--}}

                    {{--<h4 class="header-title m-t-0">Statistics</h4>--}}
                    {{--<div id="morris-bar-example" style="height: 280px;"></div>--}}
                    {{--</div>--}}
                    {{--</div><!-- end col -->--}}

                    {{--<div class="col-lg-4">--}}
                    {{--<div class="card-box">--}}

                    {{--<h4 class="header-title m-t-0">Total Revenue</h4>--}}
                    {{--<div id="morris-line-example" style="height: 280px;"></div>--}}
                    {{--</div>--}}
                    {{--</div><!-- end col -->--}}

                    {{--</div>--}}
                            <!-- end row -->


                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card-box">
                                <h4 class="header-title m-t-0 m-b-30">Unspecified  Checkout</h4>

                                <div class="table-responsive">
                                    <table class="table table table-hover m-0">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>User Name</th>
                                            <th>Phone</th>
                                            <th>Location</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <th>
                                                    <img src="http://placehold.it/120x120" alt="user" class="thumb-sm img-circle" />
                                                </th>
                                                <td>
                                                    <h5 class="m-0"></h5>
                                                    <p class="m-0 text-muted font-13"><small>Web designer</small></p>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>



                                        </tbody>
                                    </table>

                                </div> <!-- table-responsive -->
                            </div> <!-- end card -->
                        </div>
                        <!-- end col -->

                        <div class="col-lg-6">
                            <div class="card-box">
                                <h4 class="header-title m-t-0 m-b-30">UpComing Checkout</h4>

                                <div class="table-responsive">
                                    <table class="table table table-hover m-0">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>User Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <th>
                                                    <span class="avatar-sm-box bg-success">L</span>
                                                </th>
                                                <td>
                                                    <h5 class="m-0"></h5>
                                                    <p class="m-0 text-muted font-13"><small>Web designer</small></p>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>



                                        </tbody>
                                    </table>

                                </div> <!-- table-responsive -->
                            </div> <!-- end card -->
                        </div>
                        <!-- end col -->

                    </div>
                    <!-- end row -->




                </div> <!-- container -->

            </div> <!-- content -->

            <footer class="footer text-right">
                2016 © NALO Solutions Limited.
            </footer>

        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.front.rightsidebar')
    </div>

@endsection




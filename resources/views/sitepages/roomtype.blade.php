
@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('addroomtype')}}" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Room Type</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room name</label>
                                <input type="text" id="roomname" class="form-control" name="roomname" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Short name</label>
                                <input type="text" id="shortname" class="form-control" name="shortname" required="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Base price:</label>
                                <input type="text" id="baseprice" class="form-control" name="baseprice" required="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Description</label>
                                <input type="text" id="description" name="description" class="form-control" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Max Occupancy</label>
                                <input type="text" id="maxoccupancy" name="maxoccupancy" class="form-control" required="">
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>

        <!-- Support Ticket Modal -->
        <div id="Modal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('updateroomtype')}}" >
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Room Type Details</h4>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="roomtypeidedit"  name="roomtypeid" class="form-control" >

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room name:</label>
                                <input type="text" id="nameedit" class="form-control" name="roomname" required="" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Short name:</label>

                                <input type="text" id="shortnameedit" class="form-control" name="shortname"  required="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Base price:</label>
                                <input type="text" id="basepriceedit" name="baseprice"class="form-control" required=""  >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Base Availability</label>
                                <input type="text" id="descriptionedit" name="description" class="form-control" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Max Occupancy</label>
                                <input type="text" id="maxoccupancyedit" class="form-control" name="maxoccupancy" required="" >
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save Changes</button>
                        </div>


                    </form>
                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
 <!-- Modal -->
        <div id="delModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete User</h4>
                    </div>
                    <form role="form" method="post" action="{{route('deleteroomtype')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="roomtypeid" id="roomtypeiddelete">
                                <p>Are you sure you want to delete <b><span id="namedelete" name="namedelete"></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Room Type Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Room Type Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <p class="text-muted font-13 m-b-30">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-user" aria-hidden="true">&nbsp;</i>  Add Room Type</button>
                                    {{--&nbsp;&nbsp;  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#priceModal"><i class="fa fa-user" aria-hidden="true">&nbsp;</i>  Set price</button>--}}

                                </p>



                                <table id="roomtypes-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Short name</th>
                                        <th>base price</th>
                                        <th>Description</th>
                                        <th>max Occupancy</th>
                                        <th width="20%">Action</th>
                                     </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>




    <script>
        $(function() {
            $('#roomtypes-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!!route('datatable.roomtypedetails')!!}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'short_name', name: 'short_name' },
                    { data: 'base_price', name: 'base_price' },
                    { data: 'description', name: 'description' },
                    { data: 'max_occupancy', name: 'max_occupancy' },
                    { data : 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    <script>
        $(document).on('click','.edit-modal',function() {

            $('#roomtypeidedit').val($(this).data('id'));
            $('#nameedit').val($(this).data('name'));
            $('#shortnameedit').val($(this).data('shortname'));
            $('#basepriceedit').val($(this).data('baseprice'));
            $('#descriptionedit').val($(this).data('description'));
            $('#maxoccupancyedit').val($(this).data('maxoccupancy'));
        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {

            $('#roomtypeiddelete').val($(this).data('id'));
            var name=$(this).data('name');

            $("#namedelete").html(name);

        });
    </script>


@endsection


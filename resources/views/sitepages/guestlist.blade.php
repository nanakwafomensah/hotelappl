
@extends('layout.local.localmaster')


@section('content')
    <div id="wrapper">
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('storecustomer')}}" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add New Customer</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Firstname</label>
                                <input type="text" id="customername" class="form-control" name="firstname" placeholder="marchaty " >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Lastname</label>
                                <input type="text" id="shortname" class="form-control" name="lastname" placeholder=" Ninyeh" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Email</label>
                                <input type="email" id="username" class="form-control" name="email" placeholder="Aninyeh">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Telephone:</label>
                                <input type="text" id="baseprice" class="form-control" name="telephone" placeholder="eaglesecurity0@gmail.com" >
                            </div>


                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <div id="priceModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('setroomprice')}}" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Set price for Room Types</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room type</label>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">start date</label>
                                <input type="date" id="start_dt" class="form-control" name="start_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">enddate:</label>
                                <input type="date" id="end_dt" class="form-control" name="end_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">price:</label>
                                <input type="text" id="price" name="price" class="form-control">
                            </div>

                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Set</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Ticket Modal -->
        <div id="Modal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content" >
                   <div id="output">

                        <div class="modal-header">
                            {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}

                            <h4 class="modal-title">Guest Details</h4>
                        </div>
                        <div class="modal-body" >

                            <div class="form-group">
                                <label for="exampleInputPassword1">Guest identity number : <span id="guestcustomerid"></span></label>

                            </div>
                            &nbsp
                            <div class="form-group">
                                <label for="exampleInputPassword1">Fullname : <span id="guestwholename"></span></label>

                            </div>
                            &nbsp

                            <div class="form-group">
                                <label for="exampleInputPassword1">Email : <span id="guestemail"></span></label>

                            </div>
                            &nbsp
                            <div class="form-group">
                                <label for="exampleInputPassword1">Telephone : <span id="guesttelephone"></span></label>

                            </div>
                            &nbsp
                            <div class="form-group">
                                <label for="exampleInputPassword1">Checkin Date : <span id="guestcheckin"></span></label>

                            </div>
                            &nbsp
                            <div class="form-group">
                                <label for="exampleInputPassword1">Checkout Date : <span id="guestcheckout"></span></label>

                            </div>



                        </div>
                   </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <a href="{{URL::to('printguest/')}}" id="add-row" class="print btn btn-success"><i class="glyphicon glyphicon-print" aria-hidden="true"></i> Print</a>
                        </div>



                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="delModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Checkout Guest </h4>
                    </div>
                    <form role="form" method="post" action="{{route('checkoutguest')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="customerid" id="customeridcheckout">
                                <p>Are you sure you want to checkout <b><span id="wholename" name="namecheckout"></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Check out</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Guest List </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Guest List
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                {{--<p class="text-muted font-13 m-b-30">--}}
                                    {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-user" aria-hidden="true">&nbsp;</i> New Customer</button>--}}
                                {{--</p>--}}



                                <table id="guestlist-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Guest id</th>
                                        <th>Guest name</th>
                                        <th>Room</th>

                                        <th width="20%">Checkout</th>

                                    </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


    <script>
        $(function() {
            $('#guestlist-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!!route('datatable.guestlistdetails')!!}',
                columns: [
                    { data: 'customer_id', name: 'customer_id' },
                    { data: 'customname', name: 'customname' },
                    { data: 'name', name: 'name' },

                    { data : 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    <script>
        $(document).on('click','.delModal',function() {
            //alert($(this).data('customerid'));
            $('#customeridcheckout').val($(this).data('customerid'));
           $('#wholename').html($(this).data('wholename'));
//            $('#lastnameedit').val($(this).data('lastname'));
//            $('#emailedit').val($(this).data('email'));
//            $('#telephoneedit').val($(this).data('telephone'));

        });
    </script>
    <script>
        $(document).on('click','.Modal1',function() {

            $('#guestcustomerid').html($(this).data('customerid'));
            $('#guestwholename').html($(this).data('wholename'));
          $('#guestcheckin').html($(this).data('checkin'));
            $('#guestcheckout').html($(this).data('checkout'));
             $('#guestemail').html($(this).data('email'));
            $('#guesttelephone').html($(this).data('telephone'));

        });
    </script>
    <script>
        $(document).on('click','.print',function(e) {
            e.preventDefault();

            var newWindow = window.open('','printwin','left=100,top=100,width=400,height=400');

            newWindow.document.write('<html><head><title>Guest Details</title>');
            newWindow.document.write('</head><body >');
            newWindow.document.write(document.getElementById("output").innerHTML);
            newWindow.document.write('</body></html>');

             newWindow.print();
        });

    </script>


@endsection


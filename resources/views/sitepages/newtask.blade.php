
@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->
        <div id="Modal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('updatetask')}}" data-parsley-validate="">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Task</h4>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="taskid"  name="taskid" class="form-control" >

                            <div class="form-group">
                                <label for="exampleInputPassword1">Task</label>
                                <input type="text" id="task" class="form-control" name="task" required="">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> Update</button>
                        </div>


                    </form>

                </div>
            </div>
        </div>


        <div id="delModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete User</h4>
                    </div>
                    <form role="form" method="post" action="{{route('deletetask')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="taskiddd" id="taskiddd"/>
                                <p>Are you sure you want to delete </br><br><span id="tasknamedd" ></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div id="Modal2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Confirm</h4>
                    </div>
                    <form role="form" method="post" action="{{route('complete_a_task')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="taskidww" id="taskidww"/>
                                <p>Are you sure you sure task has being completed ?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-yes" aria-hidden="true"></i>Yes</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">User Task Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        User Task Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <p class="text-muted font-13 m-b-30">

                                <div class="form-group">
                                    <div class="col-lg-4">

                                        <input type="text" id="taskedit" name="task" class="form-control" placeholder=" have to call nana" required>
                                    </div>

                                    <button type="button" class="btn btn-info add_task" ><i class="fa fa-plus" aria-hidden="true">&nbsp;</i>  Add Task</button>
                                    <button type="button" class="btn btn-info refresh" ><i class="fa fa-refresh" aria-hidden="true">&nbsp;</i></button>

                                </div>
                                </p>



                                <table id="tasks-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                       <th>task</th>
                                        <th>Created At</th>
                                        <th width="40%">Action</th>
                                    </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © Alma Productions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    {{ Html::script('parsley/js/parsley.min.js') }}


    <script>
        $(function() {
            $('#tasks-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable.tasklist') !!}',
                columns: [
                    { data: 'task', name: 'task' },
                    { data: 'created_at', name: 'created_at' },

                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    <script>
        $(document).on('click','.add_task',function() {
           $('#taskedit').parsley().validate();
            if($('#taskedit').parsley().isValid()){
                var task=$("#taskedit").val() ;
                $.ajax({
                    type:'post' ,
                    url: '{{URL::to('createtask')}}',
                    data:{
                        '_token':$('input[name=_token]').val(),
                        'task':task

                    },
                    beforeSend: function(){
                        $('#loader').show();
                    },
                    complete: function(){
                        $('#loader').hide();
                    },
                    success:function(data){
                        if(data=="success"){
                            toastr.success('A New task has been succesfully added','Success',{timeOut: 1000});
                            $('#taskedit').val("");
                            location.reload();
                        }
                        else if(data=="fail"){
                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                        }


                    }
                });
                console.log("valid");
            }else{
                console.log("not valid");
            }



        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {

            $('#taskiddd').val($(this).data('id'));
             $('#tasknamedd').html($(this).data('task'));


        });
        $(document).on('click','.edit-modal',function() {

            $('#taskid').val($(this).data('id'));
            $('#task').val($(this).data('task'));


        });
    </script>

    <script>

        $(document).on('click','.completebtn',function() {

            $('#taskidww').val($(this).data('id'));



        });
        $(document).on('click','.refresh',function() {

            location.reload();
          });

    </script>


@endsection


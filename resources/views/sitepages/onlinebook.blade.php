<!--
<!DOCTYPE html>
<html>
    <head>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
        {{--<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>--}}
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <nav>
        <div class="nav-wrapper">
            <a href="#" class="brand-logo">Logo</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="sass.html">Sass</a></li>
                <li><a href="badges.html">Components</a></li>
                <li><a href="collapsible.html">JavaScript</a></li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <h2 class="center">Search your room</h2>
    </div>
    <div class="row">
        <div class="col m6 s12 offset-m3">
            <form action="{{route('searchroom')}}" method="post">
                {{csrf_field()}}
                <div class="card-panel">
                    <div class="row">
                        <div class="col m5 s12">
                            <div class="input-field">
                                <label for="start_dt">Check-in date</label>
                                <input  type="text" id="start_dt" name="start_dt" />
                            </div>
                        </div>
                        <div class="col m5 s12">
                            <div class="input-field">
                                <label for="end_dt">Check-in date</label>
                                <input  type="text" id="end_dt" name="end_dt" />
                            </div>
                        </div>
                        <div class="col m5 s12">
                            <label for="occupancy">Persons</label>
                            <input  type="text" id="value in select_occupancy" name="min_occupancy"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m4 offset-m4">
                            <button class="btn">Search</button>
                        </div>
                    </div>
                </div>
            </form>
            @if(session('available_rooms'))
            {{--@foreach($available_rooms as $available_room)--}}
            {{--<?php echo $available_room ?>--}}
            {{--@endforeach--}}
            @endif
        </div>
    </div>
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Footer Content</h5>
                    <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2014 Copyright Text
                <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
        </div>
    </footer>
</body>
</html> -->
@extends('layout.online.onlinemaster')
@section('content')
<section class="featured-lis" >
<div class="container">
    <div class="row">
        <div class="col-md-6 wow fadeIn" data-wow-delay="0.5s">
            <h3 class="section-title">Rooms Available</h3>
            <div id="new-products" class="owl-carousel">
                <div class="item">
                    <div class="product-item">
                        <div class="carousel-thumb">
                            <img src="assets/img/product/img1.jpg" alt="">
                            <div class="overlay">
                                <a href="ads-details.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <a href="ads-details.html" class="item-name">Lorem ipsum dolor sit</a>
                        <span class="price">$150</span>
                    </div>
                </div>
                <div class="item">
                    <div class="product-item">
                        <div class="carousel-thumb">
                            <img src="assets/img/product/img2.jpg" alt="">
                            <div class="overlay">
                                <a href="ads-details.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <a href="ads-details.html" class="item-name">Sed diam nonummy</a>
                        <span class="price">$67</span>
                    </div>
                </div>
                <div class="item">
                    <div class="product-item">
                        <div class="carousel-thumb">
                            <img src="assets/img/product/img3.jpg" alt="">
                            <div class="overlay">
                                <a href="ads-details.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <a href="ads-details.html" class="item-name">Feugiat nulla facilisis</a>
                        <span class="price">$300</span>
                    </div>
                </div>
                <div class="item">
                    <div class="product-item">
                        <div class="carousel-thumb">
                            <img src="assets/img/product/img4.jpg" alt="">
                            <div class="overlay">
                                <a href="ads-details.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <a href="ads-details.html" class="item-name">Lorem ipsum dolor sit</a>
                        <span class="price">$149</span>
                    </div>
                </div>
                <div class="item">
                    <div class="product-item">
                        <div class="carousel-thumb">
                            <img src="assets/img/product/img5.jpg" alt="">
                            <div class="overlay">
                                <a href="ads-details.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <a href="ads-details.html" class="item-name">Sed diam nonummy</a>
                        <span class="price">$90</span>
                    </div>
                </div>
                <div class="item">
                    <div class="product-item">
                        <div class="carousel-thumb">
                            <img src="assets/img/product/img6.jpg" alt="">
                            <div class="overlay">
                                <a href="ads-details.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <a href="ads-details.html" class="item-name">Praesent luptatum zzril</a>
                        <span class="price">$169</span>
                    </div>
                </div>
                <div class="item">
                    <div class="product-item">
                        <div class="carousel-thumb">
                            <img src="assets/img/product/img7.jpg" alt="">
                            <div class="overlay">
                                <a href="ads-details.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <a href="ads-details.html" class="item-name">Lorem ipsum dolor sit</a>
                        <span class="price">$79</span>
                    </div>
                </div>
                <div class="item">
                    <div class="product-item">
                        <div class="carousel-thumb">
                            <img src="assets/img/product/img8.jpg" alt="">
                            <div class="overlay">
                                <a href="ads-details.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                        <a href="ads-details.html" class="item-name">Sed diam nonummy</a>
                        <span class="price">$149</span>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <!-- <div class="col-sm-4">
            <div class="sidebar sidebar-single-download">
                <aside>
                    <div class="item-buttons">
                        <p><span class="icon-social-dropbox"></span> Full Template package</p>
                        <p><span class="icon-support"></span> Support from author</p>
                        <p><span class="icon-trophy"></span> Quality checked by GrayGrids</p>
                        <p><span class="icon-refresh"></span> Free future updates</p>
                        <p><span class="icon-vector"></span> Personal and commercial use</p>
                        <p><span class="icon-wallet"></span> One-time payment</p>
                        <p><span class="icon-notebook"></span> Documentation included</p>
                        <p class="text-center"><a style="font-size: 10px;text-transform: uppercase;" href="https://graygrids.com/lite-vs-full/" target="_blank"><span class="icon-info"></span> Lite Version vs. Full Version →</a></p>
                        <p> <form id="edd_purchase_13219" class="edd_download_purchase_form edd_purchase_13219" method="post">
                            <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <meta itemprop="price" content="16.00"/>
                                <meta itemprop="priceCurrency" content="USD"/>
                            </span>
                            <div class="edd_purchase_submit_wrapper">
                                <a href="#" class="edd-add-to-cart button blue btn btn-danger" data-action="edd_add_to_cart" data-download-id="13219" data-variable-price="no" data-price-mode=single data-price="16.00"><span class="edd-add-to-cart-label">Buy Now - &#36;16</span> <span class="edd-loading" aria-label="Loading"></span></a><input type="submit" class="edd-add-to-cart edd-no-js button blue btn btn-danger" name="edd_purchase_download" value="Buy Now - &#036;16" data-action="edd_add_to_cart" data-download-id="13219" data-variable-price="no" data-price-mode=single /><a href="https://graygrids.com/checkout/" class="edd_go_to_checkout button blue btn btn-danger" style="display:none;">Checkout</a>
                                <span class="edd-cart-ajax-alert" aria-live="assertive">
                                    <span class="edd-cart-added-alert" style="display: none;">
                                        <svg class="edd-icon edd-icon-check" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" aria-hidden="true">
                                            <path d="M26.11 8.844c0 .39-.157.78-.44 1.062L12.234 23.344c-.28.28-.672.438-1.062.438s-.78-.156-1.06-.438l-7.782-7.78c-.28-.282-.438-.673-.438-1.063s.156-.78.438-1.06l2.125-2.126c.28-.28.672-.438 1.062-.438s.78.156 1.062.438l4.594 4.61L21.42 5.656c.282-.28.673-.438 1.063-.438s.78.155 1.062.437l2.125 2.125c.28.28.438.672.438 1.062z"/>
                                        </svg>
                                    Added to cart </span>
                                </span>
                            </div>
                            <input type="hidden" name="download_id" value="13219">
                            <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                            <input type="hidden" name="edd_redirect_to_checkout" id="edd_redirect_to_checkout" value="1">
                        </form>
                    </p>
                    <p><a href="https://graygrids.com/?wpb_process_download=1&download_id=13219" class="btn btn-border-common"><i class="icon-puzzle"></i> Download Lite Version</a></p>
                    <p><a href="http://preview.graygrids.com/item/classix-free-bootstrap-html5-classified-ads-template/" class="btn btn-common lg-dl-btn" target="_blank"><i class="fa fa-external-link"></i> Live Demo</a></p>
                </div>
                <div class="download-counter text-center">
                    <p><i class="icon-basket-loaded"></i>146 sales & 5714 downloads</p>
                </div>
                <div class="parameters">
                </div>
            </aside>
        </div> -->
    </div>
</div>
</div>
</section>
<br><br>
@endsection

@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('makepayment')}}" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add New Payment</h4>
                        </div>

                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Customerid</label>
                                <input type="text" id="customerid" class="form-control" name="customerid"  >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">CustomerName</label>
                                <input type="text" id="customername" class="form-control" name="customername" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Category</label>
                                <select class="form-control" id="category" name="category">
                                    <option value="reservation">reservation</option>
                                    <option value="food">food</option>
                                    <option value="others">others</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div></div>
                                <label for="exampleInputPassword1">Receipt Number:</label>
                                <input type="text" id="receiptnumber" class="form-control" name="receiptnumber" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Invoice Number:</label>
                                <input type="text" id="invoicenumber" class="form-control" name="invoicenumber" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Payment Date:</label>
                                <input type="date" id="paymentdate" class="form-control" name="paymentdate" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Amount:</label>
                                <input type="text" id="amount" class="form-control" name="amount" placeholder="GHC" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Remarks:</label>
                                <textarea id="remarks" class="form-control" name="remark" rows="5" ></textarea>
                            </div>

                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <div id="priceModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('setroomprice')}}" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Set price for Room Types</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room type</label>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">start date</label>
                                <input type="date" id="start_dt" class="form-control" name="start_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">enddate:</label>
                                <input type="date" id="end_dt" class="form-control" name="end_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">price:</label>
                                <input type="text" id="price" name="price" class="form-control">
                            </div>

                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Set</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Ticket Modal -->
        <div id="Modal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('editcustomer')}}" >
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Customer Details</h4>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="customeridedit"  name="customerid" class="form-control" >

                            <div class="form-group">
                                <label for="exampleInputPassword1">First name:</label>
                                <input type="text" id="firstnameedit" class="form-control" name="firstname" placeholder="Albert Ninyeh" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Last name:</label>

                                <input type="text" id="lastnameedit" class="form-control" name="lastname" placeholder="Aninyeh">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Email:</label>
                                <input type="text" id="emailedit" name="email"class="form-control" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Telephone</label>
                                <input type="text" id="telephoneedit" name="telephone" class="form-control">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save Changes</button>
                        </div>


                    </form>
                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="delModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Customer</h4>
                    </div>
                    <form role="form" method="post" action="{{route('deletecustomer')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="customerid" id="customeriddelete">
                                <p>Are you sure you want to delete <b><span id="firstnamedelete" name="namedelete"></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Debtors Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Debtors Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">





                                <table id="debtors-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Invoice Number</th>
                                        <th>Customer Name</th>
                                        <th>Invoice Date</th>
                                        <th>Total Amount</th>
                                        <th>Owing</th>
                                        {{--<th>Excess</th>--}}
                                        {{--<th>created_at</th>--}}
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script>
        $('#debtors-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!!route('datatable.debtors')!!}',
            columns: [
                {data: 'invoicenumber', name: 'invoicenumber'},
                {data: 'wholename', name: 'wholename'},
                {data: 'invoicedate', name: 'invoicedate'},
                {data: 'a_mount', name: 'a_mount'},
                {data: 'owing', name: 'owing'},
//                {data: 'excess', name: 'excess'},
//                {data: 'created_at', name: 'created_at'},
//                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                });
            }
        });
    </script>


@endsection


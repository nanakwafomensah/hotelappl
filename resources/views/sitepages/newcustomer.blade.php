
@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('storecustomer')}}" method="post"  id="add_form" data-parsley-validate="">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add New Customer</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Firstname</label>
                                <input type="text" id="firstname" class="form-control" name="firstname" required="" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Lastname</label>
                                <input type="text" id="lastname" class="form-control" name="lastname"required="" >
                            </div>
                            <div class="form-group">
                            <label for="exampleInputPassword1">Email</label>
                            <input type="email" id="email" class="form-control" name="email" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Telephone:</label>
                                <input type="text" id="telephone" class="form-control" name="telephone" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Location:</label>
                                <input type="text" id="location" class="form-control" name="location" required="">
                            </div>


                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="addcustomer" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>

                        </div>

                    </form>

                </div>
            </div>
        </div>
        <div id="priceModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('setroomprice')}}" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Set price for Room Types</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room type</label>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">start date</label>
                                <input type="date" id="start_dt" class="form-control" name="start_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">enddate:</label>
                                <input type="date" id="end_dt" class="form-control" name="end_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">price:</label>
                                <input type="text" id="price" name="price" class="form-control">
                            </div>

                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Set</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Ticket Modal -->
        <div id="Modal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('editcustomer')}}" >
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Customer Details</h4>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="customeridedit"  name="customerid" class="form-control" >

                            <div class="form-group">
                                <label for="exampleInputPassword1">First name:</label>
                                <input type="text" id="firstnameedit" class="form-control" name="firstname"  >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Last name:</label>

                                <input type="text" id="lastnameedit" class="form-control" name="lastname" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Email:</label>
                                <input type="text" id="emailedit" name="email"class="form-control" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Telephone</label>
                                <input type="text" id="telephoneedit" name="telephone" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Location</label>
                                <input type="text" id="location" name="location" class="form-control" >
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save Changes</button>
                        </div>


                    </form>
                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="delModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Customer</h4>
                    </div>
                    <form role="form" method="post" action="{{route('deletecustomer')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="customerid" id="customeriddelete">
                                <p>Are you sure you want to delete <b><span id="firstnamedelete" name="namedelete"></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Guest Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Guest Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <p class="text-muted font-13 m-b-30">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-user" aria-hidden="true">&nbsp;</i> New Guest</button>
                                </p>


                                <table id="customers-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Fullname</th>
                                        <th>Location</th>
                                        <th>Email</th>
                                        <th>Telephone</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                    </thead>

                                </table>


                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


    <script>
        $(function() {
            $('#customers-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!!route('datatable.customersdetails')!!}',
                columns: [
                    { data: 'wholename', name: 'wholename' },
                    { data: 'location', name: 'location' },
                    { data: 'email', name: 'email' },
                    { data: 'telephone', name: 'telephone' },
                   { data : 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    <script>
        $(document).on('click','.edit-modal',function() {

            $('#customeridedit').val($(this).data('customerid'));
            $('#firstnameedit').val($(this).data('firstname'));
            $('#lastnameedit').val($(this).data('lastname'));
            $('#emailedit').val($(this).data('email'));
            $('#telephoneedit').val($(this).data('telephone'));

        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {
   // alert($(this).data('customerid'));
            $('#customeriddelete').val($(this).data('customerid'));
            var name=$(this).data('firstname');

            $("#firstnamedelete").html(name);

        });
    </script>
    <script>


//        $(document).on('click','#addcustomer',function(e) {
//
//            $('#firstname').parsley().validate();
//            $('#lastname').parsley().validate();
//            $('#email').parsley().validate();
//            $('#telephone').parsley().validate();
//            $('#location').parsley().validate();
//        });

    </script>

@endsection


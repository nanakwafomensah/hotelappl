
@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Invoice Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Invoice Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">


                                <form role="form" method="post" action="{{route('generate_invoice')}}" name="Invoiceform">
                                    <div class="modal-body">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="customer">Customer:</label>
                                                    <input type="text" id="customerinvoiceid" class="form-control" name="customerinvoiceid"  value="{{$customerid}}" disabled/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="date">Date:</label>
                                                    <input type="text" id="invoicedate" class="form-control" name="invoicedate" value="{{$invoicedate}}"  disabled/>
                                                </div>
                                            </div>
                                            <div  class="col-md-4">
                                                <div class="form-group">
                                                    <label for="invoicenumber">Invoice Number:</label>
                                                    <input type="text" id="invoicenumber" class="form-control" name="invoicenumber"  value="{{$invoicenumber}}" disabled/>
                                                </div>
                                            </div>
                                        </div>




                                        <table class="table table-hover table-inverse">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Description</th>
                                                <th width="10%">quantity</th>
                                                <th>Amount (GHC)</th>
                                                <th>Total (GHC)</th>
                                            </tr>
                                            </thead>
                                            <tbody id="data">
                                            <?php $n=1;$s=0;$totalamount=0; $c=null;?>
                                            @foreach($items_oninvoice as $i)
                                               <?php$s=$s+1 ;?>
                                                <tr class="item">
                                                    <th scope="row" id="itemnumber">{{$n++}}</th>
                                                    <td>
                                                        <div class="form-group">
                                                            <input name="description{{$c+1}}" class="form-control"  id="description{{$c+1}}" value="{{$i->description}}" type="text" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <input name="quantity{{$c+1}}" class="form-control"   onchange="check_user();" id="quantity{{$c+1}}" value="{{$i->quantity}}" type="number" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <input name="amount{{$c+1}}" class="form-control" onchange="check_user();" id="amount{{$c+1}}"  value="{{number_format($i->amount)}}" type="number" disabled/>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <input name="total{{$c+1}}" class="form-control" id="total{{$c+1}}" value="{{number_format($i->quantity * $i->amount)}}" type="number"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                               <?php $c++;?>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        <input type="hidden" id="number_of_items" name="number_of_items" value="{{$c}}" />
                                        <div class="row">
                                            <div class="col-md-3">
                                                <button type="button" class="btn btn-info" id="addnew">Add New Item</button>
                                            </div>
                                        </div>

                                    </div>
                                    {{csrf_field()}}
                                    <div class="modal-footer" id="downloadbtn">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                                        <button type="submit" class="btn btn-success" id="geninvoice"><i class="fa fa-save" aria-hidden="true" ></i> Save Changes</button>
                                        {{--<a  class="btn btn-success" id="downloadbtn"><i class="fa fa-download" aria-hidden="true" ></i> Download</a>--}}

                                    </div>

                                </form>


                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {


            $('#addnew').click(function(){
                var currentItem = Number($('#number_of_items').val());

                currentItem =currentItem + 1;

                $('#number_of_items').val(currentItem);
                var strToAdd='<tr class="item"><th scope="row">'+currentItem+'</th><td><div class="form-group"><input name="description'+currentItem+'" class="form-control"  id="description'+currentItem+'" type="text" /> </div></td><td><div class="form-group"><input name="quantity'+currentItem+'" class="form-control"   onchange="check_user();" id="quantity'+currentItem+'" type="number" /> </div> </td> <td> <div class="form-group"> <input name="amount'+currentItem+'" class="form-control" onchange="check_user();" id="amount'+currentItem+'" type="number" /> </div> </td> <td> <div class="form-group"> <input name="total'+currentItem+'" class="form-control" id="total'+currentItem+'" type="number" /> </div> </td> </tr>';

                $('#data').append(strToAdd);

            });

        })
        $('#geninvoice').on('click', function(e) {
            e.preventDefault();
            if(validateinvoiceform()) { //validate invoice form
                var number_of_items = $('#number_of_items').val();

                var i;

                var customerid = $('#customerinvoiceid').val();
                var invoicedate = $('#invoicedate').val();
                var invoicenumber = $('#invoicenumber').val();
                 $.ajax({
                    type:'post' ,
                    url: '{{URL::to('delete_for_update')}}',
                    data:{
                        '_token':$('input[name=_token]').val(),
                        'customerid':customerid,
                        'invoicedate':invoicedate,
                        'invoicenumber':invoicenumber,

                    },
                    success:function(data){
                        if(data=='good'){
                            for (i = 1; i <=number_of_items; i++) {
                                var description=$("#description"+i).val();
                                var quantity=$("#quantity"+i).val();
                                var amount=$("#amount"+i).val();
                                var total=$("#total"+i).val();

                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('update_invoice')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'customerid':customerid,
                                        'invoicedate':invoicedate,
                                        'invoicenumber':invoicenumber,
                                        'description':description,
                                        'quantity':quantity,
                                        'amount':amount,
                                        'total':total
                                    },
                                    success:function(data){
                                        $('#geninvoice').hide();
                                        $('#downloadbtn').html(data);


                                    }
                                });

                            }
                        }else if(data=='bad'){
                            alert("bad");
                        }

                    }
                });



            }
        });


    </script>
    <script>
        function check_user(){
            var index=document.getElementById("data").rows.length;
            var amount=$('#amount'+index).val();
            var quantity=$('#quantity'+index).val();
            $('#total'+index).val(amount*quantity);
        }
        function validateinvoiceform(){

            var validatecustomerid = document.forms["Invoiceform"]["customerinvoiceid"].value;
            var validatedate = document.forms["Invoiceform"]["invoicedate"].value;
            var validateinvoicenumber = document.forms["Invoiceform"]["invoicenumber"].value;
            if (validatecustomerid == "") {
                $("#customerinvoiceid").css({"background-color": "pink"});
                return false;
            }
            else if(validatedate== ""){
                $("#invoicedate").css({"background-color": "pink"});
                return false;
            }
            else if(validateinvoicenumber== ""){
                $("#invoicenumber").css({"background-color": "pink"});
                return false;
            }else{
                $("#customerinvoiceid").css({"background-color": "white"});
                $("#invoicedate").css({"background-color": "white"});
                $("#invoicenumber").css({"background-color": "white"});
                return true;
            }

        }


    </script>

@endsection



@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->
        <!-- Modal -->
        <div id="edit-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Book Event</h4>
                    </div>
                    <form role="form" method="post" action="{{route('deletecustomer')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="eventid" id="eventidbook">
                                <p>Are you sure you want to Book this Event <b><span id="firstnamedelete" name="namedelete"></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Events booked</h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        User Task Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <p class="text-muted font-13 m-b-30">


                                </p>



                                <table id="events-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name of Event</th>
                                        <th>Purpose</th>
                                        <th>Coordinators</th>
                                        <th>projected total</th>
                                        <th>Actual total</th>
                                        <th width="40%">Action</th>
                                    </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © Alma Productions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    {{ Html::script('parsley/js/parsley.min.js') }}


    <script>
        $(function() {
            $('#events-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable.closeevent') !!}',
                columns: [
                    { data: 'eventname', name: 'eventname' },
                    { data: 'eventtype', name: 'eventtype' },
                    { data: 'coordinator', name: 'coordinator' },
                    { data: 'projectedtotal', name: 'projectedtotal' },
                    { data: 'actualtotal', name: 'actualtotal' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(document).on('click','.edit-modal',function() {

            $('#customeridedit').val($(this).data('customerid'));
            $('#firstnameedit').val($(this).data('firstname'));
            $('#lastnameedit').val($(this).data('lastname'));
            $('#emailedit').val($(this).data('email'));
            $('#telephoneedit').val($(this).data('telephone'));

        });
    </script>



@endsection



@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Invoice</h4>
                    </div>
                    <form role="form" method="post" action="{{route('generate_invoice')}}" name="Invoiceform">
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="customer">Customer:</label>
                                        <input type="text" id="customerinvoiceid" class="form-control" name="customerinvoiceid"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="date">Date:</label>
                                        <input type="date" id="invoicedate" class="form-control" name="invoicedate" />
                                    </div>
                                </div>
                                <div  class="col-md-4">
                                    <div class="form-group">
                                        <label for="invoicenumber">Invoice Number:</label>
                                        <input type="text" id="invoicenumber" class="form-control" name="invoicenumber"  value="{{uniqid()}}" disabled/>
                                    </div>
                                </div>
                            </div>




                            <table class="table table-hover table-inverse">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th width="10%">quantity</th>
                                    <th>Amount (GHC)</th>
                                    <th>Total (GHC)</th>
                                </tr>
                                </thead>
                                <tbody id="data">
                                <tr class="item">
                                    <th scope="row" id="itemnumber">1</th>
                                    <td>
                                        <div class="form-group">
                                            <input name="description1" class="form-control"  id="description1" type="text" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="quantity1" class="form-control"   onchange="check_user();" id="quantity1" type="number" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="amount1" class="form-control" onchange="check_user();" id="amount1" type="number"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="total1" class="form-control" id="total1" type="number"/>
                                        </div>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                            <input type="hidden" id="number_of_items" name="number_of_items" value="1" />
                            <div class="row">
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-info" id="addnew">Add New Item</button>
                                </div>
                            </div>

                        </div>
                        {{csrf_field()}}
                        <div class="modal-footer" id="downloadbtn">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-success" id="geninvoice"><i class="fa fa-save" aria-hidden="true" ></i> Save</button>
                            {{--<a  class="btn btn-success" id="downloadbtn"><i class="fa fa-download" aria-hidden="true" ></i> Download</a>--}}

                        </div>

                    </form>

                </div>
            </div>
        </div>

        <!-- Support Ticket Modal -->
        <div id="Modal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('editcustomer')}}" >
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Customer Details</h4>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="customeridedit"  name="customerid" class="form-control" >

                            <div class="form-group">
                                <label for="exampleInputPassword1">First name:</label>
                                <input type="text" id="firstnameedit" class="form-control" name="firstname" placeholder="Albert Ninyeh" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Last name:</label>

                                <input type="text" id="lastnameedit" class="form-control" name="lastname" placeholder="Aninyeh">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Email:</label>
                                <input type="text" id="emailedit" name="email"class="form-control" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Telephone</label>
                                <input type="text" id="telephoneedit" name="telephone" class="form-control">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save Changes</button>
                        </div>


                    </form>
                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="delModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Customer</h4>
                    </div>
                    <form role="form" method="post" action="{{route('deletecustomer')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="customerid" id="customeriddelete">
                                <p>Are you sure you want to delete <b><span id="firstnamedelete" name="namedelete"></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Billing Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Billing Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <p class="text-muted font-13 m-b-30">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-user" aria-hidden="true">&nbsp;</i>Generate Bill</button>
                                </p>



                                <table id="bills-table" class="table table-striped table-bordered" >
                                    <thead>
                                    <tr>
                                        <th>Invoice Number</th>
                                        <th>Customer Name</th>
                                        <th>Invoice Date</th>
                                        <th>Total Bill</th>
                                        <th>Owing</th>
                                        <th>Excess</th>
                                        {{--<th>created_at</th>--}}
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot><tr><td></td><td></td><td></td><td></td><td></td><td></td></tr></tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script>
        $('#bills-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!!route('datatable.bills')!!}',
            columns: [
                {data: 'invoicenumber', name: 'invoicenumber'},
                {data: 'wholename', name: 'wholename'},
                {data: 'invoicedate', name: 'invoicedate'},
                {data: 'a_mount', name: 'a_mount'},
                {data: 'owing', name: 'owing'},
                {data: 'excess', name: 'excess'},
//                {data: 'created_at', name: 'created_at'},
//                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                });
            }
        });
    </script>
    <script>
        $(document).ready(function() {

            var currentItem = 1;
            $('#addnew').click(function(){
                currentItem =currentItem+1;
                $('#number_of_items').val(currentItem);
                var strToAdd='<tr class="item"><th scope="row">'+currentItem+'</th><td><div class="form-group"><input name="description'+currentItem+'" class="form-control"  id="description'+currentItem+'" type="text" /> </div></td><td><div class="form-group"><input name="quantity'+currentItem+'" class="form-control"   onchange="check_user();" id="quantity'+currentItem+'" type="number" /> </div> </td> <td> <div class="form-group"> <input name="amount'+currentItem+'" class="form-control" onchange="check_user();" id="amount'+currentItem+'" type="number" /> </div> </td> <td> <div class="form-group"> <input name="total'+currentItem+'" class="form-control" id="total'+currentItem+'" type="number" /> </div> </td> </tr>';

                $('#data').append(strToAdd);

            });

        })



        $('#geninvoice').on('click', function(e) {
            e.preventDefault();
            if(validateinvoiceform()) { //validate invoice form
                var number_of_items = $('#number_of_items').val();
                var i;

                var customerid = $('#customerinvoiceid').val();
                var invoicedate = $('#invoicedate').val();
                var invoicenumber = $('#invoicenumber').val();


                for (i = 1; i <=number_of_items; i++) {
                    var description=$("#description"+i).val();
                    var quantity=$("#quantity"+i).val();
                    var amount=$("#amount"+i).val();
                    var total=$("#total"+i).val();

                    $.ajax({
                        type:'post' ,
                        url: '{{URL::to('generate_invoice')}}',
                        data:{
                            '_token':$('input[name=_token]').val(),
                            'customerid':customerid,
                            'invoicedate':invoicedate,
                            'invoicenumber':invoicenumber,
                            'description':description,
                            'quantity':quantity,
                            'amount':amount,
                            'total':total
                        },
                        success:function(data){
                            $('#geninvoice').hide();
                            $('#downloadbtn').html(data);


                        }
                    });

                }

            }
        });

    </script>
    <script>
        function check_user(){
            var index=document.getElementById("data").rows.length;
            var amount=$('#amount'+index).val();
            var quantity=$('#quantity'+index).val();
            $('#total'+index).val(amount*quantity);
        }
        function validateinvoiceform(){

            var validatecustomerid = document.forms["Invoiceform"]["customerinvoiceid"].value;
            var validatedate = document.forms["Invoiceform"]["invoicedate"].value;
            var validateinvoicenumber = document.forms["Invoiceform"]["invoicenumber"].value;
            if (validatecustomerid == "") {
                $("#customerinvoiceid").css({"background-color": "pink"});
                return false;
            }
            else if(validatedate== ""){
                $("#invoicedate").css({"background-color": "pink"});
                return false;
            }
            else if(validateinvoicenumber== ""){
                $("#invoicenumber").css({"background-color": "pink"});
                return false;
            }else{
                $("#customerinvoiceid").css({"background-color": "white"});
                $("#invoicedate").css({"background-color": "white"});
                $("#invoicenumber").css({"background-color": "white"});
                return true;
            }

        }


    </script>



@endsection


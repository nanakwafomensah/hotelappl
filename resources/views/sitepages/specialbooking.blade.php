
@extends('layout.local.localmaster')



@section('content')
    <div id="wrapper">
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">New Booking</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room name</label>
                                <input type="text" id="roomname" class="form-control" name="roomname" placeholder="marchaty " >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Short name</label>
                                <input type="text" id="shortname" class="form-control" name="shortname" placeholder=" Ninyeh" >
                            </div>
                            {{--<div class="form-group">--}}
                            {{--<label for="exampleInputPassword1">Username</label>--}}
                            {{--<input type="text" id="username" class="form-control" name="username" placeholder="Aninyeh">--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="exampleInputPassword1">Base price:</label>
                                <input type="text" id="baseprice" class="form-control" name="baseprice" placeholder="GHC" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Base availability</label>
                                <input type="text" id="baseavailability" name="baseavailablity" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Max Occupancy</label>
                                <input type="number" id="maxoccupancy" name="maxoccupancy" class="form-control">
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <div id="priceModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('setroomprice')}}" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Set price for Room Types</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room type</label>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">start date</label>
                                <input type="date" id="start_dt" class="form-control" name="start_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">enddate:</label>
                                <input type="date" id="end_dt" class="form-control" name="end_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">price:</label>
                                <input type="text" id="price" name="price" class="form-control">
                            </div>

                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Set</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Ticket Modal -->
        <div id="Modal1" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('reserveroom')}}" >
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Customer Reservation</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="customerid">Customer ID:</label>

                                <input type="text" id="customerid" class="form-control" name="customerid"/>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Customer Name:</label>

                                <input type="text" id="customername" class="form-control" name="customername" />
                            </div>
                            {{--<div class="form-group">--}}
                            {{--<label for="exampleInputPassword1">Receipt Number:</label>--}}
                            {{--<input type="text" id="receiptnumber" name="receiptnumber"class="form-control"/>--}}
                            {{--</div>--}}


                            <div class="form-group">
                                <label for="exampleInputPassword1">Start date:</label>
                                <input type="date" id="startdate" name="startdate"class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">End date:</label>
                                <input type="date" id="enddate" name="enddate"class="form-control" />
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">Category:</label>
                                            <input type="text" id="category" class="form-control" name="category" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">PaymentDate:</label>
                                            <input type="date" id="paymentdate" class="form-control" name="paymentdate" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">Receipt Number:</label>
                                            <input type="text" id="receiptnumber" class="form-control" name="receiptnumber" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1"></label>
                                            <button type="button " id="validatebtn" class="form-control btn-circle btn-primary" name="validatebtn" >validate</button>
                                        </div>
                                    </div>

                                </div></div>
                            <div class="form-group">

                                <label for="exampleInputPassword1">Room Info</label>
                                <input type="text" id="namereserve" name="roomname" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Total price for <span id="max_occupancyreserve"></span> rooms </label>
                                <input type="text" id="total_pricereserve" class="form-control" name="total_price" >
                            </div>
                            <input type="hidden" id="roomidreserve" name="roomidreserve" class="form-control" />
                            <input type="hidden" id="postoccupancy" name="occupancy" class="form-control" />
                        </div>
                        <div class="modal-footer" id="reserveid">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>Reserve</button>
                        </div>


                    </form>
                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="delModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Invoice</h4>
                    </div>
                    <form role="form" method="post" action="{{route('generate_invoice')}}" name="Invoiceform">
                        <div class="modal-body">

                                 <div class="row">
                                     <div class="col-md-4">
                                         <div class="form-group">
                                             <label for="customer">Customer:</label>
                                             <input type="text" id="customerinvoiceid" class="form-control" name="customerinvoiceid"/>
                                         </div>
                                     </div>
                                     <div class="col-md-4">
                                         <div class="form-group">
                                             <label for="date">Date:</label>
                                             <input type="date" id="invoicedate" class="form-control" name="invoicedate" />
                                         </div>
                                     </div>
                                     <div  class="col-md-4">
                                         <div class="form-group">
                                             <label for="invoicenumber">Invoice Number:</label>
                                             <input type="text" id="invoicenumber" class="form-control" name="invoicenumber"  value="{{uniqid()}}" disabled/>
                                         </div>
                                     </div>
                                 </div>




                            <table class="table table-hover table-inverse">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th width="10%">quantity</th>
                                    <th>Amount (GHC)</th>
                                    <th>Total (GHC)</th>
                                </tr>
                                </thead>
                                <tbody id="data">
                                <tr class="item">
                                    <th scope="row" id="itemnumber">1</th>
                                    <td>
                                        <div class="form-group">
                                            <input name="description1" class="form-control"  id="description1" type="text" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="quantity1" class="form-control"   onchange="check_user();" id="quantity1" type="number" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="amount1" class="form-control" onchange="check_user();" id="amount1" type="number" disabled/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                           <input name="total1" class="form-control" id="total1" type="number"/>
                                        </div>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                                <input type="hidden" id="number_of_items" name="number_of_items" value="1" />
                                <div class="row">
                                   <div class="col-md-3">
                                       <button type="button" class="btn btn-info" id="addnew">Add New Item</button>
                                   </div>
                               </div>

                        </div>
                        {{csrf_field()}}
                        <div class="modal-footer" id="downloadbtn">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-success" id="geninvoice"><i class="fa fa-save" aria-hidden="true" ></i> Save</button>
                            {{--<a  class="btn btn-success" id="downloadbtn"><i class="fa fa-download" aria-hidden="true" ></i> Download</a>--}}

                        </div>

                    </form>

                </div>
            </div>
        </div>
        <div id="Modal2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('reserveroom')}}" >
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Customer Reservation</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="customerid">Customer ID:</label>

                                <input type="text" id="customerid" class="form-control" name="customerid"/>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Customer Name:</label>

                                <input type="text" id="customername" class="form-control" name="customername" />
                            </div>
                            {{--<div class="form-group">--}}
                            {{--<label for="exampleInputPassword1">Receipt Number:</label>--}}
                            {{--<input type="text" id="receiptnumber" name="receiptnumber"class="form-control"/>--}}
                            {{--</div>--}}


                            <div class="form-group">
                                <label for="exampleInputPassword1">Start date:</label>
                                <input type="text" id="startdate" name="startdate"class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">End date:</label>
                                <input type="text" id="enddate" name="enddate"class="form-control" />
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">Category:</label>
                                            <input type="text" id="category" class="form-control" name="category" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">PaymentDate:</label>
                                            <input type="date" id="paymentdate" class="form-control" name="paymentdate" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">Receipt Number:</label>
                                            <input type="text" id="receiptnumber" class="form-control" name="receiptnumber" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1"></label>
                                            <button type="button " id="validatebtn" class="form-control btn-circle btn-primary" name="validatebtn" >validate</button>
                                        </div>
                                    </div>

                                </div></div>
                            <div class="form-group">

                                <label for="exampleInputPassword1">Room Info</label>
                                <input type="text" id="namereserve" name="roomname" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Total price for <span id="max_occupancyreserve"></span> rooms </label>
                                <input type="text" id="total_pricereserve" class="form-control" name="total_price" >
                            </div>
                            <input type="hidden" id="roomidreserve" name="roomidreserve" class="form-control" />
                            <input type="hidden" id="postoccupancy" name="occupancy" class="form-control" />
                        </div>
                        <div class="modal-footer" id="reserveid">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>Reserve</button>
                        </div>


                    </form>
                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Booking Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Booking Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                {{--<div class="panel-body">--}}

                                    {{--<form method="POST" id="search-form" class="form-inline" role="form">--}}
                                        {{--<div class="col-md-10">--}}

                                            {{--<div class="col-md-3">--}}
                                                {{--<label for="">From</label><br>--}}
                                                {{--<input type="date" id="start_dtid" name="start_dt"/>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-3">--}}
                                                {{--<label for="">To</label><br>--}}
                                                {{--<input type="date" id="end_dtid" name="end_dt"/>--}}
                                            {{--</div>--}}

                                            {{--<div class="col-md-3">--}}
                                                {{--<label for="">Max Occupancy</label><br>--}}
                                                {{--<input type="text" id="max_availabilty" name="max_occupancy"/>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-2">--}}
                                                {{--<label for=""> </label><br>--}}
                                                {{--<div class="row">--}}

                                                    {{--<button type="submit" class="btn btn-primary">Search</button>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}


                                    {{--</form>--}}
                                {{--</div>--}}

                                <table id="availableroomtypes-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Short name</th>
                                        <th>Total price(RATE)</th>
                                        <th>base availabilty</th>
                                        <th>max Occupancy</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script>
        $('#customerid').on('keyup',function (e) {

            var customerid= $('#customerid').val();

            $.ajax({
                type:'get',
                url:'{{URL::to('validatecustomer')}}',
                data:{
                    'customerid':customerid
                },
                success:function(data){
                    $('#customername').val(data.first_name);
                    //$('tbody').html(data);



                }
            });

        })
        {{--$('#customerinvoiceid').on('keyup',function (e) {--}}

            {{--var customerid= $('#customerinvoiceid').val();--}}

            {{--$.ajax({--}}
                {{--type:'get',--}}
                {{--url:'{{URL::to('validatecustomer')}}',--}}
                {{--data:{--}}
                    {{--'customerid':customerid--}}
                {{--},--}}
                {{--success:function(data){--}}
                    {{--alert("ok");--}}



                {{--}--}}
            {{--});--}}

        {{--})--}}
    </script>
    <script>
        $(document).ready(function() {
            $('#reserveid').hide();
        });
        $('#validatebtn').on('click', function(e) {
            e.preventDefault();
            var customerid= $('#customerid').val();
            var category=$('#category').val();
            var paymentdate=$('#paymentdate').val();
            var receiptnumber=$('#receiptnumber').val();
            $.ajax({
                type:'get',
                url:'{{URL::to('validatepayment')}}',
                data:{
                    'customerid':customerid,
                    'category':category,
                    'paymentdate':paymentdate,
                    'receiptnumber':receiptnumber
                },
                success:function(data){
                    //$('#customername').val(data.first_name);
                    //$('tbody').html(data);
                    // alert(data);
                    if(data.status=='open'){
                        //  alert('valid')
                        $('#customerid').css({"background-color": "LightGreen"});
                        $('#category').css({"background-color": "LightGreen"});
                        $('#paymentdate').css({"background-color": "LightGreen"});
                        $('#receiptnumber').css({"background-color": "LightGreen"});
                        $('#reserveid').show();
                    }else{
                        $('#customerid').css({"background-color": "LightPink"});
                        $('#category').css({"background-color": "LightPink"});
                        $('#paymentdate').css({"background-color": "LightPink"});
                        $('#receiptnumber').css({"background-color": "LightPink"});
                    }


                }
            });

        })
    </script>

    <script>

        {{--$('#search-form').on('submit', function(e) {--}}
            {{--e.preventDefault();--}}
            {{--var start_dt= $('#start_dtid').val();--}}
            {{--var end_dt=$('#end_dtid').val();--}}
            {{--var max_availabilty=$('#max_availabilty').val();--}}
            {{--$.ajax({--}}
                {{--type:'get',--}}
                {{--url:'{{URL::to('getavailableroomslocal')}}',--}}
                {{--data:{--}}
                    {{--'start_dt':start_dt,--}}
                    {{--'end_dt':end_dt,--}}
                    {{--'max_occupancy':max_availabilty--}}

                {{--},--}}
                {{--success:function(data){--}}

                    {{--$('tbody').html(data);--}}



                {{--}--}}
            {{--});--}}

        {{--});--}}
    </script>
    <script>
        $(document).on('click','.edit-modal',function() {
            $('#roomidreserve').val($(this).data('id'));
            $('#namereserve').val($(this).data('name'));
            // $('#firstnameedit').val($(this).data('short_name'));
            $('#total_pricereserve').val($(this).data('total_price'));
            // $('#emailedit').val($(this).data('base_availabilty'));
            $('#max_occupancyreserve').html($(this).data('max_occupancy'));
            $('#postoccupancy').val($(this).data('max_occupancy'));

            $('#startdate').val($('#start_dtid').val());
            $('#enddate').val($('#end_dtid').val());
        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {
            //$('#roomidreserve').val($(this).data('id'));
            $('#amount1').val($(this).data('total_price'));
            $('#description1').val($(this).data('name'));


        });
    </script>

    <script>
        $(function() {
            $('#availableroomtypes-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!!route('datatable.specialbook')!!}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'short_name', name: 'short_name' },
                    { data: 'base_price', name: 'base_price' },
                    { data: 'description', name: 'description' },
                    { data: 'max_occupancy', name: 'max_occupancy' },
                    { data : 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    <script>
        $(document).ready(function() {

            var currentItem = 1;
            $('#addnew').click(function(){
                currentItem =currentItem+1;
                $('#number_of_items').val(currentItem);
               var strToAdd='<tr class="item"><th scope="row">'+currentItem+'</th><td><div class="form-group"><input name="description'+currentItem+'" class="form-control"  id="description'+currentItem+'" type="text" /> </div></td><td><div class="form-group"><input name="quantity'+currentItem+'" class="form-control"   onchange="check_user();" id="quantity'+currentItem+'" type="number" /> </div> </td> <td> <div class="form-group"> <input name="amount'+currentItem+'" class="form-control" onchange="check_user();" id="amount'+currentItem+'" type="number" /> </div> </td> <td> <div class="form-group"> <input name="total'+currentItem+'" class="form-control" id="total'+currentItem+'" type="number" /> </div> </td> </tr>';

                $('#data').append(strToAdd);

            });

})
        $('#geninvoice').on('click', function(e) {
            e.preventDefault();
           if(validateinvoiceform()) { //validate invoice form
               var number_of_items = $('#number_of_items').val();
               var i;

               var customerid = $('#customerinvoiceid').val();
               var invoicedate = $('#invoicedate').val();
               var invoicenumber = $('#invoicenumber').val();


               for (i = 1; i <=number_of_items; i++) {
                   var description=$("#description"+i).val();
                   var quantity=$("#quantity"+i).val();
                   var amount=$("#amount"+i).val();
                   var total=$("#total"+i).val();

                   $.ajax({
                       type:'post' ,
                       url: '{{URL::to('generate_invoice')}}',
                       data:{
                           '_token':$('input[name=_token]').val(),
                           'customerid':customerid,
                           'invoicedate':invoicedate,
                           'invoicenumber':invoicenumber,
                           'description':description,
                           'quantity':quantity,
                           'amount':amount,
                           'total':total
                       },
                       success:function(data){
                           $('#geninvoice').hide();
                           $('#downloadbtn').html(data);


                       }
                   });

               }

           }
        });


    </script>
<script>
    function check_user(){
        var index=document.getElementById("data").rows.length;
        var amount=$('#amount'+index).val();
        var quantity=$('#quantity'+index).val();
        $('#total'+index).val(amount*quantity);
        }
    function validateinvoiceform(){

        var validatecustomerid = document.forms["Invoiceform"]["customerinvoiceid"].value;
        var validatedate = document.forms["Invoiceform"]["invoicedate"].value;
        var validateinvoicenumber = document.forms["Invoiceform"]["invoicenumber"].value;
        if (validatecustomerid == "") {
            $("#customerinvoiceid").css({"background-color": "pink"});
            return false;
        }
        else if(validatedate== ""){
            $("#invoicedate").css({"background-color": "pink"});
            return false;
        }
        else if(validateinvoicenumber== ""){
            $("#invoicenumber").css({"background-color": "pink"});
            return false;
        }else{
            $("#customerinvoiceid").css({"background-color": "white"});
            $("#invoicedate").css({"background-color": "white"});
            $("#invoicenumber").css({"background-color": "white"});
            return true;
        }

    }


</script>
    <script>
        {{--$('#downloadbtn').on('click',function (e) {--}}
            {{--e.preventDefault();--}}
          {{--var x=1;--}}
            {{--window.location.href="{{URL::to('pdfinvoice/')}}";--}}
        {{--})--}}
    </script>
@endsection


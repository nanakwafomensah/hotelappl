
@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('storeevent')}}" method="post"  id="add_form" data-parsley-validate="">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">New Event</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Name of Event</label>
                                <input type="text" id="firstname" class="form-control" name="eventname" required="" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Purpose of Event</label>
                                <textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder="This textarea has a limit of 225 chars." name="purpose"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Event Type</label>
                                <select class="form-control select2" name="eventtype">
                                    <option>Select</option>
                                    <option value="AK">Course, seminar, training</option>
                                    <option value="HI">Conference, meeting </option>
                                    <option value="CA">Christmas party</option>
                                    <option value="NV">Nevada</option>
                                    <option value="OR">Oregon</option>
                                    <option value="WA">Washington</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-4">
                                    <label for="exampleInputPassword1">Period:</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="date" id="telephone" class="form-control" name="startdate" required="">
                                </div>
                                <div class="col-lg-4">
                                    <input type="date" id="telephone" class="form-control" name="enddate" required="">
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label for="exampleInputPassword1">Event Coordinator:</label>
                                        <input type="text" id="location" class="form-control" required="" name="coordinator1">

                                    </div>
                                    <div class="col-lg-4">
                                        <label for="exampleInputPassword1">phone number:</label>
                                        <input type="text" id="location" class="form-control" name="coorphone1" required="">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label for="exampleInputPassword1">Event Coordinator2:</label>
                                        <input type="text" id="location" class="form-control"  required="" name="coordinator2">
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="exampleInputPassword1">phone number:</label>
                                        <input type="text" id="location" class="form-control" required=""name="coorphone2">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">refrence Email</label>
                                <input type="email" class="form-control"   name="email"/>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="addevent" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>

                        </div>

                    </form>

                </div>
            </div>
        </div>

        <!-- Support Ticket Modal -->
        <div id="Modal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form action="{{route('updateevent')}}" method="post"  id="update_form" data-parsley-validate="">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Update Event Details</h4>
                        </div>
                        <div class="modal-body">
                              <input type="hidden" id="updateeventid" name="updateeventid"/>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Name of Event</label>
                                <input type="text" id="updatefirstname" class="form-control" name="updateeventname" required="" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Purpose of Event</label>
                                <textarea id="updatetextarea" class="form-control" maxlength="225" rows="3" placeholder="This textarea has a limit of 225 chars." name="updatepurpose"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Event Type</label>
                                <select class="form-control select2" name="updateeventtype">
                                    <option>Select</option>
                                    <option value="AK">Course, seminar, training</option>
                                    <option value="HI">Conference, meeting </option>
                                    <option value="CA">Christmas party</option>
                                    <option value="NV">Nevada</option>
                                    <option value="OR">Oregon</option>
                                    <option value="WA">Washington</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-4">
                                    <label for="exampleInputPassword1">Period:</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="date" id="updatestartdate" class="form-control" name="updatestartdate" required="">
                                </div>
                                <div class="col-lg-4">
                                    <input type="date" id="updateenddate" class="form-control" name="updateenddate" required="">
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label for="exampleInputPassword1">Event Coordinator:</label>
                                        <input type="text" id="updatecoordinator1" class="form-control" required="" name="updatecoordinator1">

                                    </div>
                                    <div class="col-lg-4">
                                        <label for="exampleInputPassword1">phone number:</label>
                                        <input type="text" id="updatecoorphone1" class="form-control" name="updatecoorphone1" required="">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"> <label for="exampleInputPassword1">Event Coordinator2:</label>
                                        <input type="text" id="updatecoordinator2" class="form-control"  required="" name="updatecoordinator2">
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="exampleInputPassword1">phone number:</label>
                                        <input type="text" id="updatecoorphone2" class="form-control" required=""name="updatecoorphone2">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">refrence Email</label>
                                <input type="email" class="form-control"   name="updateemail" id="updateemail"/>
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="updateevent" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>

                        </div>

                    </form>
                   </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="delModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Customer</h4>
                    </div>
                    <form role="form" method="post" action="{{route('deletecustomer')}}">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="customerid" id="customeriddelete">
                                <p>Are you sure you want to delete <b><span id="firstnamedelete" name="namedelete"></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Event Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Event Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <p class="text-muted font-13 m-b-30">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-user" aria-hidden="true">&nbsp;</i> New Event</button>
                                </p>



                                <table id="events-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name of Event</th>
                                        <th>Purpose</th>
                                        <th>Event Type</th>
                                        <th>Event Coordinator</th>
                                        <th>Email Address</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script>
        $(function() {
            $('#events-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!!route('datatable.eventsdetails')!!}',
                columns: [
                    { data: 'eventname', name: 'eventname' },
                    { data: 'purpose', name: 'purpose' },
                    { data: 'eventtype', name: 'eventtype' },
                    { data: 'coordinator', name: 'coordinator', orderable: false, searchable: false },
                    { data: 'email', name: 'email' },
                    { data : 'action', name: 'action', orderable: false, searchable: false}
                ]
            });


            $(document).on('click','.edit-modal',function() {

                var x=$(this).data('id');


                var eventid=$(this).data('id') ;
                $.ajax({
                    type:'get' ,
                    url: '{{URL::to('event/getevent')}}',
                    data:{
                        '_token':$('input[name=_token]').val(),
                        'eventid':eventid

                    },
                    success:function(data){
                        console.log(data.id);
                        $('#updatefirstname').val(data.eventname);
                        $('#updateeventid').val(data.id);
                       $('#updatetextarea').val(data.purpose);
                       $('#updatestartdate').val(data.startdate);
                       $('#updateenddate').val(data.enddate);
                       $('#updatecoordinator1').val(data.coordinator1);
                       $('#updatecoordinator2').val(data.coordinator2);
                       $('#updatecoorphone1').val(data.coorphone1);
                       $('#updatecoorphone2').val(data.coorphone2);
                       $('#updateemail').val(data.email);
                    }
                });

            });
        });
    </script>
@endsection


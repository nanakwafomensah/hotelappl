
@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">New Booking</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room name</label>
                                <input type="text" id="roomname" class="form-control" name="roomname" placeholder="marchaty " >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Short name</label>
                                <input type="text" id="shortname" class="form-control" name="shortname" placeholder=" Ninyeh" >
                            </div>
                            {{--<div class="form-group">--}}
                            {{--<label for="exampleInputPassword1">Username</label>--}}
                            {{--<input type="text" id="username" class="form-control" name="username" placeholder="Aninyeh">--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="exampleInputPassword1">Base price:</label>
                                <input type="text" id="baseprice" class="form-control" name="baseprice" placeholder="eaglesecurity0@gmail.com" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Base availability</label>
                                <input type="text" id="baseavailability" name="baseavailablity" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Max Occupancy</label>
                                <input type="text" id="maxoccupancy" name="maxoccupancy" class="form-control">
                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <div id="priceModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{route('setroomprice')}}" method="post"  id="add_form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Set price for Room Types</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="exampleInputPassword1">Room type</label>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">start date</label>
                                <input type="date" id="start_dt" class="form-control" name="start_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">enddate:</label>
                                <input type="date" id="end_dt" class="form-control" name="end_dt" placeholder="" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">price:</label>
                                <input type="text" id="price" name="price" class="form-control">
                            </div>

                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

                            <button type="submit" id="add-row" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Set</button>
                        </div>

                    </form>
                    <div id="savemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Ticket Modal -->
        <div id="Modal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('reserveroom')}}" >
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Customer Reservation</h4>
                        </div>
                        <div class="modal-body">
                        <div class="form-group">
                                <label for="customerid">Customer ID:</label>

                                <input type="text" id="customerid" class="form-control" name="customerid"/>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Customer Name:</label>

                                <input type="text" id="customername" class="form-control" name="customername" />
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="exampleInputPassword1">Receipt Number:</label>--}}
                                {{--<input type="text" id="receiptnumber" name="receiptnumber"class="form-control"/>--}}
                            {{--</div>--}}


                            <div class="form-group">
                                <label for="exampleInputPassword1">Start date:</label>
                                <input type="text" id="startdate" name="startdate"class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">End date:</label>
                                <input type="text" id="enddate" name="enddate"class="form-control" />
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">Category:</label>
                                            <input type="text" id="category" class="form-control" name="category" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">PaymentDate:</label>
                                            <input type="date" id="paymentdate" class="form-control" name="paymentdate" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">Receipt Number:</label>
                                            <input type="text" id="receiptnumber" class="form-control" name="receiptnumber" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1"></label>
                                            <button type="button " id="validatebtn" class="form-control btn-circle btn-primary" name="validatebtn" >validate</button>
                                        </div>
                                    </div>

                                </div></div>
                              <div class="form-group">

                                <label for="exampleInputPassword1">Room Info</label>
                                <input type="text" id="namereserve" name="roomname" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Total price for <span id="max_occupancyreserve"></span> rooms </label>
                                <input type="text" id="total_pricereserve" class="form-control" name="total_price" >
                            </div>
                            <input type="hidden" id="roomidreserve" name="roomidreserve" class="form-control" />
                            <input type="hidden" id="postoccupancy" name="occupancy" class="form-control" />
                        </div>
                        <div class="modal-footer" id="reserveid">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>Reserve</button>
                        </div>


                    </form>
                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="delModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete User</h4>
                    </div>
                    <form role="form" method="post" action="">
                        <div class="modal-body">
                            <div class="form-group">
                                {{csrf_field()}}
                                <input type="hidden" name="roomtypeid" id="roomtypeiddelete">
                                <p>Are you sure you want to delete <b><span id="namedelete" name="namedelete"></span></b>?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div id="Modal2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->

                <div class="modal-content">
                    <form  method="post" action="{{route('reserveroom')}}" >
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Customer Reservation</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="customerid">Customer ID:</label>

                                <input type="text" id="customerid" class="form-control" name="customerid"/>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Customer Name:</label>

                                <input type="text" id="customername" class="form-control" name="customername" />
                            </div>
                            {{--<div class="form-group">--}}
                            {{--<label for="exampleInputPassword1">Receipt Number:</label>--}}
                            {{--<input type="text" id="receiptnumber" name="receiptnumber"class="form-control"/>--}}
                            {{--</div>--}}


                            <div class="form-group">
                                <label for="exampleInputPassword1">Start date:</label>
                                <input type="text" id="startdate" name="startdate"class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">End date:</label>
                                <input type="text" id="enddate" name="enddate"class="form-control" />
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">Category:</label>
                                            <input type="text" id="category" class="form-control" name="category" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">PaymentDate:</label>
                                            <input type="date" id="paymentdate" class="form-control" name="paymentdate" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1">Receipt Number:</label>
                                            <input type="text" id="receiptnumber" class="form-control" name="receiptnumber" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="exampleInputPassword1"></label>
                                            <button type="button " id="validatebtn" class="form-control btn-circle btn-primary" name="validatebtn" >validate</button>
                                        </div>
                                    </div>

                                </div></div>
                            <div class="form-group">

                                <label for="exampleInputPassword1">Room Info</label>
                                <input type="text" id="namereserve" name="roomname" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Total price for <span id="max_occupancyreserve"></span> rooms </label>
                                <input type="text" id="total_pricereserve" class="form-control" name="total_price" >
                            </div>
                            <input type="hidden" id="roomidreserve" name="roomidreserve" class="form-control" />
                            <input type="hidden" id="postoccupancy" name="occupancy" class="form-control" />
                        </div>
                        <div class="modal-footer" id="reserveid">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                            <button type="submit" id="add-row" class="edit btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i>Reserve</button>
                        </div>


                    </form>
                    <div id="updatemodal">
                        @include('errors.online.validationerrors')
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Booking Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Booking Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <div class="panel-body">

                                    <form method="POST" id="search-form" class="form-inline" role="form">
                                        <div class="col-md-10">

                                            <div class="col-md-3">
                                                <label for="">From</label><br>
                                                <input type="date" id="start_dtid" name="start_dt"/>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">To</label><br>
                                                <input type="date" id="end_dtid" name="end_dt"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label for="">Max Occupancy</label><br>
                                                 <input type="text" id="max_availabilty" name="max_occupancy"/>
                                            </div>
                                            <div class="col-md-2">
                                                <label for=""> </label><br>
                                                <div class="row">

                                                    <button type="submit" class="btn btn-primary">Search</button>
                                                </div>

                                            </div>
                                        </div>


                                    </form>
                                </div>

                                <table id="roomtypesavailable-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Short name</th>
                                        <th>Total price(RATE)</th>
                                        <th>base availabilty</th>
                                        <th>max Occupancy</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                     <tbody>

                                     </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>
    $('#customerid').on('keyup',function (e) {

        var customerid= $('#customerid').val();

        $.ajax({
            type:'get',
            url:'{{URL::to('validatecustomer')}}',
            data:{
                'customerid':customerid
             },
            success:function(data){
                $('#customername').val(data.first_name);
                //$('tbody').html(data);



            }
        });

    })
</script>
<script>
    $(document).ready(function() {
        $('#reserveid').hide();
    });
    $('#validatebtn').on('click', function(e) {
        e.preventDefault();
        var customerid= $('#customerid').val();
        var category=$('#category').val();
        var paymentdate=$('#paymentdate').val();
        var receiptnumber=$('#receiptnumber').val();
        $.ajax({
            type:'get',
            url:'{{URL::to('validatepayment')}}',
            data:{
                'customerid':customerid,
                'category':category,
                'paymentdate':paymentdate,
                'receiptnumber':receiptnumber
            },
            success:function(data){
                //$('#customername').val(data.first_name);
                //$('tbody').html(data);
             // alert(data);
               if(data.status=='open'){
                //  alert('valid')
                   $('#customerid').css({"background-color": "LightGreen"});
                   $('#category').css({"background-color": "LightGreen"});
                   $('#paymentdate').css({"background-color": "LightGreen"});
                   $('#receiptnumber').css({"background-color": "LightGreen"});
                   $('#reserveid').show();
            }else{
                   $('#customerid').css({"background-color": "LightPink"});
                   $('#category').css({"background-color": "LightPink"});
                   $('#paymentdate').css({"background-color": "LightPink"});
                   $('#receiptnumber').css({"background-color": "LightPink"});
               }


            }
        });

    })
</script>

    <script>

        $('#search-form').on('submit', function(e) {
            e.preventDefault();
                var start_dt= $('#start_dtid').val();
                var end_dt=$('#end_dtid').val();
                var max_availabilty=$('#max_availabilty').val();
            $.ajax({
                    type:'get',
                    url:'{{URL::to('getavailableroomslocal')}}',
                    data:{
                    'start_dt':start_dt,
                    'end_dt':end_dt,
                    'max_occupancy':max_availabilty

                    },
                    success:function(data){

                    $('tbody').html(data);



                    }
        });

    });
</script>
    <script>
        $(document).on('click','.edit-modal',function() {
            $('#roomidreserve').val($(this).data('id'));
            $('#namereserve').val($(this).data('name'));
           // $('#firstnameedit').val($(this).data('short_name'));
            $('#total_pricereserve').val($(this).data('total_price'));
           // $('#emailedit').val($(this).data('base_availabilty'));
            $('#max_occupancyreserve').html($(this).data('max_occupancy'));
             $('#postoccupancy').val($(this).data('max_occupancy'));

            $('#startdate').val($('#start_dtid').val());
            $('#enddate').val($('#end_dtid').val());
        });
    </script>
@endsection


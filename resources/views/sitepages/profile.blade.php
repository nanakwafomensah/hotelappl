
@extends('layout.local.localmaster')



@section('content')
    <div id="wrapper">

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">

                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form role="form" action="{{route('updateprofile')}}" method="post" enctype="multipart/form-data">
                                    @if(!empty($hoteldetails->id))
                                    <input name="profileid" value="{{$hoteldetails->id}}" type="hidden"/>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4">

                                                </div>
                                                <div class="col-md-4">

                                                </div>
                                                <div class="col-md-4">
                                                    <section class="panel panel-default">
                                                        <div class="panel-body">
                                                            @if(!empty($hoteldetails->avatar))
                                                            <div><img src="uploads/avatars/{{$hoteldetails->avatar}}" width="270px" height="270px" class="img-responsive text-center" ></div>
                                                             @else
                                                                <div><img src="uploads/avatars/default.jpg" width="270px" height="270px" class="img-responsive text-center" ></div>

                                                            @endif
                                                                <br>
                                                            <br>
                                                            {{--//<form enctype="multipart/form-data"  action=""></form>--}}
                                                            <input type="file" id="imgInp" name="avatar" class="form-control">
                                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Hotel Name </label><label class="text-danger">*</label>
                                                <input type="text" class="form-control"  name="hotelname" value="
                                                {{$hoteldetails->hotelname}}
                                                   ">
                                            </div>

                                            <div class="form-group">
                                                <label>Address</label><label class="text-danger">&nbsp;*</label>
                                                <textarea class="form-control" rows="4"  name="address" required="">
                                                        {{$hoteldetails->address}}
                                                 </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Website</label>
                                                <input type="text" class="form-control"  name="website" value="{{$hoteldetails->website}}">
                                            </div>
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control"  name="city" value="
                                                {{$hoteldetails->city}}
                                               ">
                                            </div>
                                            <div class="form-group">
                                                <label>Country</label>
                                                <input type="text" class="form-control"  name="country" value="
                                                {{$hoteldetails->country}}
                                               ">
                                            </div>


                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control"  name="email" value="
                                                {{$hoteldetails->email}}
                                               ">
                                            </div>

                                            <div class="form-group">
                                                <label>Phone</label><label class="text-danger">&nbsp;*</label>
                                                <input type="text" class="form-control"  name="phone" value="
                                          {{$hoteldetails->phone}}
                                                ">
                                            </div>
                                            <div class="form-group">
                                                <label>Mobile</label>
                                                <input type="text" class="form-control"  name="telephone" value="
                                                {{$hoteldetails->telephone}}
                                                ">
                                            </div>
                                             {{csrf_field()}}
                                            <button type="submit" class="btn btn-sm btn-success pull-right">Save Changes</button>

                                        </div>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>




@endsection


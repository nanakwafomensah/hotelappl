
@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper">
        <!-- Modal -->


        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Send Broadcast </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Send Broadcast
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <div id="exTab2" class="container">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a  href="#1" data-toggle="tab">Single SMS</a>
                                        </li>
                                        <li><a href="#2" data-toggle="tab">Bulk SMS</a>
                                        </li>
                                        <li><a href="#3" data-toggle="tab">Statistics</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content ">
                                        <div class="tab-pane active" id="1">
                                            <form action="" method="post"  id="send_sms" name="single_sms_form">

                                                           <div class="col-md-6">
                                                               <div class="form-group">
                                                                   <label for="exampleInputPassword1">Number(s)</label>
                                                                   <input type="text" id="number" class="form-control" name="number" placeholder="" >
                                                               </div>
                                                               <div class="form-group">
                                                                   <label for="exampleInputPassword1">Name of receipient</label>
                                                                   <input type="text" id="name_of_receipient" class="form-control" name="name_of_receipient" placeholder="" >
                                                               </div>
                                                               <div class="form-group">
                                                                   <label for="exampleInputPassword1">Sender</label>
                                                                   <input type="text" id="sender" class="form-control" name="sender" placeholder="" >
                                                               </div>
                                                               <div class="form-group">
                                                                   <label for="exampleInputPassword1">Message:</label>
                                                                   <textarea id="messagecontent" name="messagecontent" class="form-control"></textarea>
                                                               </div>
                                                                   <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                               <div class="form-group">

                                                               <button type="submit" id="sendsingle" class="save btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Send</button>
                                                               </div>
                                                                <div class="success_message">

                                                                </div>
                                                           </div>
                                                           <div class="col-md-6">

                                                               <div class="card-box">
                                                                   <h4 class="header-title m-t-0 m-b-30">Guest Contacts</h4>

                                                                   <div class="table-responsive">
                                                                       <table class="table table table-hover m-0"  id="customercontacts">
                                                                           <thead>
                                                                           <tr>
                                                                               <th>Status</th>
                                                                               <th>User Name</th>
                                                                               <th>Phone</th>


                                                                           </tr>
                                                                           </thead>

                                                                           <tbody>



                                                                           </tbody>


                                                                       </table>

                                                                   </div> <!-- table-responsive -->
                                                               </div> <!-- end card -->

                                                           </div>


                                            </form>
                                    </div>
                                        <div class="tab-pane" id="2">
                                            <div class="col-md-8">
                                                   <div id="validate_message"></div>
                                                <form>
                                                   <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="row " id="formid">
                                                                <div class="col-md-2">
                                                                    <label class="btn btn-default">
                                                                    <input id="checkedinbutton" type="checkbox" value="checkin" class="group_item"/>CheckedIn guest
                                                                        </label>
                                                                 </div>
                                                                <div class="col-md-2">
                                                                    <label class="btn btn-default">
                                                                    <input id="checkedoutbutton" type="checkbox" value="checkedout" class="group_item"/>CheckedOut guest
                                                                        </label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label class="btn btn-default">
                                                                    <input id="allguest" type="checkbox" value="allguest" class="group_item"/>All guest
                                                                        </label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label class="btn btn-default">
                                                                    <input id="allemployees" type="checkbox" value="allemp" class="group_item"/>All employees
                                                                        </label>
                                                                </div>
                                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                            <div class="col-md-2"> <button type="button" class="btn btn-warning" id="loadcontact"><span class="glyphicon glyphicon-open" aria-hidden="true"></span>Load Contacts for SMS</button>
                                                            </div>
                                                 </div>

                                                    </div>
                                                </div>
                                                     <br>
                                                   <div class="panel panel-default" style="display: none;" id="show_date_panel">
                                                    <div class="panel-body">
                                                <div class="row ">
                                                    <div class="col-md-4">
                                                        <label>From</label>
                                                        <input type="date" name="smsdate" id="from"/>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <label>To</label>
                                                        <input type="date" name="smsdate" id="to"/>
                                                    </div>
                                                </div>
                                                        </div>
                                                </div>
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                </form>
                                                   <table class="table table table-hover m-0"  id="groupcontacttable">
                                                     <thead>
                                                        <tr>
                                                            <th><input type="checkbox" id="checkAll" name="checkAll" />Select All</th>
                                                            <th>name</th>
                                                            <th>Phone Number</th>


                                                        </tr>
                                                    </thead>

                                                    <tbody id="bulkcontact">



                                                    </tbody>


                                                  </table>
                                            </div>


                                            <div class="col-md-4" >
                                            <form>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1"><strong>Sender:</strong></label>
                                                    <input type="text" id="sender" class="form-control" name="sender" placeholder="" >
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1"><strong>Message:</strong></label>
                                                    <textarea id="messagecontent" name="messagecontent" class="form-control"></textarea>
                                                </div>
                                            </form></br>

                                                <button type="button" class="btn btn-success btn-lg" id="send_bulk_sms">Send</button>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="3">
                                            <h3>add clearfix to tab-content (see the css)</h3>
                                        </div>
                                    </div>
                                </div>





                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


        <script>
            $('#customercontacts').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!!route('datatable.usercontact')!!}',
                columns: [
                    {data: 'status', name: 'status'},
                    {data: 'customname', name: 'customname'},
                    {data: 'telephone', name: 'telephone'}

                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }
            });
        </script>


        <script>
        $(document).on('click','#customerstatus',function(e) {
            e.preventDefault();

        $('#number').val($(this).data('telephone'));
         $('#name_of_receipient').val($(this).data('name'));

        });
        </script>
       <script>
           $(document).on('click','#sendsingle',function(e) {
               e.preventDefault();
               if(validatesinglesms()){
                   var number = $('#number').val();
                   var name_of_receipient = $('#name_of_receipient').val();
                   var sender = $('#sender').val();
                   var messagecontent = $('#messagecontent').val();
                   $.ajax({
                       type:'post' ,
                       url: '{{URL::to('sms/single')}}',
                       data:{
                           '_token':$('input[name=_token]').val(),
                           'number':number,
                           'name_of_receipient':name_of_receipient,
                           'sender':sender,
                           'messagecontent':messagecontent

                       },
                       success:function(data){
                           //alert(data);
                           $('.success_message').html(data);

                       }
                   });
               }
           });
               function validatesinglesms(){

               var validatenumber = document.forms["single_sms_form"]["number"].value;
               var validatereceipient = document.forms["single_sms_form"]["name_of_receipient"].value;
               var sender = document.forms["single_sms_form"]["sender"].value;
               var message = document.forms["single_sms_form"]["messagecontent"].value;

               if (validatenumber == "") {
                   $("#number").css({"background-color": "pink"});
                   return false;
               }
               else if(validatereceipient== ""){
                   $("#name_of_receipient").css({"background-color": "pink"});
                   return false;
               }
               else if(sender== ""){
                   $("#sender").css({"background-color": "pink"});
                   return false;
               }
               else if(message==""){
                   $("#messagecontent").css({"background-color": "pink"});
                   return false;
               }else{
                   $("#number").css({"background-color": "white"});
                   $("#name_of_receipient").css({"background-color": "white"});
                   $("#sender").css({"background-color": "white"});
                   $("#messagecontent").css({"background-color": "white"});
                   return true;
               }

           }

       </script>
    <script>
        $('#checkedinbutton,#checkedoutbutton').click(function() {
            $('#show_date_panel').toggle(this.checked);
        });
    </script>
    <script>
        var checkedvalue;
        $(document).on('click','#loadcontact',function(e) {

           if($('.group_item:checkbox:checked').length > 1){
               $("#validate_message").html('<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Message!</strong>Only One group can be selected</div>');
           }else{
               loadgroupcontact($('.group_item:checkbox:checked').val());
           }


        });
        function loadgroupcontact(checkedvalue){
           if(checkedvalue=='checkin'){
               var date_from = $('#from').val();
               var date_to = $('#to').val();
               var value='checkin';

           }

            $.ajax({
                type:'post' ,
                url: '{{URL::to('sms/bulk')}}',
                data:{
                    '_token':$('input[name=_token]').val(),
                    'date_from':date_from,
                    'date_to':date_to,
                    'value':value


                },
                success:function(data){

                   $('#bulkcontact').html(data);

                }
            });
        }

        $(document).on('click','#send_bulk_sms',function(e) {
            $('#bulkcontact').find('input[type="checkbox"]:checked').each(function () {
                var name=$(this).data('name');
                var telephone=$(this).data('telephone');
                $.ajax({
                    type:'post' ,
                    url: '{{URL::to('sms/sendbulk')}}',
                    data:{
                        '_token':$('input[name=_token]').val(),
                        'name':name,
                        'telephone':telephone



                    },
                    success:function(data){
                             alert(data);


                    }
                });

            });
        });

        $('input[type="checkbox"][name="checkAll"]').change(function() {
            if(this.checked) {
                var table = document.getElementById('groupcontacttable');
                var val = table.rows[0].cells[0].children[0].checked;
                for (var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].cells[0].children[0].checked = val;
                }
            }else{
                var table = document.getElementById('groupcontacttable');
                var val = table.rows[0].cells[0].children[0].unchecked;
                for (var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].cells[0].children[0].checked = val;
                }
            }
        });
    </script>

@endsection


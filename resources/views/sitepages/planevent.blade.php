
@extends('layout.local.localmaster')
        <!-- Plugins css-->

<link href="http://coderthemes.com/zircos_1.6/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="http://coderthemes.com/zircos_1.6/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />


@section('content')
    <div id="wrapper">
        <!-- Modal -->


        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Event Management </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Event Management
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="demo-box">
                                <p class="text-muted m-b-15 font-20">
                                    Name of Event:
                                </p>

                                <select class="selectpicker" data-live-search="true"  data-style="btn-info" name="selectpicker" >
                                      @foreach($listofevents as $s)
                                          @if($eventid==$s->id)
                                            <option value="{{$s->id}}"  selected >{{$s->eventname}}</option>
                                              @else
                                            <option value="{{$s->id}}">{{$s->eventname}}</option>
                                        @endif

                                    @endforeach


                                </select>

                               </div>
                        </div>
                        <div class="col-lg-4">

                        </div>
                        <div class="col-lg-4">


                        </div>

                    </div>
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Plan for <span id="name_of_event" ></span></b></h4>




                                <form class="form-horizontal" role="form" method="post" action="{{route('postplanevent')}}">
                                    {{csrf_field()}}
                                    <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" name="eventid" id="eventid">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Size</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="size" id="size">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Gender mix</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="gender_mix" id="gender_mix">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Age of attendees</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="age_of_attendees" id="age_of_attendees">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Any special needs</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control" rows="5" name="special_needs" id="special_needs"></textarea>
                                        </div>
                                    </div>
                                    </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">List food and beverage requirements</label>
                                                <div class="col-md-10">
                                                    <textarea class="form-control" rows="5" name="food_requirements" id="food_requirements"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">Schedule speaker(s), if required.</label>
                                                <div class="col-md-10">
                                                    <textarea class="form-control" rows="5" name="schedule_speakers" id="schedule_speakers"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4"></div>
                                                <div class="col-md-4"></div>
                                                <div class="col-md-2">
                                                    <p class="text-muted font-13 m-b-30">
                                                        &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success" ><i class="fa fa-save" aria-hidden="true">&nbsp;</i> Save Changes</button>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                   </div>


                                </form>


                        <a  name="previewevent" id="previewevent" class="btn btn-info">Click to Preview</a>

                    </div> <!-- content -->

            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>


    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>




    <script type="text/javascript" src="http://coderthemes.com/zircos_1.6/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
    <script src="http://coderthemes.com/zircos_1.6/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="http://coderthemes.com/zircos_1.6/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
 <script>
//
                $(function() {
                    var eventid=$('select[name=selectpicker]').val() ;
                    $.ajax({
                        type:'get' ,
                        url: '{{URL::to('event/getevent')}}',
                        data:{
                            '_token':$('input[name=_token]').val(),
                            'eventid':eventid

                        },
                        success:function(data){
                            console.log(data.id);
                             $('#name_of_event').html(data.eventname);
                            $('#eventid').val(data.id);
                            $('#size').val(data.size);
                            $('#gender_mix').val(data.gender_mix);
                            $('#age_of_attendees').val(data.age_of_attendees);
                            $('#special_needs').val(data.special_needs);
                            $('#food_requirements').val(data.food_requirements);
                            $('#schedule_speakers').val(data.schedule_speakers);
                        }
                    });

                    });

            </script>
    <script>
        $('.selectpicker').on('change', function() {

            var eventid=$('select[name=selectpicker]').val() ;
            $.ajax({
                type:'get' ,
                url: '{{URL::to('event/getevent')}}',
                data:{
                    '_token':$('input[name=_token]').val(),
                    'eventid':eventid

                },
                success:function(data){
                    console.log(data.id);
                    $('#name_of_event').html(data.eventname);
                    $('#eventid').val(data.id);
                    $('#size').val(data.size);
                    $('#gender_mix').val(data.gender_mix);
                    $('#age_of_attendees').val(data.age_of_attendees);
                    $('#special_needs').val(data.special_needs);
                    $('#food_requirements').val(data.food_requirements);
                    $('#schedule_speakers').val(data.schedule_speakers);
                }
            });

        });



    </script>
<script>
    $('#previewevent').on('click', function() {
       // var x=$('#previewevent').attr('href');
        var plan_id=$('#eventid').val();
        //alert(plan_id);
        var url="eventplanpdf"+"/"+plan_id;
        //alert("hey");
        window.location.href = url;

    });
</script>
@endsection


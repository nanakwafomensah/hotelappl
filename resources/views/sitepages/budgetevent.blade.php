
@extends('layout.local.localmaster')

@section('content')

    <link href="http://coderthemes.com/zircos_1.6/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="http://coderthemes.com/zircos_1.6/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />

    <style>
        /* Style the buttons that are used to open and close the accordion panel */
        button.accordion {
            background-color: #777777;
            color: #FFFFFF;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            text-align: left;
            border: none;
            outline: none;
            transition: 0.4s;

        }

        /* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
        button.accordion.active, button.accordion:hover {
            background-color: #2B384E;
        }

        /* Style the accordion panel. Note: hidden by default */
        div.panel {
            padding: 0 18px;
            background-color: white;
            display: none;
        }
    </style>

    <div id="wrapper">
        <!-- Modal -->



        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Budgeting </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                        Budgeting
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="demo-box">
                                <p class="text-muted m-b-15 font-20">
                                    Name of Event:
                                </p>

                                <select class="selectpicker" data-live-search="true"  data-style="btn-info" name="selectpicker" style="display:inline !important;" >
                                    @foreach($listofevents as $s)

                                        <option value="{{$s->id}}">{{$s->eventname}}</option>


                                    @endforeach


                                </select>

                            </div>
                        </div>
                        <div class="col-lg-4">

                        </div>
                        <div class="col-lg-4">

                         
                        </div>

                    </div>
<div>
    <input type="hidden" name="eventid" id="eventid">
<button class="accordion "  ><center>Venue</center></button>
    <div class="panel">
        <div class="row">
            <div class="col-md-12">
                <div class="demo-box">


                    <div class="table-responsive">
                        <table class="table m-0 table-colored table-info" id="table_venue">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>category</th>
                                <th>projected subtotal</th>
                                <th>actual subtotal</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody id="data_budget">
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>
                                            <div class="form-group">
                                                <input name="category_venue1" class="form-control"  id="category_venue1" type="text" value="Location Rental" disabled/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="projected_venue1" class="form-control"  id="projected_venue1" type="text" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="actual_venue1" class="form-control"  id="actual_venue1" type="text" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="comment_venue1" class="form-control"  id="comment_venue1" type="text" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>
                                            <div class="form-group">
                                                <input name="category_venue2" class="form-control"  id="category_venue2" type="text" value="Event Staff" disabled/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="projected_venue2" class="form-control"  id="projected_venue2" type="text" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="actual_venue2" class="form-control"  id="actual_venue2" type="text" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="comment_venue2" class="form-control"  id="comment_venue2" type="text" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>
                                            <div class="form-group">
                                                <input name="category_venue3" class="form-control"  id="category_venue3" type="text" value="Equipment Rental" disabled/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="projected_venue3" class="form-control"  id="projected_venue3" type="text" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="actual_venue3" class="form-control"  id="actual_venue3" type="text" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="comment_venue2" class="form-control"  id="comment_venue2" type="text" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>
                                            <div class="form-group">
                                                <input name="category_venue4" class="form-control"  id="category_venue4" type="text" value="Equipment Rental" disabled/>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="projected_venue4" class="form-control"  id="projected_venue4" type="text" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="actual_venue4" class="form-control"  id="actual_venue4" type="text" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input name="comment_venue4" class="form-control"  id="comment_venue4" type="text" />
                                            </div>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="number_of_items_venue" name="number_of_items_venue" value="4" />
                        <button   type="button"  class="btn-info" id="add_row_budget">+</button>
                        <button   type="button"  class="btn-danger" id="remove_row_budget">-</button>
                        <button   type="submit"  class="btn-success" id="save_venue">Save Changes</button>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <button class="accordion"><center>Travel</center></button>
    <div class="panel">
        <div class="row">
            <div class="col-md-12">
                <div class="demo-box">

                    <div class="table-responsive">
                        <table class="table m-0 table-colored table-info" id="table_travel">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>category</th>
                                <th>projected subtotal</th>
                                <th>actual subtotal</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody id="data_travel">
                            <tr>
                                <th scope="row">1</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_travel1" class="form-control"  id="category_travel1" type="text" value="FLight/Driving" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_travel1" class="form-control"  id="projected_travel1" type="text"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_travel1" class="form-control"  id="actual_travel1" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_travel1" class="form-control"  id="comment_travel1" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_travel2" class="form-control"  id="category_travel2" type="text" value="Lodging" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_travel2" class="form-control"  id="projected_travel2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_travel2" class="form-control"  id="actual_travel2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_travel2" class="form-control"  id="comment_travel2" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_travel3" class="form-control"  id="category_travel3" type="text" value="Per Diem" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_travel3" class="form-control"  id="projected_travel3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_travel3" class="form-control"  id="actual_travel3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_travel3" class="form-control"  id="comment_travel3" type="text" />
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="number_of_items_travel" name="number_of_items_travel" value="3" />
                        <button   type="button"  class="btn-info" id="add_row_travel">+</button>
                        <button   type="button"  class="btn-danger" id="remove_row_travel">-</button>
                        <button   type="submit"  class="btn-success" id="save_travel">Save Changes</button>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <button class="accordion"><center>Public Relations</center></button>
    <div class="panel">
        <div class="row">
            <div class="col-md-12">
                <div class="demo-box">

                    <div class="table-responsive">
                        <table class="table m-0 table-colored table-info" id="table_publicrelations">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>category</th>
                                <th>projected subtotal</th>
                                <th>actual subtotal</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody id="data_publicrelations">
                            <tr>
                                <th scope="row">1</th>
                                <td>
                                    <div class="form-group">
                                        <input name="categorypr1" class="form-control"  id="categorypr1" type="text" value="Announcements" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projectedpr1" class="form-control"  id="projectedpr1" type="text"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actualpr1" class="form-control"  id="actualpr1" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="commentpr1" class="form-control"  id="commentpr1" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>
                                    <div class="form-group">
                                        <input name="categorypr2" class="form-control"  id="categorypr2" type="text" value="Graphics" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projectedpr2" class="form-control"  id="projectedpr2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actualpr2" class="form-control"  id="actualpr2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="commentpr2" class="form-control"  id="commentpr2" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>
                                    <div class="form-group">
                                        <input name="categorypr3" class="form-control"  id="categorypr3" type="text" value="Press releases" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projectedpr3" class="form-control"  id="projectedpr3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actualpr3" class="form-control"  id="actualpr3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="commentpr3" class="form-control"  id="commentpr3" type="text" />
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="number_of_items_publicrelations" name="number_of_items_publicrelations" value="3" />
                        <button   type="button"  class="btn-info" id="add_row_publicrelations">+</button>
                        <button   type="button"  class="btn-danger" id="remove_row_publicrelations">-</button>
                        <button   type="submit"  class="btn-success" id="save_publicrelations">Save Changes</button>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <button class="accordion"><center>Decor</center></button>
    <div class="panel">
        <div class="row">
            <div class="col-md-12">
                <div class="demo-box">

                    <div class="table-responsive">
                        <table class="table m-0 table-colored table-info" id="table_decor">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>category</th>
                                <th>projected subtotal</th>
                                <th>actual subtotal</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody id="data_decor">
                            <tr>
                                <th scope="row">1</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_decor1" class="form-control"  id="category_decor1" type="text" value="Linens" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_decor1" class="form-control"  id="projected_decor1" type="text"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_decor1" class="form-control"  id="actual_decor1" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_decor1" class="form-control"  id="comment_decor1" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_decor2" class="form-control"  id="category_decor2" type="text" value="Lighting" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_decor2" class="form-control"  id="projected_decor2" type="text"  />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_decor2" class="form-control"  id="actual_decor2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_decor2" class="form-control"  id="comment_decor2" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_decor3" class="form-control"  id="category_decor3" type="text" value="Additional Sinage" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_decor3" class="form-control"  id="projected_decor3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_decor3" class="form-control"  id="actual_decor3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_decor3" class="form-control"  id="comment_decor3" type="text" />
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="number_of_items_decor" name="number_of_items_decor" value="3" />
                        <button   type="button"  class="btn-info" id="add_row_decor">+</button>
                        <button   type="button"  class="btn-danger" id="remove_row_decor">-</button>
                        <button   type="submit"  class="btn-success" id="save_decor">Save Changes</button>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <button class="accordion"><center>Event programming</center></button>
    <div class="panel">
        <div class="row">
            <div class="col-md-12">
                <div class="demo-box">

                    <div class="table-responsive">
                        <table class="table m-0 table-colored table-info" id="table_eventp">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>category</th>
                                <th>projected subtotal</th>
                                <th>actual subtotal</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody id="data_eventp">
                            <tr>
                                <th scope="row">1</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_eventp1" class="form-control"  id="category_eventp1" type="text" value="Speakers" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_eventp1" class="form-control"  id="projected_eventp1" type="text"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_eventp1" class="form-control"  id="actual_eventp1" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_eventp1" class="form-control"  id="comment_eventp1" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_eventp2" class="form-control"  id="category_eventp2" type="text" value="Performers" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_eventp2" class="form-control"  id="projected_eventp2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_eventp2" class="form-control"  id="actual_eventp2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_eventp2" class="form-control"  id="comment_eventp2" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_eventp3" class="form-control"  id="category_eventp3" type="text" value="Video Production" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_eventp3" class="form-control"  id="projected_eventp3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_eventp3" class="form-control"  id="actual_eventp3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_eventp3" class="form-control"  id="comment_eventp3" type="text" />
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="number_of_items_eventp" name="number_of_items_eventp" value="3" />
                        <button   type="button"  class="btn-info" id="add_row_eventp">+</button>
                        <button   type="button"  class="btn-danger" id="remove_row_eventp">-</button>
                        <button   type="submit"  class="btn-success" id="save_eventp">Save Changes</button>
                    </div>
                </div>

            </div>

        </div>
    </div>


    <button class="accordion"><center>Social Media</center></button>
    <div class="panel">
        <div class="row">
            <div class="col-md-12">
                <div class="demo-box">

                    <div class="table-responsive">
                        <table class="table m-0 table-colored table-info" id="table_socialmedia">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>category</th>
                                <th>projected subtotal</th>
                                <th>actual subtotal</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody id="data_socialmedia">
                            <tr>
                                <th scope="row">1</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_sm1" class="form-control"  id="category_sm1" type="text" value="Facebook" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_sm1" class="form-control"  id="projected_sm1" type="text"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_sm1" class="form-control"  id="actual_sm1" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_sm1" class="form-control"  id="comment_sm1" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_sm2" class="form-control"  id="category_sm2" type="text" value="Snapchat" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_sm2" class="form-control"  id="projected_sm2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_sm2" class="form-control"  id="actual_sm2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_sm2" class="form-control"  id="comment_sm2" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_sm3" class="form-control"  id="category_sm3" type="text" value="Google+" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_sm3" class="form-control"  id="projected_sm3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_sm3" class="form-control"  id="actual_sm3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_sm3" class="form-control"  id="comment_sm3" type="text" />
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="number_of_items_socialmedia" name="number_of_items_socialmedia" value="3" />
                        <button   type="button"  class="btn-info" id="add_row_socialmedia">+</button>
                        <button   type="button"  class="btn-danger" id="remove_row_socialmedia">-</button>
                        <button   type="submit"  class="btn-success" id="save_socialmedia">Save Changes</button>
                    </div>
                </div>

            </div>

        </div>
    </div>


    <button class="accordion"><center>Refreshment</center></button>
    <div class="panel">
        <div class="row">
            <div class="col-md-12">
                <div class="demo-box">

                    <div class="table-responsive">
                        <table class="table m-0 table-colored table-info" id="table_refreshment">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>category</th>
                                <th>projected subtotal</th>
                                <th>actual subtotal</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody id="data_refreshment">
                            <tr>
                                <th scope="row">1</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_refreshment1" class="form-control"  id="category_refreshment1" type="text" value="Drinks" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_refreshment1" class="form-control"  id="projected_refreshment1" type="text"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_refreshment1" class="form-control"  id="actual_refreshment1" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_refreshment1" class="form-control"  id="comment_refreshment1" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_refreshment2" class="form-control"  id="category_refreshment2" type="text" value="Food" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_refreshment2" class="form-control"  id="projected_refreshment2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_refreshment2" class="form-control"  id="actual_refreshment2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_refreshment2" class="form-control"  id="comment_refreshment2" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_refreshment3" class="form-control"  id="category_refreshment3" type="text" value="Catering Staff" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_refreshment3" class="form-control"  id="projected_refreshment3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_refreshment3" class="form-control"  id="actual_refreshment3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_refreshment3" class="form-control"  id="comment_refreshment3" type="text" />
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="number_of_items_refreshment" name="number_of_items_refreshment" value="3" />
                        <button   type="button"  class="btn-info" id="add_row_refreshment">+</button>
                        <button   type="button"  class="btn-danger" id="remove_row_refreshment">-</button>
                        <button   type="submit"  class="btn-success" id="save_refreshment">Save Changes</button>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <button class="accordion"><center>Others</center></button>
     <div class="panel">
        <div class="row">
            <div class="col-md-12">
                <div class="demo-box">

                    <div class="table-responsive">
                        <table class="table m-0 table-colored table-info" id="table_others">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>category</th>
                                <th>projected subtotal</th>
                                <th>actual subtotal</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody id="data_others">
                            <tr>
                                <th scope="row">1</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_others1" class="form-control"  id="category_others1" type="text" value="Gift bags" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_others1" class="form-control"  id="projected_others1" type="text"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_others1" class="form-control"  id="actual_others1" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_others1" class="form-control"  id="comment_others1" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_others2" class="form-control"  id="category_others2" type="text" value="Pens" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_others2" class="form-control"  id="projected_others2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_others2" class="form-control"  id="actual_others2" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_others2" class="form-control"  id="comment_others2" type="text" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>
                                    <div class="form-group">
                                        <input name="category_others3" class="form-control"  id="category_others3" type="text" value="Note books" disabled/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="projected_others3" class="form-control"  id="projected_others3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="actual_others3" class="form-control"  id="actual_others3" type="text" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="comment_others3" class="form-control"  id="comment_others3" type="text" />
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" id="number_of_items_others" name="number_of_items_others" value="3" />
                        <button   type="button"  class="btn-info" id="add_row_others">+</button>
                        <button   type="button"  class="btn-danger" id="remove_row_others">-</button>
                        <button   type="submit"  class="btn-success" id="save_others"  onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 4000);">Save Changes</button>
                    </div>
                </div>

            </div>

        </div>
    </div>

    </br>
    <div class="row">
        <div class="col-lg-4">

        </div>
        <div class="col-lg-4">

        </div>
        <div class="col-lg-4">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-info" id="downloadbudget">preview budget</a>
        </div>

    </div>
</div>

                    <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="http://coderthemes.com/zircos_1.6/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
    <script src="http://coderthemes.com/zircos_1.6/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="http://coderthemes.com/zircos_1.6/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

    <script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;

    if (panel.style.display === "block") {
    panel.style.display = "none";
    } else {
    panel.style.display = "block";
    }
    }
    }
    </script>
    <script>
        $(document).ready(function() {

            var currentItem = 4;
            $('#add_row_budget').click(function(){
                currentItem =currentItem+1;
                $('#number_of_items_venue').val(currentItem);
                var strToAdd='<tr> <th scope="row">'+currentItem+'</th> <td> <div class="form-group"> <input name="category_venue'+currentItem+'" class="form-control"  id="category_venue'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="projected_venue'+currentItem+'" class="form-control"  id="projected_venue'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="actual_venue'+currentItem+'" class="form-control"  id="actual_venue'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="comment_venue'+currentItem+'" class="form-control"  id="comment_venue'+currentItem+'" type="text" /> </div> </td> </tr>';
                $('#data_budget').append(strToAdd);

            });
            $('#remove_row_budget').click(function(){
                var rowCount = $('#table_venue tr').length;
                if(rowCount >5){
                    currentItem =currentItem-1;
                    $('#table_venue tr:last').remove();
                }
            });
        })
        $(document).ready(function() {
            var currentItem = 3;
            $('#add_row_travel').click(function(){

                currentItem =currentItem+1;
                $('#number_of_items_travel').val(currentItem);
                var strToAdd='<tr> <th scope="row">'+currentItem+'</th> <td> <div class="form-group"> <input name="category_travel'+currentItem+'" class="form-control"  id="category_travel'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="projected_travel'+currentItem+'" class="form-control"  id="projected_travel'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="actual_travel'+currentItem+'" class="form-control"  id="actual_travel'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="comment_travel'+currentItem+'" class="form-control"  id="comment_travel'+currentItem+'" type="text" /> </div> </td> </tr>';
                $('#data_travel').append(strToAdd);

            });
            $('#remove_row_travel').click(function(){
                var rowCount = $('#table_travel tr').length;
                if(rowCount >4){
                    currentItem =currentItem-1;
                    $('#table_travel tr:last').remove();
                }
            });
        })
        $(document).ready(function() {
            var currentItem = 3;
            $('#add_row_publicrelations').click(function(){

                currentItem =currentItem+1;
                $('#number_of_items_publicrelations').val(currentItem);
                var strToAdd='<tr> <th scope="row">'+currentItem+'</th> <td> <div class="form-group"> <input name="category'+currentItem+'" class="form-control"  id="category'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="projected'+currentItem+'" class="form-control"  id="projected'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="actual'+currentItem+'" class="form-control"  id="actual'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="comment'+currentItem+'" class="form-control"  id="comment'+currentItem+'" type="text" /> </div> </td> </tr>';
                $('#data_publicrelations').append(strToAdd);

            });
            $('#remove_row_publicrelations').click(function(){
                var rowCount = $('#table_publicrelations tr').length;
                if(rowCount >4){
                    currentItem =currentItem-1;
                    $('#table_publicrelations tr:last').remove();
                }
            });
        })
        $(document).ready(function() {
            var currentItem = 3;
            $('#add_row_decor').click(function(){

                currentItem =currentItem+1;
                $('#number_of_items_decor').val(currentItem);
                var strToAdd='<tr> <th scope="row">'+currentItem+'</th> <td> <div class="form-group"> <input name="category_decor'+currentItem+'" class="form-control"  id="category_decor'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="projected_decor'+currentItem+'" class="form-control"  id="projected_decor'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="actual_decor'+currentItem+'" class="form-control"  id="actual_decor'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="comment_decor'+currentItem+'" class="form-control"  id="comment_decor'+currentItem+'" type="text" /> </div> </td> </tr>';
                $('#data_decor').append(strToAdd);

            });
            $('#remove_row_decor').click(function(){
                var rowCount = $('#table_decor tr').length;
                if(rowCount >4){
                    currentItem =currentItem-1;
                    $('#table_decor tr:last').remove();
                }
            });
        })
        $(document).ready(function() {
            var currentItem = 3;
            $('#add_row_eventp').click(function(){

                currentItem =currentItem+1;
                $('#number_of_items_eventp').val(currentItem);
                var strToAdd='<tr> <th scope="row">'+currentItem+'</th> <td> <div class="form-group"> <input name="category_eventp'+currentItem+'" class="form-control"  id="category_eventp'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="projected_eventp'+currentItem+'" class="form-control"  id="projected_eventp'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="actual_eventp'+currentItem+'" class="form-control"  id="actual_eventp'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="comment_eventp'+currentItem+'" class="form-control"  id="comment-eventp'+currentItem+'" type="text" /> </div> </td> </tr>';
                $('#data_eventp').append(strToAdd);

            });
            $('#remove_row_eventp').click(function(){
                var rowCount = $('#table_eventp tr').length;
                if(rowCount >4){
                    currentItem =currentItem-1;
                    $('#table_eventp tr:last').remove();
                }
            });
        })
        $(document).ready(function() {
            var currentItem = 3;
            $('#add_row_socialmedia').click(function(){

                currentItem =currentItem+1;
                $('#number_of_items_socialmedia').val(currentItem);
                var strToAdd='<tr> <th scope="row">'+currentItem+'</th> <td> <div class="form-group"> <input name="category_sm'+currentItem+'" class="form-control"  id="category_sm'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="projected_sm'+currentItem+'" class="form-control"  id="projected_sm'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="actual_sm'+currentItem+'" class="form-control"  id="actual_sm'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="comment_sm'+currentItem+'" class="form-control"  id="comment_sm'+currentItem+'" type="text" /> </div> </td> </tr>';
                $('#data_socialmedia').append(strToAdd);

            });
            $('#remove_row_socialmedia').click(function(){
                var rowCount = $('#table_socialmedia tr').length;
                if(rowCount >4){
                    currentItem =currentItem-1;
                    $('#table_socialmedia tr:last').remove();
                }
            });
        })
        $(document).ready(function() {
            var currentItem = 3;
            $('#add_row_refreshment').click(function(){

                currentItem =currentItem+1;
                $('#number_of_items_refreshment').val(currentItem);
                var strToAdd='<tr> <th scope="row">'+currentItem+'</th> <td> <div class="form-group"> <input name="category_refreshment'+currentItem+'" class="form-control"  id="category_refreshment'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="projected_refreshment'+currentItem+'" class="form-control"  id="projected_refreshment'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="actual_refreshment'+currentItem+'" class="form-control"  id="actual_refreshment'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="comment_refreshment'+currentItem+'" class="form-control"  id="comment_refreshment'+currentItem+'" type="text" /> </div> </td> </tr>';
                $('#data_refreshment').append(strToAdd);

            });
            $('#remove_row_refreshment').click(function(){
                var rowCount = $('#table_refreshment tr').length;
                if(rowCount >4){
                    currentItem =currentItem-1;
                    $('#table_refreshment tr:last').remove();
                }
            });
        })
        $(document).ready(function() {
            var currentItem = 3;
            $('#add_row_others').click(function(){

                currentItem =currentItem+1;
                $('#number_of_items_others').val(currentItem);
                var strToAdd='<tr> <th scope="row">'+currentItem+'</th> <td> <div class="form-group"> <input name="category_others'+currentItem+'" class="form-control"  id="category_others'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="projected_others'+currentItem+'" class="form-control"  id="projected_others'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="actual_others'+currentItem+'" class="form-control"  id="actual_others'+currentItem+'" type="text" /> </div> </td> <td> <div class="form-group"> <input name="comment_others'+currentItem+'" class="form-control"  id="comment_others'+currentItem+'" type="text" /> </div> </td> </tr>';
                $('#data_others').append(strToAdd);

            });
            $('#remove_row_others').click(function(){
                var rowCount = $('#table_others tr').length;
                if(rowCount >4){
                    currentItem =currentItem-1;
                    $('#table_others tr:last').remove();
                }
            });
        })


        /////saving
        $(document).ready(function(){
            $('#save_venue').click(function(e){
                e.preventDefault();
                var eventid=$('select[name=selectpicker]').val() ;
                var budget_type='venue';

                /////////////////////////////////
                $.ajax({
                    type:'get' ,
                    url: '{{URL::to('event/update_event_budget')}}',
                    data:{
                        '_token':$('input[name=_token]').val(),
                        'eventid':eventid,
                        'budget_type':budget_type

                    },
                    success:function(data){
                        if(data=='removed'){
                            var number_of_items_venue = $('#number_of_items_venue').val();
                            for (i = 1; i <=number_of_items_venue; i++) {
                                var category_venue = $("#category_venue" + i).val();
                                var projected_venue = $("#projected_venue" + i).val();
                                var actual_venue = $("#actual_venue" + i).val();
                                var comment_venue = $("#comment_venue" + i).val();
                                var type='venue';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_venue':category_venue,
                                        'projected_venue':projected_venue,
                                        'actual_venue':actual_venue,
                                        'comment_venue':comment_venue,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }else{
                            var number_of_items_venue = $('#number_of_items_venue').val();
                            for (i = 1; i <=number_of_items_venue; i++) {
                                var category_venue = $("#category_venue" + i).val();
                                var projected_venue = $("#projected_venue" + i).val();
                                var actual_venue = $("#actual_venue" + i).val();
                                var comment_venue = $("#comment_venue" + i).val();
                                var type='venue';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_venue':category_venue,
                                        'projected_venue':projected_venue,
                                        'actual_venue':actual_venue,
                                        'comment_venue':comment_venue,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }

                    }
                });
                ////////////////////////////////


                });
            $('#save_travel').click(function(e){
                e.preventDefault();
                var eventid=$('select[name=selectpicker]').val() ;
                var budget_type='travel';

                /////////////////////////////////
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('event/update_event_budget')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'eventid': eventid,
                        'budget_type': budget_type

                    },
                    success: function (data) {
                        if(data=='removed'){
                            var number_of_items_travel = $('#number_of_items_travel').val();
                            for (i = 1; i <=number_of_items_travel; i++) {
                                var category_travel = $("#category_travel" + i).val();
                                var projected_travel = $("#projected_travel" + i).val();
                                var actual_travel = $("#actual_travel" + i).val();
                                var comment_travel = $("#comment_travel" + i).val();
                                var type='travel';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_travel':category_travel,
                                        'projected_travel':projected_travel,
                                        'actual_travel':actual_travel,
                                        'comment_travel':comment_travel,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }else{
                            var number_of_items_travel = $('#number_of_items_travel').val();
                            for (i = 1; i <=number_of_items_travel; i++) {
                                var category_travel = $("#category_travel" + i).val();
                                var projected_travel = $("#projected_travel" + i).val();
                                var actual_travel = $("#actual_travel" + i).val();
                                var comment_travel = $("#comment_travel" + i).val();
                                var type='travel';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_travel':category_travel,
                                        'projected_travel':projected_travel,
                                        'actual_travel':actual_travel,
                                        'comment_travel':comment_travel,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                    }

                });
                //////////////////////////////////////////


            });
            $('#save_publicrelations').click(function(e){
                e.preventDefault();
                var eventid=$('select[name=selectpicker]').val() ;
                var budget_type='publicrelations';

                /////////////////////////////////
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('event/update_event_budget')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'eventid': eventid,
                        'budget_type': budget_type

                    },
                    success: function (data) {
                        if(data=='removed'){
                            var number_of_items_publicrelations = $('#number_of_items_publicrelations').val();
                            for (i = 1; i <=number_of_items_publicrelations; i++) {
                                var categorypr = $("#categorypr" + i).val();
                                var projectedpr = $("#projectedpr" + i).val();
                                var actualpr = $("#actualpr" + i).val();
                                var commentpr = $("#commentpr" + i).val();
                                var type='publicrelations';
                                var eventid=$('select[name=selectpicker]').val() ;

                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_publicrelations':categorypr,
                                        'projected_publicrelations':projectedpr,
                                        'actual_publicrelations':actualpr,
                                        'comment_publicrelations':commentpr,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                        else{
                            var number_of_items_publicrelations = $('#number_of_items_publicrelations').val();
                            for (i = 1; i <=number_of_items_publicrelations; i++) {
                                var categorypr = $("#categorypr" + i).val();
                                var projectedpr = $("#projectedpr" + i).val();
                                var actualpr = $("#actualpr" + i).val();
                                var commentpr = $("#commentpr" + i).val();
                                var type='publicrelations';
                                var eventid=$('select[name=selectpicker]').val() ;

                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_publicrelations':categorypr,
                                        'projected_publicrelations':projectedpr,
                                        'actual_publicrelations':actualpr,
                                        'comment_publicrelations':commentpr,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                    }
                });
                ////////////////////////////////////////


            });
            $('#save_decor').click(function(e){
                e.preventDefault();
                var eventid=$('select[name=selectpicker]').val() ;
                var budget_type='decor';

                /////////////////////////////////
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('event/update_event_budget')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'eventid': eventid,
                        'budget_type': budget_type

                    },
                    success: function (data) {
                        if(data=='removed'){
                            var number_of_items_decor = $('#number_of_items_decor').val();
                            for (i = 1; i <=number_of_items_decor; i++) {
                                var category_decor= $("#category_decor" + i).val();
                                var projected_decor = $("#projected_decor" + i).val();
                                var actual_decor = $("#actual_decor" + i).val();
                                var comment_decor = $("#comment_decor" + i).val();
                                var type='decor';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_decor':category_decor,
                                        'projected_decor':projected_decor,
                                        'actual_decor':actual_decor,
                                        'comment_decor':comment_decor,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }else{
                            var number_of_items_decor = $('#number_of_items_decor').val();
                            for (i = 1; i <=number_of_items_decor; i++) {
                                var category_decor= $("#category_decor" + i).val();
                                var projected_decor = $("#projected_decor" + i).val();
                                var actual_decor = $("#actual_decor" + i).val();
                                var comment_decor = $("#comment_decor" + i).val();
                                var type='decor';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_decor':category_decor,
                                        'projected_decor':projected_decor,
                                        'actual_decor':actual_decor,
                                        'comment_decor':comment_decor,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                    }

                });

                ////////////////////////////


            });
            $('#save_eventp').click(function(e){
                e.preventDefault();
                var eventid=$('select[name=selectpicker]').val() ;
                var budget_type='eventp';

                /////////////////////////////////
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('event/update_event_budget')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'eventid': eventid,
                        'budget_type': budget_type

                    },
                    success: function (data) {
                        if(data=='removed'){
                            var number_of_items_eventp = $('#number_of_items_eventp').val();
                            for (i = 1; i <=number_of_items_eventp; i++) {
                                var category_eventp= $("#category_eventp" + i).val();
                                var projected_eventp = $("#projected_eventp" + i).val();
                                var actual_eventp = $("#actual_eventp" + i).val();
                                var comment_eventp = $("#comment_eventp" + i).val();
                                var type='eventp';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_eventp':category_eventp,
                                        'projected_eventp':projected_eventp,
                                        'actual_eventp':actual_eventp,
                                        'comment_eventp':comment_eventp,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }else{
                            var number_of_items_eventp = $('#number_of_items_eventp').val();
                            for (i = 1; i <=number_of_items_eventp; i++) {
                                var category_eventp= $("#category_eventp" + i).val();
                                var projected_eventp = $("#projected_eventp" + i).val();
                                var actual_eventp = $("#actual_eventp" + i).val();
                                var comment_eventp = $("#comment_eventp" + i).val();
                                var type='eventp';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_eventp':category_eventp,
                                        'projected_eventp':projected_eventp,
                                        'actual_eventp':actual_eventp,
                                        'comment_eventp':comment_eventp,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                    }
                });
                /////////////////////////////////


            });
            $('#save_socialmedia').click(function(e){
                e.preventDefault();
                var eventid=$('select[name=selectpicker]').val() ;
                var budget_type='socialmedia';

                /////////////////////////////////
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('event/update_event_budget')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'eventid': eventid,
                        'budget_type': budget_type

                    },
                    success: function (data) {
                        if(data=='removed'){

                            ///////
                            var number_of_items_socialmedia = $('#number_of_items_socialmedia').val();
                            for (i = 1; i <=number_of_items_socialmedia; i++) {
                                var category_sm = $("#category_sm" + i).val();
                                var projected_sm = $("#projected_sm" + i).val();
                                var actual_sm = $("#actual_sm" + i).val();
                                var comment_sm = $("#comment_sm" + i).val();
                                var type='socialmedia';
                                var eventid=$('select[name=selectpicker]').val() ;

                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_sm':category_sm,
                                        'projected_sm':projected_sm,
                                        'actual_sm':actual_sm,
                                        'comment_sm':comment_sm,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }else{

                            ///////
                            var number_of_items_socialmedia = $('#number_of_items_socialmedia').val();
                            for (i = 1; i <=number_of_items_socialmedia; i++) {
                                var category_sm = $("#category_sm" + i).val();
                                var projected_sm = $("#projected_sm" + i).val();
                                var actual_sm = $("#actual_sm" + i).val();
                                var comment_sm = $("#comment_sm" + i).val();
                                var type='socialmedia';
                                var eventid=$('select[name=selectpicker]').val() ;

                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_sm':category_sm,
                                        'projected_sm':projected_sm,
                                        'actual_sm':actual_sm,
                                        'comment_sm':comment_sm,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                    }
                });





            });
            $('#save_refreshment').click(function(e){
                e.preventDefault();
                var eventid=$('select[name=selectpicker]').val() ;
                var budget_type='refreshment';

                /////////////////////////////////
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('event/update_event_budget')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'eventid': eventid,
                        'budget_type': budget_type

                    },
                    success: function (data) {
                        if(data=='removed'){
                            var number_of_items_refreshment = $('#number_of_items_refreshment').val();
                            for (i = 1; i <=number_of_items_refreshment; i++) {
                                var category_refreshment = $("#category_refreshment" + i).val();
                                var projected_refreshment = $("#projected_refreshment" + i).val();
                                var actual_refreshment = $("#actual_refreshment" + i).val();
                                var comment_refreshment = $("#comment_refreshment" + i).val();
                                var type='refreshment';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_refreshment':category_refreshment,
                                        'projected_refreshment':projected_refreshment,
                                        'actual_refreshment':actual_refreshment,
                                        'comment_refreshment':comment_refreshment,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                        else{
                            var number_of_items_refreshment = $('#number_of_items_refreshment').val();
                            for (i = 1; i <=number_of_items_refreshment; i++) {
                                var category_refreshment = $("#category_refreshment" + i).val();
                                var projected_refreshment = $("#projected_refreshment" + i).val();
                                var actual_refreshment = $("#actual_refreshment" + i).val();
                                var comment_refreshment = $("#comment_refreshment" + i).val();
                                var type='refreshment';
                                var eventid=$('select[name=selectpicker]').val() ;
                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_refreshment':category_refreshment,
                                        'projected_refreshment':projected_refreshment,
                                        'actual_refreshment':actual_refreshment,
                                        'comment_refreshment':comment_refreshment,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();
                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                    }
                });


            });
            $('#save_others').click(function(e){

                e.preventDefault();



                var eventid=$('select[name=selectpicker]').val() ;
                var budget_type='others';

                /////////////////////////////////
                $.ajax({
                    type: 'get',
                    url: '{{URL::to('event/update_event_budget')}}',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'eventid': eventid,
                        'budget_type': budget_type

                    },
                    success: function (data) {
                        if(data=='removed'){
                            var number_of_items_others = $('#number_of_items_others').val();
                            for (i = 1; i <=number_of_items_others; i++) {

                                var category_others = $("#category_others" + i).val();
                                var projected_others = $("#projected_others" + i).val();
                                var actual_others = $("#actual_others" + i).val();
                                var comment_others = $("#comment_others" + i).val();
                                var type='others';
                                var eventid=$('select[name=selectpicker]').val() ;

                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_others':category_others,
                                        'projected_others':projected_others,
                                        'actual_others':actual_others,
                                        'comment_others':comment_others,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();

                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }else{
                            var number_of_items_others = $('#number_of_items_others').val();
                            for (i = 1; i <=number_of_items_others; i++) {

                                var category_others = $("#category_others" + i).val();
                                var projected_others = $("#projected_others" + i).val();
                                var actual_others = $("#actual_others" + i).val();
                                var comment_others = $("#comment_others" + i).val();
                                var type='others';
                                var eventid=$('select[name=selectpicker]').val() ;

                                $.ajax({
                                    type:'post' ,
                                    url: '{{URL::to('event_budget')}}',
                                    data:{
                                        '_token':$('input[name=_token]').val(),
                                        'eventid':eventid,
                                        'category_others':category_others,
                                        'projected_others':projected_others,
                                        'actual_others':actual_others,
                                        'comment_others':comment_others,
                                        'type':type

                                    },
                                    beforeSend: function(){
                                        waitingDialog.show();

                                    },
                                    complete: function(){
                                        waitingDialog.hide();
                                    },
                                    success:function(data){
                                        if(data=="success"){
                                            toastr.success('Budgets succesfully added','Success',{timeOut: 1000});

                                        }
                                        else if(data=="fail"){
                                            toastr.error('An Error Occured','Task',{timeOut: 1000});
                                        }


                                    }
                                });
                            }
                        }
                    }
                });


            });
        })
    </script>
    <script>
        /**
         * Module for displaying "Waiting for..." dialog using Bootstrap
         *
         * @author Eugene Maslovich <ehpc@em42.ru>
         */

        var waitingDialog = waitingDialog || (function ($) {
                    'use strict';

                    // Creating modal dialog's DOM
                    var $dialog = $(
                            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
                            '<div class="modal-dialog modal-m">' +
                            '<div class="modal-content">' +
                            '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
                            '<div class="modal-body">' +
                            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
                            '</div>' +
                            '</div></div></div>');

                    return {
                        /**
                         * Opens our dialog
                         * @param message Custom message
                         * @param options Custom options:
                         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
                         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
                         */
                        show: function (message, options) {
                            // Assigning defaults
                            if (typeof options === 'undefined') {
                                options = {};
                            }
                            if (typeof message === 'undefined') {
                                message = 'Saving.....';
                            }
                            var settings = $.extend({
                                dialogSize: 'm',
                                progressType: '',
                                onHide: null // This callback runs after the dialog was hidden
                            }, options);

                            // Configuring dialog
                            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
                            $dialog.find('.progress-bar').attr('class', 'progress-bar');
                            if (settings.progressType) {
                                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
                            }
                            $dialog.find('h3').text(message);
                            // Adding callbacks
                            if (typeof settings.onHide === 'function') {
                                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                                    settings.onHide.call($dialog);
                                });
                            }
                            // Opening dialog
                            $dialog.modal();
                        },
                        /**
                         * Closes dialog
                         */
                        hide: function () {
                            $dialog.modal('hide');
                        }
                    };

                })(jQuery);

    </script>
    <script>
        $('.selectpicker').on('change', function() {

            var eventid = $('select[name=selectpicker]').val();

            $('#eventid').val(eventid);
        });
        $('#downloadbudget').on('click', function() {

            var event_id=$('#eventid').val();

               if(event_id==''){
                   var event_id= $(".selectpicker option:selected").val();
                   var url="eventbudgetpdf"+"/"+event_id;
                   window.location.href = url;
               }else{
                   var url="eventbudgetpdf"+"/"+event_id;

                   window.location.href = url;
               }



        });
    </script>
@endsection


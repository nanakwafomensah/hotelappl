
@extends('layout.local.localmaster')

@section('content')
    <div id="wrapper" xmlns="http://www.w3.org/1999/html">
        <!-- Modal -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title-box">
                                <h4 class="page-title">View Invoices </h4>
                                <ol class="breadcrumb p-0 m-0">
                                    <li>
                                        <a href="{{route('dashboard')}}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#">Settings </a>
                                    </li>
                                    <li class="active">
                                      View Invoice
                                    </li>
                                </ol>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box table-responsive">

                                        <div class="panel panel-default">
                                            <!-- Default panel contents -->
                                            <div class="panel-heading">Invoice {{$invoicenumber}}</div>

                                            <div class="panel-body">

                                                <a href="{{URL::to("pdfinvoice/".$customerid . "/" . "$invoicenumber")}}" type="button" class="btn btn-info btn-round">Export to pdf</a>
                                                <a href="{{URL::to("pdfinvoice/".$customerid . "/" . "$invoicenumber")}}" type="button" class="btn btn-info btn-round">Export to csv</a>
                                                <a href="{{URL::to("pdfinvoice/".$customerid . "/" . "$invoicenumber")}}" type="button" class="btn btn-info btn-round">Export to .doc</a>
                                                <a href="{{URL::to("pdfinvoice/".$customerid . "/" . "$invoicenumber")}}" type="button" class="btn btn-info btn-round">Email</a>
                                                &nbsp;</br></br></br></br>

                                                <div class="row">

                                                    <div class="row">
                                                        <div class="col-lg-3"><strong> Invoice No.</strong></div>
                                                        <div class="col-lg-3">{{$invoicenumber}}</div>
                                                        <div class="col-lg-6"><strong> Invoice date :</strong><span>{{$invoicedate}}</span></div>
                                                    </div>
                                                    <hr />
                                                    <div class="row">
                                                        <div class="col-lg-3"><strong>Customer ID </strong></div>
                                                        <div class="col-lg-5">{{$customerid}}</div>
                                                        <div class="col-lg-4"></div>
                                                    </div>
                                                    <hr  />
                                                    <div class="row">
                                                        <div class="col-lg-3"><strong>Customer Name </strong></div>
                                                        <div class="col-lg-5">{{$customername}}</div>
                                                        <div class="col-lg-4"></div>
                                                    </div>

                                                </div>
                                                <hr  />
                                                <!-- Table -->
                                                <div class="row">
                                                    <table class="table table-hover" style="border-radius: 5px;width: 50%;margin: 0px auto;float: none;">
                                                        <thead>
                                                        <tr>
                                                            <th> No.</th>
                                                            <th>Description</th>
                                                            <th>Quantity</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $n=1;$totalamount=0;?>
                                                        @foreach($items_oninvoice as $i)
                                                            <?php $totalamount=$totalamount+$i->amount?>
                                                            <tr>
                                                                <td>{{$n++}}</td>
                                                                <td>{{$i->description}}</td>
                                                                <td>{{$i->quantity}}</td>
                                                                <td>GHC {{number_format($i->amount)}}</td>
                                                            </tr>
                                                        @endforeach
                                                        <tfoot>
                                                        <tr><td></td>
                                                            <td></td>
                                                            <td>Total Bill:</td>
                                                            <td>GHC {{number_format($totalamount)}}</td>
                                                        </tr>
                                                        </tfoot>


                                                    </table>

                                                    </row>

                                                </div>
                                                <hr style="width: 100%; color: black; height: 1px; background-color:black;" />

                                                <div class="panel panel-default" style="border-radius: 5px;width: 50%;margin: 0px auto;float: none;">
                                                    <div class="panel-heading"><strong>FINANCIAL STATUS</strong></div>
                                                    <div class="panel-body">
                                                        <div class="col-lg-4"><strong>Owing :</strong><span>GHC {{number_format($owing)}}</span></div>
                                                        <div class="col-lg-4"><strong>Excess :</strong><span>GHC {{number_format($excess)}}</span></div>
                                                        <div class="col-lg-4"><strong>Amount Paid :</strong><span>GHC {{number_format($payment)}}</span></div>

                                                    </div>
                                                </div>







                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </div> <!-- container -->
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box table-responsive">

                                        <div class="panel panel-default">
                                            <!-- Default panel contents -->
                                            <div class="panel-heading">All Payments for  invoice {{$invoicenumber}}</div>

                                            <div class="panel-body">

                                                <a href="{{URL::to("pdfinvoice/".$customerid . "/" . "$invoicenumber")}}" type="button" class="btn btn-info btn-round">Export to pdf</a>
                                                <a href="{{URL::to("pdfinvoice/".$customerid . "/" . "$invoicenumber")}}" type="button" class="btn btn-info btn-round">Export to csv</a>
                                                <a href="{{URL::to("pdfinvoice/".$customerid . "/" . "$invoicenumber")}}" type="button" class="btn btn-info btn-round">Export to .doc</a>
                                                <a href="{{URL::to("pdfinvoice/".$customerid . "/" . "$invoicenumber")}}" type="button" class="btn btn-info btn-round">Email</a>
                                                &nbsp;</br></br></br></br>


                                                <hr  />
                                                <!-- Table -->
                                                <div class="row">
                                                    <table class="table table-hover" >
                                                        <thead>
                                                        <tr>
                                                            <th> No.</th>
                                                            <th>Category</th>
                                                            <th>Receipt Number</th>
                                                            <th>Payment Date</th>
                                                            <th>Remark</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $n=1;$totalamount=0;?>
                                                        @foreach($paymentdetails as $i)
                                                            <?php $totalamount=$totalamount+$i->amount?>
                                                            <tr>
                                                                <td>{{$n++}}</td>
                                                                <td>{{$i->category}}</td>
                                                                <td>{{$i->receiptnumber}}</td>
                                                                <td>{{$i->paymentdate}}</td>
                                                                <td>{{$i->remark}}</td>
                                                                <td>GHC {{number_format($i->amount)}}</td>
                                                            </tr>
                                                        @endforeach
                                                        <tfoot>
                                                        <tr><td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>Total Bill:</td>
                                                            <td>GHC {{number_format($totalamount)}}</td>
                                                        </tr>
                                                        </tfoot>


                                                    </table>

                                                    </row>

                                                </div>








                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </div> <!-- container -->
                        </div>
                    </div>

            </div> <!-- content -->
            <footer class="footer text-right">
                {{date ('Y')}} © NALO Solutions Limited.
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        @include('layout.local.rightsidebar')




    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>





@endsection


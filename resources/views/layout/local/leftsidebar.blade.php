<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Navigation</li>

                <li class="has_sub">
                    <a href="{{route('dashboard')}}" class="waves-effect"><i class="fa fa-tachometer"></i><span> Dashboard </span> </a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tasks"></i> <span> To do List</span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('newtask')}}"><i class="fa fa-angle-double-right"></i>New Task</a></li>
                        {{--<li><a href="{{route('newbooking')}}"><i class="f`a fa-angle-double-right"></i>Booking</a></li>--}}
                        <li><a href="{{route('completedtask')}}"><i class="fa fa-angle-double-right"></i>Completed Task</a></li>



                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user"></i> <span> Guest</span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('newcustomer')}}"><i class="fa fa-angle-double-right"></i>New guest</a></li>
                        {{--<li><a href="{{route('newbooking')}}"><i class="fa fa-angle-double-right"></i>Booking</a></li>--}}
                        <li><a href="{{route('specialbook')}}"><i class="fa fa-angle-double-right"></i>Booking</a></li>
                        <li><a href="{{route('guestlist')}}"><i class="fa fa-angle-double-right"></i>Guest list</a></li>
                        <li><a href="{{route('archivedguest')}}"><i class="fa fa-angle-double-right"></i>Archived guest</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-calendar"></i> <span>Event</span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('newevent')}}"><i class="fa fa-angle-double-right"></i>Register an Event</a></li>
                      <li><a href="{{route('planevent')}}"><i class="fa fa-angle-double-right"></i>Plan for an  Event</a></li>
                      <li><a href="{{route('budgetevent')}}"><i class="fa fa-angle-double-right"></i>Budget for an Event</a></li>
                      <li><a href="{{route('closeevent')}}"><i class="fa fa-angle-double-right"></i>Close Event</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money"></i><span> Finance </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('billing')}}"><i class="fa fa-angle-double-right"></i> Billing</a></li>
                        <li><a href="{{route('payment')}}"><i class="fa fa-angle-double-right"></i> Payments</a></li>
                        <li><a href="{{route('debtors')}}"><i class="fa fa-angle-double-right"></i> Debtors</a></li>
                        <li><a href="{{route('excess')}}"><i class="fa fa-angle-double-right"></i> Excess</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-line-chart"></i><span> Reports </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">

                        <li><a href="admin-sweet-alert.html"><i class="fa fa-angle-double-right"></i> Summary</a></li>
                        <li><a href="admin-widgets.html"><i class="fa fa-angle-double-right"></i> Perfomamnce</a></li>
                        <li><a href="admin-nestable.html"><i class="fa fa-angle-double-right"></i> New Additions</a></li>
                        <li><a href="admin-rangeslider.html"><i class="fa fa-angle-double-right"></i> Monthly</a></li>
                        <li><a href="admin-sweet-alert.html"><i class="fa fa-angle-double-right"></i> District</a></li>
                        <li><a href="admin-widgets.html"><i class="fa fa-angle-double-right"></i> Service</a></li>
                        <li><a href="admin-nestable.html"><i class="fa fa-angle-double-right"></i> Client Collector</a></li>
                        <li><a href="admin-rangeslider.html"><i class="fa fa-angle-double-right"></i> Debtor</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-secret"></i><span> Settings </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('broadcast_message')}}"><i class="fa fa-angle-double-right"></i>BroadCast Message</a></li>
                        {{--<li><a href="admin-widgets.html"><i class="fa fa-angle-double-right"></i> Perfomamnce</a></li>--}}
                        {{--<li><a href="admin-nestable.html"><i class="fa fa-angle-double-right"></i> New Additions</a></li>--}}
                        {{--<li><a href="admin-rangeslider.html"><i class="fa fa-angle-double-right"></i> Monthly</a></li>--}}
                        {{--<li><a href="admin-sweet-alert.html"><i class="fa fa-angle-double-right"></i> District</a></li>--}}
                        <li><a href="{{route('profile')}}"><i class="fa fa-angle-double-right"></i>Profile</a></li>
                        <li><a href="{{route('room_type')}}"><i class="fa fa-angle-double-right"></i>Room</a></li>
                        <li><a href="{{route('new_user')}}"><i class="fa fa-angle-double-right"></i>New User</a></li>

                    </ul>
                </li>

</ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

        {{--<div class="help-box">--}}
            {{--<h5 class="text-muted m-t-0">For Help ?</h5>--}}
            {{--<p class=""><span class="text-custom">Email:</span> <br/> nanamensah1140@gmail.com</p>--}}
            {{--<p class="m-b-0"><span class="text-custom">Call:</span> <br/> (223)262489168</p>--}}
        {{--</div>--}}

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
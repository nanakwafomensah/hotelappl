 <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App favicon -->
    <link rel="shortcut icon" href="admin/images/favicon.ico">
    <!-- App title -->
    <title>HMS</title>

    <!--Morris Chart CSS -->

    {{Html::style('admin/plugins/morris/morris.css')}}
    {{Html::style('loader/loading.css')}}
    <!-- App css -->


    {{ Html::style('//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css') }}

    {{ Html::style('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css') }}

    {{ Html::style('admin/css/core.css') }}

    {{ Html::style('admin/css/components.css') }}

    {{ Html::style('admin/css/icons.css') }}

    {{ Html::style('admin/css/pages.css') }}

    {{ Html::style('admin/css/menu.css') }}

    {{ Html::style('admin/css/responsive.css') }}

    {{ Html::style('admin/plugins/switchery/switchery.min.css') }}
    {{ Html::style('parsley/css/parsley.css') }}
    {{ Html::style('//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css') }}
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ Html::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}

    {{ Html::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}
    <![endif]-->


    {{ Html::script('admin/js/modernizr.min.js') }}


</head>
<body class="fixed-left">
@include('layout.local.front.topbar')
@include('layout.local.front.leftsidebar')
@yield('content')


<script>
    var resizefunc = [];
</script>
<!--parsley->


<!-- jQuery  -->

{{ Html::script('admin/js/detect.js') }}

{{ Html::script('admin/js/fastclick.js') }}

{{ Html::script('admin/js/jquery.blockUI.js') }}

{{ Html::script('admin/js/waves.js') }}

{{ Html::script('admin/js/jquery.slimscroll.js') }}

{{ Html::script('admin/js/jquery.scrollTo.min.js') }}

{{ Html::script('admin/plugins/switchery/switchery.min.js') }}

<!-- Counter js  -->

{{ Html::script('admin/plugins/waypoints/jquery.waypoints.min.js') }}

{{ Html::script('admin/plugins/counterup/jquery.counterup.min.js') }}

<!--Morris Chart-->

{{ Html::script('admin/plugins/morris/morris.min.js') }}

{{ Html::script('admin/plugins/raphael/raphael-min.js') }}

<!-- Dashboard init -->

{{ Html::script('admin/pages/jquery.dashboard.js') }}

<!-- App js -->

{{ Html::script('admin/js/jquery.core.js') }}

{{ Html::script('admin/js/jquery.app.js') }}



{{ Html::script('//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.0/js/toastr.min.js') }}
<script>
     @if(Session::has('message'))
         var type="{{Session::get('alert-type','info')}}"
          switch(type){
              case 'success':
                  toastr.success('{{Session::get('message')}}','Success',{timeOut: 1000});

                  break;
              case 'error':
                  toastr.error('{{Session::get('message')}}','Success',{timeOut: 1000});

                  break;
              case 'info':
                  toastr.info('{{Session::get('message')}}','Success',{timeOut: 1000});

                  break;
          }
         @endif

</script>
</body>

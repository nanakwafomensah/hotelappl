<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <!-- App favicon -->
    <link rel="shortcut icon" href="admin/images/favicon.ico">
    <!-- App title -->
    <title>HMS</title>
    <!-- App css -->
    <link href="admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="admin/css/core.css" rel="stylesheet" type="text/css" />
    <link href="admin/css/components.css" rel="stylesheet" type="text/css" />
    <link href="admin/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="admin/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="admin/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="admin/css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="admin/plugins/switchery/switchery.min.css">
    <script src="admin/js/modernizr.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-83057131-1', 'auto');
        ga('send', 'pageview');
    </script>
    <style>
        body {
            /*background-image:url({{ URL::asset('assets/images/rsz_1bkgnd.jpg') }});
            background-image: url("../assets/images/logo.png");
            background-repeat: repeat-y;*/
        }
    </style>
</head>
<body>

<!-- OLD LOGIN -->
{{-- <section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="m-t-40 account-pages">
                        <div class="text-center account-logo-box">
                            <h2 class="text-uppercase">
                                <a href="{{route('login_local')}}" class="text-success">
                                    <span>HMS</span>
                                </a>
                            </h2>
                        </div>
                    <form role="form" class="login-form" method="post" action="postlogin_local">
                        {{csrf_field()}}
                        @if(session('error'))
                        <div class="alert alert-danger">
                             {{session('error')}}
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="icon fa fa-user"></i>
                                <input type="text" id="sender-email" class="form-control" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="icon fa fa-unlock-alt"></i>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" id="remember" name="rememberme" value="forever" style="float: left;">
                            <label for="remember">Remember me</label>
                        </div>
                        <!-- <a href="{{route('dashboard')}}" name="submit" value="Submit"/> -->
                        <button  type="submit" class="btn btn-common log-btn" >Submit</button>
                        @include('errors.online.validationerrors')

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}

 <!-- HOME -->
<section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="m-t-40 account-pages">
                <div class="panel panel-color panel-info">
                        <div class="text-center panel-heading">
                        {{-- <div class="text-center account-logo-box"> --}}
                            <h2 class="text-uppercase">
                            <a href="{{route('login_local')}}" class="text-success">
                                <span>HMS</span>
                            </a>
                            </h2>
                            <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                        </div>
                        <div style="padding-top: 5px">
                        </div>
                        <div class="account-content">
                            <form role="form" class="login-form" method="post" action="postlogin_local">
                                {{csrf_field() }}
                                @if(session('error'))
                                <div class="alert alert-danger">
                                    {{session('error')}}
                                </div>
                                @endif
                                @if(session('success'))
                                    <div class="alert alert-success">
                                        {{session('success')}}
                                    </div>
                                @endif
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="text" id="sender-email" class="form-control" name="email" placeholder="Email">
                                </div><br><br>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="mdi mdi-lock"></i></span>
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group account-btn text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn w-md btn-info waves-effect waves-light" value="login">Login <i class="mdi mdi-login"></i></button>
                                    </div>
                                </div><br><br><br>

                                <div class="form-group">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="remember" name="rememberme" value="forever" >
                                        <label for="remember">Remember me</label>&emsp;&emsp;&emsp;&emsp;&emsp;
                                        <a href="#">Forgot my password</a>
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    </div>
                    <!-- end card-box-->
                </div>
                <!-- end wrapper -->
            </div>
        </div>
    </div>
</section>
        <!-- END HOME -->






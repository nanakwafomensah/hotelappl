<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>INVOICE {{$invoicenumber}}</title>
    <link rel="stylesheet" href="receiptstyle/style.css" media="all" />

</head>
<body>
<header class="clearfix">
    <div id="logo">
        <?php $profiles=App\Profile::all()->first()?>
        <img src="uploads/avatars/{{$profiles->avatar}}" alt="user-img" class="img-circle user-img">
    </div>
    <div id="company">
        <h2 class="name">{{$hoteldetails->hotelname}}</h2>
        <div> {{$hoteldetails->address}}, {{$hoteldetails->website}} , {{$hoteldetails->city}},{{$hoteldetails->country}}.</div>
        <div>   {{$hoteldetails->phone}},{{$hoteldetails->telephone}}</div>
        <div><a href="mailto:company@example.com">  {{$hoteldetails->email}}</a></div>
    </div>
    </div>
</header>
<main>
    <div id="details" class="clearfix">
        <div id="client">
            <div class="to">PAYMENT FROM:</div>
            <h2 class="name">{{$customerdetails->first_name}} {{$customerdetails->last_name}}</h2>
            <div class="address">796 Silver Harbour, TX 79273, US</div>
            <div class="email"><a href="#">{{$customerdetails->email}}</a></div>
        </div>
        <div id="invoice">
            <h1>RECEIPT {{$receipt}}</h1>
                <div class="text">Invoice Number: {{$invoicenumber}}</div>
            <div class="date">Date of Invoice: {{$invoicedate->invoicedate}}</div>
            <div class="date"> Date of Payment: 30/06/2014</div>


        {{--</div>--}}
    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th class="no">#</th>
            <th class="desc">CATEGORY</th>
            <th class="unit">RECEIPT NUMBER</th>
            <th class="qty">REMARK</th>
            <th class="total">AMOUNT</th>
        </tr>
        </thead>
        <tbody>
        <?php $n=1;$totalamount=0;?>
        @foreach($paymentdetails as $i)
            <?php $totalamount=$totalamount+$i->amount?>
            <tr>
                <td class="no">{{$n++}}</td>
                <td class="desc">{{$i->category}}</td>
                <td class="unit">{{$i->receiptnumber}}</td>
                <td class="qty">{{$i->remark}}</td>
                <td class="total">GHC {{$i->amount}}</td>
            </tr>
        @endforeach


        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">TOTAL</td>
            <td>GHC {{number_format($totalamount)}}</td>
        </tr>


        </tfoot>
    </table>
    <div id="thanks">Thank you!</div>
    <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
    </div>
</main>
<footer>
    Payment was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>
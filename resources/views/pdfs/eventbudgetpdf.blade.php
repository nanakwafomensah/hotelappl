<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>event type </title>

<style>
    @font-face {
        font-family: SourceSansPro;

    }
    table {
        border-collapse: collapse;
    }

    td, th {

        border: 1px solid black; height: 10px; } /* Make cells a bit taller */

    th {
        background: #F3F3F3; /* Light grey background */
        font-weight: bold; /* Make sure they're bold */
    }


    .category {
        background-color: yellow;
    }
    .header_category {
        background-color: #00A8FF;
    }
    .header_projected {
         background-color: #2b982b;
     }
    .header_actual {
          background-color: #2b982b;
      }
    .header_comment {
           background-color: blue;
       }
    body {
        position: relative;
        /*width: 21cm;  */
        /*height: 29.7cm; */
        margin: 0 auto;
        color: #555555;
        background: #FFFFFF;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-family: SourceSansPro;
    }


</style>
</head>
<body>

<div class="container">



<div id="tableside">
<table style="width:400px;">
    <thead>
    <tr>
        <td class="header_category">CATEGORY</td>
        <td class="header_projected">PROJECTED SUBTOTAL</td>
        <td class="header_actual">ACTUAL SUBTOTAL</td>
        <td class="header_comment">COMMENT</td>
    </tr>
    </thead>
    <tbody>
    <?php $totalprojected=0;
          $totalactual=0;
    ?>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='venue')
            <?php
            $totalprojected=$totalprojected + $s->projected_subtotal;
            $totalactual=$totalactual + $s->actual_subtotal;
            ?>
          @endif
    @endforeach
    <tr class="category">
        <td><span>Venue </span> <span>SubTotal</span></td>
        <td id="venue_projected_total">{{$totalprojected}}</td>
        <td id="venue_actual_total">{{$totalactual}}</td>
        <td id="venue_comment"></td>
    </tr>

    @foreach($eventbudget as $s)
        @if($s->budget_type=='venue')
           <tr>
                <td>{{$s->categories}}</td>
                <td id="venue_projected_value">{{$s->projected_subtotal}}</td>
                <td id="venue_actual_value">{{$s->actual_subtotal}}</td>
                <td>{{$s->comment}}</td>
            </tr>
            @endif
    @endforeach




    <?php $totalprojectedt=0;
    $totalactualt=0;
    ?>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='travel')
            <?php
            $totalprojectedt=$totalprojectedt + $s->projected_subtotal;
            $totalactualt=$totalactualt + $s->actual_subtotal;
            ?>
        @endif
    @endforeach
    <tr class="category">
        <td><span>Travel</span> <span>SubTotal</span></td>
        <td id="travel_projected_total">{{$totalprojectedt}}</td>
        <td id="travel_actual_total">{{$totalactualt}}</td>
        <td id="travel_comment"></td>
    </tr>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='travel')

            <tr>
                <td>{{$s->categories}}</td>
                <td id="travel_projected_value">{{$s->projected_subtotal}}</td>
                <td id="travel_actual_value">{{$s->actual_subtotal}}</td>
                <td>{{$s->comment}}</td>
            </tr>
        @endif
    @endforeach






    <?php $totalprojectedpr=0;
    $totalactualpr=0;
    ?>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='publicrelations')
            <?php
            $totalprojectedpr=$totalprojectedpr + $s->projected_subtotal;
            $totalactualpr=$totalactualpr + $s->actual_subtotal;
            ?>
        @endif
    @endforeach
    <tr class="category">
        <td><span>Public Relations</span> <span>SubTotal</span></td>
        <td id="travel_projected_total">{{$totalprojectedpr}}</td>
        <td id="travel_actual_total">{{$totalactualpr}}</td>
        <td id="travel_comment"></td>
    </tr>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='publicrelations')

            <tr>
                <td>{{$s->categories}}</td>
                <td id="pr_projected_value">{{$s->projected_subtotal}}</td>
                <td id="pr_actual_value">{{$s->actual_subtotal}}</td>
                <td>{{$s->comment}}</td>
            </tr>
        @endif
    @endforeach





    <?php $totalprojectedd=0;
    $totalactuald=0;
    ?>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='decor')
            <?php
            $totalprojectedd=$totalprojectedd + $s->projected_subtotal;
            $totalactuald=$totalactuald + $s->actual_subtotal;
            ?>
        @endif
    @endforeach
    <tr class="category">
        <td><span>Decor</span> <span>SubTotal</span></td>
        <td id="decor_projected_total">{{$totalprojectedd}}</td>
        <td id="decor_actual_total">{{$totalactuald}}</td>
        <td id="decor_comment"></td>
    </tr>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='decor')

            <tr>
                <td>{{$s->categories}}</td>
                <td id="d_projected_value">{{$s->projected_subtotal}}</td>
                <td id="d_actual_value">{{$s->actual_subtotal}}</td>
                <td>{{$s->comment}}</td>
            </tr>
        @endif
    @endforeach




    <?php $totalprojectedep=0;
    $totalactualep=0;
    ?>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='eventp')
            <?php
            $totalprojectedep=$totalprojectedep + $s->projected_subtotal;
            $totalactualep=$totalactualep + $s->actual_subtotal;
            ?>
        @endif
    @endforeach
    <tr class="category">
        <td><span>Event Programming</span> <span>SubTotal</span></td>
        <td id="decor_projected_total">{{$totalprojectedep}}</td>
        <td id="decor_actual_total">{{$totalactualep}}</td>
        <td id="decor_comment"></td>
    </tr>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='eventp')

            <tr>
                <td>{{$s->categories}}</td>
                <td id="ep_projected_value">{{$s->projected_subtotal}}</td>
                <td id="ep_actual_value">{{$s->actual_subtotal}}</td>
                <td>{{$s->comment}}</td>
            </tr>
        @endif
    @endforeach






    <?php $totalprojectedsm=0;
    $totalactualsm=0;
    ?>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='socialmedia')
            <?php
            $totalprojectedsm=$totalprojectedsm + $s->projected_subtotal;
            $totalactualsm=$totalactualsm + $s->actual_subtotal;
            ?>
        @endif
    @endforeach
    <tr class="category">
        <td><span>Social Media</span> <span>SubTotal</span></td>
        <td id="sm_projected_total">{{$totalprojectedsm}}</td>
        <td id="sm_actual_total">{{$totalactualsm}}</td>
        <td id="sm_comment"></td>
    </tr>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='socialmedia')

            <tr>
                <td>{{$s->categories}}</td>
                <td id="sm_projected_value">{{$s->projected_subtotal}}</td>
                <td id="sm_actual_value">{{$s->actual_subtotal}}</td>
                <td>{{$s->comment}}</td>
            </tr>
        @endif
    @endforeach






    <?php $totalprojectedr=0;
    $totalactualr=0;
    ?>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='refreshment')
            <?php
            $totalprojectedr=$totalprojectedr + $s->projected_subtotal;
            $totalactualr=$totalactualr + $s->actual_subtotal;
            ?>
        @endif
    @endforeach
    <tr class="category">
        <td><span>Refreshment</span> <span>SubTotal</span></td>
        <td id="r_projected_total">{{$totalprojectedr}}</td>
        <td id="r_actual_total">{{$totalactualr}}</td>
        <td id="r_comment"></td>
    </tr>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='refreshment')

            <tr>
                <td>{{$s->categories}}</td>
                <td id="r_projected_value">{{$s->projected_subtotal}}</td>
                <td id="r_actual_value">{{$s->actual_subtotal}}</td>
                <td>{{$s->comment}}</td>
            </tr>
        @endif
    @endforeach





    <?php $totalprojectedo=0;
    $totalactualo=0;
    ?>
    @foreach($eventbudget as $s)
        @if($s->budget_type=='refreshment')
            <?php
            $totalprojectedo=$totalprojectedo + $s->projected_subtotal;
            $totalactualo=$totalactualo + $s->actual_subtotal;
            ?>
        @endif
    @endforeach
    <tr class="category">
        <td><span>Others</span> <span>SubTotal</span></td>
        <td id="o_projected_total">{{$totalprojectedo}}</td>
        <td id="o_actual_total">{{$totalactualo}}</td>
        <td id="o_comment"></td>
    </tr>

    @foreach($eventbudget as $s)
        @if($s->budget_type=='others')

            <tr>
                <td>{{$s->categories}}</td>
                <td id="o_projected_value">{{$s->projected_subtotal}}</td>
                <td id="o_actual_value">{{$s->actual_subtotal}}</td>
                <td>{{$s->comment}}</td>
            </tr>
        @endif
    @endforeach

    </tbody>
</table>
</div>

  <div id="graphside">
      <h4>hello</h4>

  </div>
</div>
</body>
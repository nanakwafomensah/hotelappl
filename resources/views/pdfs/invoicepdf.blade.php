<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>INVOICE {{$invoicenumber}}</title>
    <link rel="stylesheet" href="invoicestyle/style.css" media="all" />

</head>
<body>
<header class="clearfix">
    <div id="logo">
        <?php $profiles=App\Profile::all()->first()?>
        @if(!empty($profiles->avatar))
        <img src="uploads/avatars/{{$profiles->avatar}}" alt="user-img" class="img-circle user-img">
            @else
                <img src="uploads/avatars/default.jpg" alt="user-img" class="img-circle user-img">
        @endif
    </div>
    <div id="company">
        <h2 class="name">{{$hoteldetails->hotelname}}</h2>
        <div> {{$hoteldetails->address}}, {{$hoteldetails->website}} , {{$hoteldetails->city}},{{$hoteldetails->country}}.</div>
        <div>   {{$hoteldetails->phone}},{{$hoteldetails->telephone}}</div>
        <div><a href="mailto:company@example.com">  {{$hoteldetails->email}}</a></div>
    </div>
    </div>
</header>
<main>
    <div id="details" class="clearfix">
        <div id="client">
            <div class="to">INVOICE TO:</div>
            <h2 class="name">{{$customerdetails->first_name}} {{$customerdetails->last_name}}</h2>
            <div class="address">796 Silver Harbour, TX 79273, US</div>
            <div class="email"><a href="#">{{$customerdetails->email}}</a></div>
        </div>
        <div id="invoice">
            <h1>INVOICE {{$invoicenumber}}</h1>
            <div class="date">Date of Invoice: {{$invoicedate->invoicedate}}</div>
            <div class="date">Due Date: 30/06/2014</div>


        </div>
    </div>
    <table border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th class="no">#</th>
            <th class="desc">DESCRIPTION</th>
            <th class="unit">UNIT PRICE</th>
            <th class="qty">QUANTITY</th>
            <th class="total">TOTAL</th>
        </tr>
        </thead>
        <tbody>
        <?php $n=1;$totalamount=0;?>
        @foreach($items_oninvoice as $i)
            <?php $totalamount=$totalamount+$i->quantity * number_format($i->amount)?>
            <tr>
                <td class="no">{{$n++}}</td>
                <td class="desc">{{$i->description}}</td>
                <td class="unit">GHC {{number_format($i->amount)}}</td>
                <td class="qty">{{$i->quantity}}</td>
                <td class="total">GHC {{$i->quantity * number_format($i->amount)}}</td>
            </tr>
        @endforeach


        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">SUBTOTAL</td>
            <td>GHC {{number_format($totalamount)}}</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">TAX 25%</td>
            <?php $taxvalue=10;?>
            <td>GHC {{$taxvalue}}</td>
        </tr>

        <tr>
            <td colspan="2"></td>
            <td colspan="2">GRAND TOTAL</td>
            <td>{{number_format($totalamount) + $taxvalue }}</td>
        </tr>
        </tfoot>
    </table>
    <div id="thanks">Thank you!</div>
    <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
    </div>
</main>
<footer>
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>